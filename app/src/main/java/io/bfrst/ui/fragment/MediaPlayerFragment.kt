package io.bfrst.ui.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.utils.Animation
import io.bfrst.artifact.Artifact
import io.bfrst.artifact.IDelayedInit
import io.bfrst.extension.normalize
import io.bfrst.extension.scale
import io.bfrst.utils.TouchSupport
import kotlinx.android.synthetic.main.fragment_media_player.view.*

class MediaPlayerFragment : Fragment(), IDelayedInit {
    enum class PushType { Next, Prev, Back }
    var animation:                  Animation? = null

    // ----- Drawables -----
    private var listOfDrawables:            MutableList<Drawable> = mutableListOf()

    // ----- Track image, name and artist views -----
    private lateinit var activeTrackImage  :ImageView
    private lateinit var activeTrackName   :TextView
    private lateinit var activeTrackArtist :TextView

    private lateinit var nextTrackImage     :ImageView
    private lateinit var nextTrackName      :TextView
    private lateinit var nextTrackArtist    :TextView

    private lateinit var prevTrackImage     :ImageView
    private lateinit var prevTrackName      :TextView
    private lateinit var prevTrackArtist    :TextView

    // ----- Control views -----
    private lateinit var playButton         :ImageView
    private lateinit var prevButton         :ImageView
    private lateinit var nextButton         :ImageView
    private lateinit var shuffleButton      :ImageView
    private lateinit var repeatButton       :ImageView


    // ----- Positions -----
    private var positionActiveTrack:    Float = 0.0f
    private var positionNextTrack:      Float = 0.0f
    private var positionPrevTrack:      Float = 0.0f
    private var positionInitialTouch:   Float = 0.0f

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_media_player, parent, false)

        activeTrackImage = view.player_track_image
        activeTrackName = view.player_track_name
        activeTrackArtist = view.player_track_artist

        nextTrackImage = view.player_track_image_next
        nextTrackName = view.player_track_name_next
        nextTrackArtist = view.player_track_artist_next

        prevTrackImage = view.player_track_image_prev
        prevTrackName = view.player_track_name_prev
        prevTrackArtist = view.player_track_artist_prev

        playButton = view.player_track_play
        prevButton = view.player_track_prev
        nextButton = view.player_track_next
        shuffleButton = view.player_track_shuffle
        repeatButton = view.player_track_repeat

        registerDelayedInit()
        return view
    }

    override fun delayedInit() {
        positionActiveTrack     = 0.0f
        positionNextTrack       = Artifact.Display.screenWidth
        positionPrevTrack       = -Artifact.Display.screenWidth

        nextButton.setOnClickListener { pushTrack(PushType.Next) }
        prevButton.setOnClickListener { pushTrack(PushType.Prev) }

        initDrawables()
        finalizeViews()
    }

    fun pushTrackByPosition() {
        when {
            activeTrackName.translationX < positionPrevTrack / 4 -> pushTrack(PushType.Next)
            activeTrackName.translationX > positionNextTrack / 4 -> pushTrack(PushType.Prev)
            else -> pushTrack(PushType.Back)
        }
    }

    /** Pushes track
     * @param pushType - what action to apply to the track
     * @param ignoreActiveAnimation - is should ignore active animation even if it is not completed.
     * Used for back button handling. User should not wait until animation ends, needs a instant reaction! */
    fun pushTrack(pushType: PushType, ignoreActiveAnimation: Boolean = true) {
        when {
            !ignoreActiveAnimation && animation != null && !animation!!.isAnimationCompleted() -> return
            ignoreActiveAnimation && animation != null -> {
                animation?.cancel()
                finalizeViews()
            }
        }

        val targetPosition: Float
        val animDuration: Long

        when (pushType) {
            PushType.Next -> {
                targetPosition = positionPrevTrack
                animDuration = 300L
                Artifact.activeTrackIndex++
            }

            PushType.Prev -> {
                targetPosition = positionNextTrack
                animDuration = 300L
                Artifact.activeTrackIndex--
            }

            PushType.Back -> {
                targetPosition = positionActiveTrack
                animDuration = 200L
            }
        }

        animation?.cancel()
        animation = Animation.animate(activeTrackName.translationX, targetPosition, animDuration) { anim ->
            moveViewsByValue(anim.value)
            scaleViewsByPosition()
            fadeViewsByPosition()
            if (anim.isCompleted) {
                finalizeViews()
                animation = null
            }
        }
    }

    fun moveViewsByValue(value: Float) {
        activeTrackImage.translationX  = value
        activeTrackName.translationX   = value
        activeTrackArtist.translationX = value

        nextTrackImage.translationX     = positionNextTrack + value
        nextTrackName.translationX      = positionNextTrack + value
        nextTrackArtist.translationX    = positionNextTrack + value

        prevTrackImage.translationX     = positionPrevTrack + value
        prevTrackName.translationX      = positionPrevTrack + value
        prevTrackArtist.translationX    = positionPrevTrack + value
    }

    fun moveViewsByTouch(touch: TouchSupport) {
        if (animation != null && !animation!!.isAnimationCompleted()) {
            animation?.cancel()
            animation = null
            finalizeViews()
        }
        activeTrackImage.translationX  = positionActiveTrack + touch.latest.x - touch.first.x
        activeTrackName.translationX   = positionActiveTrack + touch.latest.x - touch.first.x
        activeTrackArtist.translationX = positionActiveTrack + touch.latest.x - touch.first.x

        nextTrackImage.translationX     = activeTrackImage.translationX    + positionNextTrack
        nextTrackName.translationX      = activeTrackName.translationX     + positionNextTrack
        nextTrackArtist.translationX    = activeTrackArtist.translationX   + positionNextTrack

        prevTrackImage.translationX     = activeTrackImage.translationX    + positionPrevTrack
        prevTrackName.translationX      = activeTrackName.translationX     + positionPrevTrack
        prevTrackArtist.translationX    = activeTrackArtist.translationX   + positionPrevTrack

        scaleViewsByPosition()
        fadeViewsByPosition()
    }

    fun scaleViewsByPosition() {
        val imageScalePosition = positionNextTrack / 2
        if (activeTrackImage.translationX >= 0) {
            activeTrackImage.scale = activeTrackImage.translationX.normalize(imageScalePosition, positionActiveTrack)
        }
        else {
            activeTrackImage.scale = activeTrackImage.translationX.normalize(-imageScalePosition, positionActiveTrack)
        }
        nextTrackImage.scale = nextTrackImage.translationX.normalize(imageScalePosition, positionActiveTrack)
        prevTrackImage.scale = prevTrackImage.translationX.normalize(-imageScalePosition, positionActiveTrack)
    }

    fun fadeViewsByPosition() {
        val imageFadePosition = positionNextTrack / 2
        if (activeTrackImage.translationX >= 0) {
            activeTrackImage.alpha = activeTrackImage.translationX.normalize(imageFadePosition, positionActiveTrack)
        }
        else {
            activeTrackImage.alpha = activeTrackImage.translationX.normalize(-imageFadePosition, positionActiveTrack)
        }
        nextTrackImage.alpha = nextTrackImage.translationX.normalize(imageFadePosition, positionActiveTrack)
        prevTrackImage.alpha = prevTrackImage.translationX.normalize(-imageFadePosition, positionActiveTrack)
    }

    fun initDrawables() {
        for (i in Artifact.tracks) {
            val ims = resources.assets.open("${i.assetName}.jpg")
            val drawable = Drawable.createFromStream(ims, null)
            listOfDrawables.add(drawable)
        }
    }

    fun finalizeViews() {
        refreshTrackImagesAndLabels()
        resetPositions()
        resetVisibility()
        resetScale()
    }

    fun refreshTrackImagesAndLabels() {
        with (Artifact) {
            // Out of bound check and fixing of invalid indexes
            if (activeTrackIndex < 0) activeTrackIndex = tracks.lastIndex
            else if (activeTrackIndex > tracks.lastIndex) activeTrackIndex = 0

            activeTrackImage.setImageDrawable(listOfDrawables[activeTrackIndex])
            activeTrackName.text = tracks[activeTrackIndex].name
            activeTrackArtist.text = tracks[activeTrackIndex].artist

            // If current track index is last index of list -> fetch first track info of list
            // ELSE just take next track info
            if (activeTrackIndex == tracks.lastIndex) {
                nextTrackImage.setImageDrawable(listOfDrawables[0])
                nextTrackName.text = tracks[0].name
                nextTrackArtist.text = tracks[0].artist
            } else {
                nextTrackImage.setImageDrawable(listOfDrawables[activeTrackIndex + 1])
                nextTrackName.text = tracks[activeTrackIndex + 1].name
                nextTrackArtist.text = tracks[activeTrackIndex + 1].artist
            }

            // If current track index is 0 in list -> fetch last track info of list
            // ELSE just take prev track info
            if (activeTrackIndex == 0) {
                // then fetch last track of list
                prevTrackImage.setImageDrawable(listOfDrawables[Artifact.tracks.lastIndex])
                prevTrackName.text = tracks[tracks.lastIndex].name
                prevTrackArtist.text = tracks[tracks.lastIndex].artist
            } else {
                prevTrackImage.setImageDrawable(listOfDrawables[activeTrackIndex - 1])
                prevTrackName.text = tracks[activeTrackIndex - 1].name
                prevTrackArtist.text = tracks[activeTrackIndex - 1].artist
            }
        }
    }

    fun resetPositions() {
        activeTrackImage.translationX  = positionActiveTrack
        activeTrackName.translationX   = positionActiveTrack
        activeTrackArtist.translationX = positionActiveTrack

        nextTrackImage.translationX     = positionNextTrack
        nextTrackName.translationX      = positionNextTrack
        nextTrackArtist.translationX    = positionNextTrack

        prevTrackImage.translationX     = positionPrevTrack
        prevTrackName.translationX      = positionPrevTrack
        prevTrackArtist.translationX    = positionPrevTrack
    }

    fun resetVisibility() {
        activeTrackImage.alpha = 1.0f
        activeTrackName.alpha = 1.0f
        activeTrackArtist.alpha = 1.0f

        nextTrackImage.alpha = 1.0f
        nextTrackName.alpha = 1.0f
        nextTrackArtist.alpha = 1.0f

        prevTrackImage.alpha = 1.0f
        prevTrackName.alpha = 1.0f
        prevTrackArtist.alpha = 1.0f
    }

    fun resetScale() {
        activeTrackImage.scale = 1.0f
        activeTrackName.scale = 1.0f
        activeTrackArtist.scale = 1.0f

        nextTrackImage.scale = 1.0f
        nextTrackName.scale = 1.0f
        nextTrackArtist.scale = 1.0f

        prevTrackImage.scale = 1.0f
        prevTrackName.scale = 1.0f
        prevTrackArtist.scale = 1.0f
    }
}