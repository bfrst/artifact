package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.fragment.app.Fragment
import io.bfrst.R
import kotlinx.android.synthetic.main.fragment_account.view.*

// Task: finish layout
// Task: make user image to be in circle
class AccountFragment : Fragment() {

    lateinit var userImage      :ImageView
    lateinit var userName       :TextView

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_account, parent, false)

        userImage   = view.account_user_image
        userName    = view.account_user_name

        val assetStream = resources.assets.open("anacondaz_album.jpg")
        val drawable = RoundedBitmapDrawable.createFromStream(assetStream, null)
        userImage.setImageDrawable(drawable)

        return view
    }

}