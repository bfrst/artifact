package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.extension.scale
import io.bfrst.utils.Animation
import kotlinx.android.synthetic.main.fragment_appbar.view.*

class AppbarFragment : Fragment() {

    private var animation               :Animation? = null
    private var isInSearchMode          :Boolean = false
    private var isInTransaction         :Boolean = false

    private lateinit var appTitle       :TextView
    private lateinit var searchBody     :ConstraintLayout
    private lateinit var searchButton   :ImageButton
    private lateinit var searchField    :EditText

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_appbar, parent, false)

        appTitle        = view.appbar_title
        searchButton    = view.appbar_search
        searchField     = view.appbar_search_field
        searchBody      = view.appbar_search_body

        searchButton.setOnClickListener { onSearchButtonClicked() }

        return view
    }

    fun isInSearchMode() = isInSearchMode

    private fun onSearchButtonClicked() {
        when {
            isInSearchMode  -> disableSearch()
            !isInSearchMode -> enableSearch()
        }
    }

    private fun enableSearch() {
        if (isInTransaction) return
        isInTransaction = true
        isInSearchMode = true
        animation?.cancel()
        animation = Animation.animate(1.0f, 0.0f, 150L) {
            appTitle.alpha = it.value
            if (it.isCompleted) appTitle.visibility = View.INVISIBLE
            // App title fade in
        }.with(1.0f, 0.75f, 150L) {
            searchButton.scale = it.value
            // Search button scale down
        }.then(0.0f, 1.0f, 200L) {
            if (it.isFirstTick) searchField.visibility = View.VISIBLE
            searchField.alpha = it.value
            if (it.isCompleted) isInTransaction = false
            // Search field fade out
        }.with(0.0f, 0.8f, 200L) {
            if (it.isFirstTick) searchBody.visibility = View.VISIBLE
            searchBody.alpha = it.value
            // Search body fade out
        }.with(1.0f, 0.5f, 200L) {
            searchButton.alpha = it.value
            // Search button fade in
        }
    }

    fun disableSearch() {
        if (isInTransaction) return
        isInTransaction = true
        isInSearchMode = false
        animation?.cancel()
        animation = Animation.animate(1.0f, 0.0f, 150L) {
            searchField.alpha   = it.value
            if (it.isCompleted)  searchField.visibility  = View.INVISIBLE
            // Search field fade in
        }.with(0.8f, 0.0f, 150L) {
            searchBody.alpha    = it.value
            if (it.isCompleted) searchBody.visibility   = View.INVISIBLE
            // Search body fade in
        }.with(0.75f, 1.0f, 150L) {
            searchButton.scale = it.value
            // Search button scale up
        }.then(0.0f, 1.0f, 200L) {
            if (it.isFirstTick) appTitle.visibility = View.VISIBLE
            appTitle.alpha = it.value
            if (it.isCompleted) isInTransaction = false
            // App title fade out
        }.with(0.5f, 1.0f, 200L) {
            searchButton.alpha = it.value
            // Search button fade out
        }
    }

}