package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import io.bfrst.R
import io.bfrst.artifact.*
import io.bfrst.utils.Animation
import io.bfrst.utils.Touch
import io.bfrst.utils.TouchSupport
import kotlinx.android.synthetic.main.fragment_drawer.view.*

// Task maybe get rid of this bar on the bottom of drawer
class DrawerFragment : Fragment(), IDelayedInit {
    var touch: TouchSupport = TouchSupport()

    var tabBindings: HashMap<Int, (event: TabEvent) -> Unit> = HashMap()

    var isOpen: Boolean = false
    var positionOpen: Float = 0.0f
    var positionClosed: Float = 0.0f

    var animation: Animation? = null

    lateinit var drawer: ConstraintLayout
    lateinit var drawerHeader: ConstraintLayout
    lateinit var drawerBody: ConstraintLayout
    lateinit var drawerFooter: ConstraintLayout
    lateinit var tabs: TabLayout

    lateinit var playerFragment: MediaPlayerFragment
    lateinit var playerMiniFragment: PlayerMiniFragment

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_drawer, parent, false)

        drawer = view.drawer
        drawerHeader = view.drawer_header
        drawerBody = view.drawer_body
        drawerFooter = view.drawer_footer

        playerFragment = childFragmentManager.findFragmentById(R.id.drawer_player) as MediaPlayerFragment
        playerMiniFragment = childFragmentManager.findFragmentById(R.id.drawer_player_mini) as PlayerMiniFragment

        tabs = view.drawer_footer_tabs
        tabs.addOnTabSelectedListener(OnTabSelectedListener)

        drawerHeader.setOnTouchListener { view, event -> handleTouch(view, event) }
        drawerBody.setOnTouchListener { view, event -> handleTouch(view, event) }

        registerDelayedInit()
        return view
    }

    override fun delayedInit() {
        positionOpen = Artifact.Display.headerHeight
        positionClosed = Artifact.Display.sceneHeight
        drawer.maxHeight = Artifact.Display.sceneHeight.toInt()
        drawer.translationY = positionClosed
    }

    // Main on touch handler
    private fun handleTouch(view: View, motionEvent: MotionEvent): Boolean {
        touch.handleTouch(Touch.fromMotionEvent(motionEvent))
        if (touch.latest.action == Touch.Action.START) {
            //animation?.cancel()
        }

        if (view.id == drawerHeader.id) handleHeaderTouch()
        if (view.id == drawerBody.id) handleBodyTouch()
        return true
    }

    /**
     * Перетягивание и свайп вправо/влево для пролистывания страниц контента
     * */
    private fun handleBodyTouch() {
        if (!isOpen) return
        with (touch) {
            when (latest.action) {
                Touch.Action.MOVE -> {
                    if (gesture != Touch.Gesture.MOVE_HORIZONTAL || !isInBoundsOfView(drawerBody)) return
                    playerFragment.moveViewsByTouch(touch)
                }
                Touch.Action.END -> {
                    when (gesture) {
                        Touch.Gesture.MOVE_HORIZONTAL -> {
                            playerFragment.pushTrackByPosition()
                        }
                        Touch.Gesture.SWIPE_LEFT -> {
                            if (!isInBoundsOfView(drawerBody)) return
                            playerFragment.pushTrack(MediaPlayerFragment.PushType.Next)
                        }
                        Touch.Gesture.SWIPE_RIGHT -> {
                            if (!isInBoundsOfView(drawerBody)) return
                            playerFragment.pushTrack(MediaPlayerFragment.PushType.Prev)
                        }
                    }
                }
            }
        }
    }

    /**
     * Одиночный быстрый клик и свайп вверх для открытия drawer'a
     * Перетаскивание и свайп влево/вправо для перелистывания треков
     * */
    private fun handleHeaderTouch() {
        if (isOpen) return
        with (touch) {
            when {
                latest.action == Touch.Action.START -> {
                    playerMiniFragment.prepareViews()
                }

                latest.action == Touch.Action.MOVE && gesture == Touch.Gesture.MOVE_HORIZONTAL -> {
                    if (!isInBoundsOfView(drawerHeader)) return
                    playerMiniFragment.moveViewsByTouch(touch)
                }

                latest.action == Touch.Action.END -> {
                    when (gesture) {
                        Touch.Gesture.NONE, Touch.Gesture.SWIPE_UP -> {
                            pushDrawer(true)
                        }

                        Touch.Gesture.MOVE_HORIZONTAL -> {
                            playerMiniFragment.makeDecisionByPosition()
                        }

                        Touch.Gesture.SWIPE_LEFT -> {
                            if (!isInBoundsOfView(drawerHeader)) return
                            playerMiniFragment.pushNext()
                        }

                        Touch.Gesture.SWIPE_RIGHT -> {
                            if (!isInBoundsOfView(drawerHeader)) return
                            playerMiniFragment.pushPrev()
                        }
                    }
                }
            }
        }
    }

    /** Changes drawer state and animates it opening/closing.
     * @param newState - true or false for open and closed states
     * @param ignoreActiveAnimation - is should ignore active animation even if it is not completed.
     * Used for back button handling. User should not wait until animation ends, needs a instant reaction!*/
    fun pushDrawer(newState: Boolean, ignoreActiveAnimation: Boolean = false) {
        when {
            !ignoreActiveAnimation && animation != null && !animation!!.isAnimationCompleted() -> return
            isOpen == newState -> return
        }

        val startPosition: Float
        val targetPosition: Float
        val animDuration: Long

        isOpen = newState
        animation?.cancel()
        when (newState) {
            true -> {
                startPosition = positionClosed
                targetPosition = positionOpen
                animDuration = 300L
                animation = Animation.animate(startPosition, targetPosition, animDuration) { anim ->
                    // Move drawer position Y
                    drawer.translationY = anim.value
                }.with(1.0f, 0.0f, animDuration) {anim ->
                    // Fade in drawer header
                    drawerHeader.alpha = anim.value
                    if (anim.isCompleted) drawerHeader.visibility = View.INVISIBLE
                }.with(0.0f, 1.0f, animDuration) { anim ->
                    // Fade out drawer body
                    if (anim.isFirstTick) drawerBody.visibility = View.VISIBLE
                    drawerBody.alpha = anim.value
                }
            }
            false -> {
                startPosition = positionOpen
                targetPosition = positionClosed
                animDuration = 200L
                animation = Animation.animate(startPosition, targetPosition, animDuration) { anim ->
                    // Move drawer position Y
                    drawer.translationY = anim.value
                }.with(1.0f, 0.0f, animDuration) {anim ->
                    // Fade in drawer body
                    drawerBody.alpha = anim.value
                    if (anim.isCompleted) drawerBody.visibility = View.INVISIBLE
                }.with(0.0f, 1.0f, animDuration) { anim ->
                    // Fade out drawer header
                    if (anim.isFirstTick) drawerHeader.visibility = View.VISIBLE
                    drawerHeader.alpha = anim.value
                }
            }
        }
    }

    // Just calling implementations start tabBindings if they exists
    enum class TabEvent { SELECT, RESELECT, DESELECT }
    private var OnTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(tab: TabLayout.Tab?) {
            if (tab == null) return
            tabBindings[tab.position]?.invoke(TabEvent.RESELECT)
            pushDrawer(false)
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            if (tab == null) return
            tabBindings[tab.position]?.invoke(TabEvent.DESELECT)
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            if (tab == null) return
            tabBindings[tab.position]?.invoke(TabEvent.SELECT)
            pushDrawer(false)
        }
    }
}