package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.artifact.*
import io.bfrst.utils.Animation

// Task: handle back button pressed
// Task: add to the app bar back button
class ContentFragment : Fragment(), IEventListener {
    enum class Screen {
        Menu,
        Music,
        Settings,
        Account,
        Login
    }

    override var listenEvents:              List<String> = listOf(ChangeContentScreenEvent.ID)

    private var animation:                  Animation? = null
    var activeScreen:                       Screen = Screen.Menu
    private set

    private lateinit var menuFragment:      MenuFragment
    private lateinit var musicFragment:     MusicFragment
    private lateinit var settingsFragment:  SettingsFragment
    private lateinit var accountFragment:   AccountFragment
    private lateinit var loginFragment:     LoginFragment

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_content, parent, false)

        menuFragment        = childFragmentManager.findFragmentById(R.id.content_menu) as MenuFragment
        musicFragment       = childFragmentManager.findFragmentById(R.id.content_music) as MusicFragment
        settingsFragment    = childFragmentManager.findFragmentById(R.id.content_settings) as SettingsFragment
        accountFragment     = childFragmentManager.findFragmentById(R.id.content_account) as AccountFragment
        loginFragment       = childFragmentManager.findFragmentById(R.id.content_login) as LoginFragment

        musicFragment.view?.visibility = View.INVISIBLE
        settingsFragment.view?.visibility = View.INVISIBLE
        accountFragment.view?.visibility = View.INVISIBLE
        loginFragment.view?.visibility = View.INVISIBLE

        registerEventListener(this)
        return view
    }

    /** @return fragment that matches passed screen name */
    private fun getFragmentByScreenName(screenName: Screen): Fragment {
        when (screenName) {
            Screen.Menu -> return menuFragment
            Screen.Music -> return musicFragment
            Screen.Settings -> return settingsFragment
            Screen.Account -> return accountFragment
            Screen.Login -> return loginFragment
        }
    }

    /** Change active screen
     * @param newScreenName - name of the screen that will be pushed into contentFragment
     * @param pushBack - is should it looks like it pushes back to the previous position
     * @param ignoreActiveAnimation - is should ignore active animation even if it is not completed.
     * Used for back button handling. User should not wait until animation ends, needs a instant reaction! */
    fun pushScreen(newScreenName: Screen, pushBack: Boolean = false, ignoreActiveAnimation: Boolean = false) {
        when {
            !ignoreActiveAnimation && animation != null && !animation!!.isAnimationCompleted() -> return
            activeScreen == newScreenName -> return
        }

        val activeFragment = getFragmentByScreenName(activeScreen)
        val targetFragment = getFragmentByScreenName(newScreenName)
        val targetPosition: Float
        val animDuration: Long

        when (pushBack) {
            true -> {
                targetPosition = Artifact.Display.screenWidth
                animDuration = 200L
            }
            false -> {
                targetPosition = -Artifact.Display.screenWidth
                animDuration = 300L
            }
        }

        activeScreen = newScreenName
        animation?.cancel()
        animation = Animation.animate(0.0f, targetPosition, animDuration) {
            if (it.isFirstTick) {
                targetFragment.view!!.visibility = View.VISIBLE
            }

            activeFragment.view!!.translationX = it.value
            targetFragment.view!!.translationX = activeFragment.view!!.translationX - targetPosition

            if (it.isCompleted) {
                targetFragment.view!!.translationX = 0.0f
                activeFragment.view!!.visibility = View.INVISIBLE
            }
        }
    }

    override fun handleEvent(event: ArtifactEvent) {
        if (event is ChangeContentScreenEvent) {
            if (activeScreen == event.newScreenName) return
            pushScreen(event.newScreenName)
        }
    }
}