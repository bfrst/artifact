package io.bfrst.ui.fragment

import android.graphics.Typeface
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.artifact.*
import kotlinx.android.synthetic.main.fragment_login.view.*

class LoginFragment : Fragment(), ArtifactComponent {

    override var listenForEvents: MutableList<String> = mutableListOf(VkLoginResult.ID)

    private lateinit var loginField:    EditText
    private lateinit var passField:     EditText
    private lateinit var passCheck:     CheckBox
    private lateinit var loginButton:   Button

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, parent, false)
        loginField = view.login_field_login
        passField = view.login_field_pass
        passCheck = view.login_check_pass
        loginButton = view.login_button_login
        initListeners()
        return view
    }

    fun initListeners() {
        passCheck.setOnClickListener {
            val selection = passField.selectionEnd
            if (passCheck.isChecked) {
                passField.inputType = InputType.TYPE_CLASS_TEXT + InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            } else {
                passField.inputType = InputType.TYPE_CLASS_TEXT + InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
            passField.setTypeface(Typeface.DEFAULT)
            passField.setSelection(selection)
        }

        loginButton.setOnClickListener {
            if (isFieldsOk()) {
                val event = VkLoginEvent(loginField.text.toString(), passField.text.toString())
                fireEvent(event)
            } else {
                val event = InfobarShowEvent("В поля введены неверные данные")
                fireEvent(event)
            }
        }
    }

    private fun isFieldsOk(): Boolean {
        return loginField.text.toString().isNotEmpty() && passField.text.toString().isNotEmpty()
    }

    fun handleLoginSuccess() {

    }

    fun handleLoginFailure() {

    }

    override fun handleEvent(event: ArtifactEvent) {
        if (event is VkLoginResult) {
            when (event.result) {
                VkLoginResult.OK -> handleLoginSuccess()
                VkLoginResult.FAILURE -> handleLoginFailure()
            }
        }
    }
}