package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.utils.Animation
import io.bfrst.artifact.Artifact
import io.bfrst.artifact.ArtifactComponent
import io.bfrst.utils.TouchSupport
import kotlinx.android.synthetic.main.fragment_player_mini.view.*

// @Task cleanup
class PlayerMiniFragment : Fragment(), ArtifactComponent {

    override var listenForEvents: MutableList<String> = mutableListOf()

    data class Track(val name: String, val artist: String)

    var touch:                      TouchSupport = TouchSupport()
    var currentTrackPosition:       Float = 0.0f
    var nextTrackPosition:          Float = 0.0f
    var prevTrackPosition:          Float = 0.0f

    var animDurationChangeTrack:    Long = 200L
    var trackAnim: Animation? = null

    var activeTrackIndex:           Int = 0
    var tracks:                     MutableList<Track> = mutableListOf(
            Track("Waiting for something more", "ember j"),
            Track("Waiting for you", "The Aces"),
            Track("The Hype", "Twenty one pilots"),
            Track("My Blood", "Twenty one pilots"),
            Track("In love with Psycho", "Kasabian"),
            Track("Mr Blue Sky", "BLOXX"))

    lateinit var playerBody:        ConstraintLayout
    lateinit var currentTrackName:         TextView
    lateinit var currentTrackArtist:       TextView
    lateinit var nextTrackName:     TextView
    lateinit var nextTrackArtist:   TextView
    lateinit var prevTrackName:     TextView
    lateinit var prevTrackArtist:   TextView
    var isInTransaction: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_player_mini, parent, false)
        playerBody          = view.player_mini_body
        currentTrackName    = view.player_mini_track_name
        currentTrackArtist  = view.player_mini_track_artist
        nextTrackName       = view.player_mini_track_name_next
        nextTrackArtist     = view.player_mini_track_artist_next
        prevTrackName       = view.player_mini_track_name_prev
        prevTrackArtist     = view.player_mini_track_artist_prev
        Artifact.registerComponent(this)
        return view
    }

    override fun onInitComponent() {
        currentTrackPosition = 0.0f
        nextTrackPosition = Artifact.Display.screenWidth
        prevTrackPosition = -Artifact.Display.screenWidth

        //playerFragment.setOnTouchListener { view, event -> handleTouch(view, event) }

        refreshTrackLabels()
    }

    fun makeDecisionByPosition() {
        if (isShouldPushNext()) pushNext()
        else if (isShouldPushPrev()) pushPrev()
        else pushBack()
    }

    fun isShouldPushNext(): Boolean {
        return currentTrackName.translationX < prevTrackPosition / 4
    }

    fun isShouldPushPrev(): Boolean {
        return currentTrackName.translationX > nextTrackPosition / 4
    }

    fun prepareViews() {
        nextTrackName.translationX      = nextTrackPosition
        nextTrackArtist.translationX    = nextTrackPosition
        prevTrackName.translationX      = prevTrackPosition
        prevTrackArtist.translationX    = prevTrackPosition

        nextTrackName.visibility    = View.VISIBLE
        nextTrackArtist.visibility  = View.VISIBLE
        prevTrackName.visibility    = View.VISIBLE
        prevTrackArtist.visibility  = View.VISIBLE
    }

    // @Task need target get rid of touch support class here, maybe rename it or refactor
    fun moveViewsByTouch(touch: TouchSupport) {
        currentTrackName.translationX = currentTrackPosition + touch.latest.x - touch.first.x
        currentTrackArtist.translationX = currentTrackPosition + touch.latest.x - touch.first.x
        nextTrackName.translationX = currentTrackName.translationX + nextTrackPosition
        nextTrackArtist.translationX = currentTrackArtist.translationX + nextTrackPosition
        prevTrackName.translationX = currentTrackName.translationX + prevTrackPosition
        prevTrackArtist.translationX = currentTrackArtist.translationX + prevTrackPosition
    }

    fun finalizeViews() {
        refreshTrackLabels()
        resetPositions()
        resetVisibility()
    }

    fun resetPositions() {
        currentTrackName.translationX = currentTrackPosition
        currentTrackArtist.translationX = currentTrackPosition
        nextTrackName.translationX = nextTrackPosition
        nextTrackArtist.translationX = nextTrackPosition
        prevTrackName.translationX = prevTrackPosition
        prevTrackArtist.translationX = prevTrackPosition
    }

    fun resetVisibility() {
        nextTrackName.visibility = View.INVISIBLE
        nextTrackArtist.visibility = View.INVISIBLE
        prevTrackName.visibility = View.INVISIBLE
        prevTrackArtist.visibility = View.INVISIBLE
    }

    fun pushNext() {
        trackAnim?.cancel()
        activeTrackIndex++
        trackAnim = Animation.animate(currentTrackName.translationX, prevTrackPosition, 200L) {
            currentTrackName.translationX = it.value
            currentTrackArtist.translationX = it.value
            nextTrackName.translationX = it.value + nextTrackPosition
            nextTrackArtist.translationX = it.value + nextTrackPosition
            prevTrackName.translationX = it.value + prevTrackPosition
            prevTrackArtist.translationX = it.value + prevTrackPosition
            if (it.isCompleted) finalizeViews()
        }
    }

    fun pushPrev() {
        trackAnim?.cancel()
        activeTrackIndex--
        trackAnim = Animation.animate(currentTrackName.translationX, nextTrackPosition, 200L) {
            currentTrackName.translationX = it.value
            currentTrackArtist.translationX = it.value
            nextTrackName.translationX = it.value + nextTrackPosition
            nextTrackArtist.translationX = it.value + nextTrackPosition
            prevTrackName.translationX = it.value + prevTrackPosition
            prevTrackArtist.translationX = it.value + prevTrackPosition
            if (it.isCompleted) finalizeViews()
        }
    }

    fun pushBack() {
        trackAnim?.cancel()
        trackAnim = Animation.animate(currentTrackName.translationX, currentTrackPosition, 200L) {
            currentTrackName.translationX = it.value
            currentTrackArtist.translationX = it.value
            nextTrackName.translationX = it.value + nextTrackPosition
            nextTrackArtist.translationX = it.value + nextTrackPosition
            prevTrackName.translationX = it.value + prevTrackPosition
            prevTrackArtist.translationX = it.value + prevTrackPosition
            if (it.isCompleted) finalizeViews()
        }
    }

    fun refreshTrackLabels() {
        if (activeTrackIndex < 0) activeTrackIndex = tracks.lastIndex
        else if (activeTrackIndex > tracks.lastIndex) activeTrackIndex = 0

        currentTrackName.text = tracks[activeTrackIndex].name
        currentTrackArtist.text = tracks[activeTrackIndex].artist

        if (activeTrackIndex == tracks.lastIndex) {
            nextTrackName.text = tracks[0].name
            nextTrackArtist.text = tracks[0].artist
        } else {
            nextTrackName.text = tracks[activeTrackIndex + 1].name
            nextTrackArtist.text = tracks[activeTrackIndex + 1].artist
        }

        if (activeTrackIndex == 0) {
            prevTrackName.text = tracks[tracks.lastIndex].name
            prevTrackArtist.text = tracks[tracks.lastIndex].artist
        } else {
            prevTrackName.text = tracks[activeTrackIndex - 1].name
            prevTrackArtist.text = tracks[activeTrackIndex - 1].artist
        }
    }
}