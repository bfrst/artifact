package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.artifact.ArtifactComponent

class MediaCenterFragment : Fragment(), ArtifactComponent {

    override var listenForEvents: MutableList<String> = mutableListOf()


    lateinit var actions: MediaActionsFragment
    lateinit var player: MediaPlayerFragment
    lateinit var playlist: MediaPlaylistFragment

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_media_center, parent, false)

        //actions = childFragmentManager.findFragmentById(R.id.drawer_body_actions) as MediaActionsFragment
        player = childFragmentManager.findFragmentById(R.id.drawer_body_player) as MediaPlayerFragment
        //playlist = childFragmentManager.findFragmentById(R.id.drawer_body_playlist) as MediaPlaylistFragment

        return view
    }

    override fun onInitComponent() {
        // @Task initTick fragments size, visibility and etc
    }



}