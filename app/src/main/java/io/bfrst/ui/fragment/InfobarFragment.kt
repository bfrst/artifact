package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.artifact.Artifact
import io.bfrst.artifact.ArtifactComponent
import io.bfrst.artifact.ArtifactEvent
import io.bfrst.artifact.InfobarShowEvent
import kotlinx.android.synthetic.main.fragment_infobar.view.*
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

/**
 * 1) infobar.show("my example text", duration as long)
 * -- inside infobar:
 * 2) set title: "my example text"
 * 3) set duration: 200L
 * 4) create animation job and run it
 * */
// @Task краш при повторном открытии приложения: onInitComponent() view!! не существует
class InfobarFragment : Fragment(), ArtifactComponent {
    companion object {
        const val LEN_SHORT = 1500L
        const val LEN_LONG = 3000L
    }

    override var listenForEvents:   MutableList<String> = mutableListOf(InfobarShowEvent.ID)

    private lateinit var infoText:  TextView

    private var currentText:        String = ""
    private var nextText:           String = ""

    private var currentDuration:    Long = LEN_SHORT
    private var nextDuration:       Long = LEN_SHORT
    private var toggleAnimDuraiton: Long = 200L

    private var positionShown:      Float = 0.0f
    private var positionHided:      Float = 0.0f

    private var isShown:            Boolean = false

    private var delayToHideJob:     Job? = null


    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_infobar, parent, false)
        infoText = view.infobar_text
        Artifact.registerComponent(this)
        return view
    }

    override fun onInitComponent() {
        positionShown = -Artifact.Display.headerHeight.toFloat()
        view?.visibility = View.INVISIBLE
        view!!.translationY = positionHided
    }

    private fun show(text: String = currentText, duration: Long = currentDuration) {
        if (isShown) {
            delayToHideJob?.cancel()
            nextText = text
            nextDuration = duration
            hide(true)
        }
        else {
            view?.visibility = View.VISIBLE
            if (currentText != text) {
                currentText = text
                infoText.text = currentText
            }
            if (currentDuration != duration) {
                currentDuration = duration
            }
            view!!.animate().translationY(positionShown).setDuration(toggleAnimDuraiton).withEndAction {
                delayToHideJob = GlobalScope.launch(UI) {
                    delay(currentDuration)
                    hide()
                }
            }
            isShown = true
        }
    }

    /** isUpdate means that infobar already shown and we need target hide it and show right after that target update text */
    private fun hide(isUpdate: Boolean = false) {
        if (isUpdate) {
            view!!.animate().translationY(positionHided).setDuration(toggleAnimDuraiton).withEndAction {
                isShown = false
                show(nextText, nextDuration)
            }
        } else {
            view!!.animate().translationY(positionHided).setDuration(toggleAnimDuraiton).withEndAction {
                isShown = false
                view?.visibility = View.INVISIBLE
            }
        }
    }

    override fun handleEvent(event: ArtifactEvent) {
        if (event is InfobarShowEvent) {
            show(event.text, event.duration)
        }
    }
}