package io.bfrst.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import io.bfrst.R
import io.bfrst.artifact.ChangeContentScreenEvent
import io.bfrst.artifact.IEventEmitter
import kotlinx.android.synthetic.main.fragment_menu.view.*

class MenuFragment : Fragment(), IEventEmitter {

    private lateinit var musicButton    :Button
    private lateinit var settingsButton :Button
    private lateinit var accountButton  :Button

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_menu, parent, false)

        musicButton     = view.menu_music
        settingsButton  = view.menu_settings
        accountButton   = view.menu_account

        musicButton.setOnClickListener      { onMusicButtonClicked() }
        settingsButton.setOnClickListener   { onSettingsButtonClicked() }
        accountButton.setOnClickListener    { onAccountButtonClicked() }

        return view
    }

    private fun onMusicButtonClicked()      = emitEvent(ChangeContentScreenEvent(ContentFragment.Screen.Music))
    private fun onSettingsButtonClicked()   = emitEvent(ChangeContentScreenEvent(ContentFragment.Screen.Settings))
    private fun onAccountButtonClicked()    = emitEvent(ChangeContentScreenEvent(ContentFragment.Screen.Account))
    
}