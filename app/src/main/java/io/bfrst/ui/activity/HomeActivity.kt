package io.bfrst.ui.activity

import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import io.bfrst.R
import io.bfrst.VKBackend
import io.bfrst.artifact.Artifact
import io.bfrst.ui.fragment.AppbarFragment
import io.bfrst.ui.fragment.ContentFragment
import io.bfrst.ui.fragment.DrawerFragment

class HomeActivity : AppCompatActivity() {

    lateinit var webView: WebView
    lateinit var vkBackend: VKBackend

    lateinit var appbarFragment: AppbarFragment
    lateinit var contentFragment: ContentFragment
    lateinit var drawerFragment: DrawerFragment

    override fun onBackPressed() {
        when {
            appbarFragment.isInSearchMode() -> appbarFragment.disableSearch()
            drawerFragment.isOpen -> drawerFragment.pushDrawer(false, true)
            contentFragment.activeScreen != ContentFragment.Screen.Menu ->
                contentFragment.pushScreen(ContentFragment.Screen.Menu, true, true)
            else -> super.onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        appbarFragment = supportFragmentManager.findFragmentById(R.id.fragment_appbar) as AppbarFragment
        contentFragment = supportFragmentManager.findFragmentById(R.id.fragment_content) as ContentFragment
        drawerFragment = supportFragmentManager.findFragmentById(R.id.fragment_drawer) as DrawerFragment

        Artifact.init(this)
        /*webView = web_view
        webView.settings.javaScriptEnabled = true
        vkBackend = VKBackend(this.applicationContext, web_view)
        webView.webViewClient = vkBackend*/
    }
}