package io.bfrst.utils

import io.bfrst.artifact.Tickable
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.UI

// Task: add along() method that allow user to execute animation and not wait for with() and then()
// Task: Repair  easing. Web site with easing: http://www.gizma.com/easing/
// Fix: CIRC_EASE_IN doesn't work right
// Fix: all of ease_in_out doesn't work right
/** Allow you target animate float value with easing. Use Animation.animate() to get instance of Animation object,
 * do not use Animation() directly! */
class Animation(from: Float, to: Float, duration: Long, easing: EasingType, callback: (animationData: AnimationData) -> Unit) : Tickable {
    /** Holds all of data of current animation */
    private var currentAnimation: AnimationData = AnimationData(from, to, duration, easing, callback)
    /** Holds all of data for next animations that gonna be executed right after current one */
    private var nextAnimations: MutableList<AnimationData> = mutableListOf()
    /** Default easing type that gonna be used for all new animations */
    private var defaultEasingType: EasingType = DEFAULT_EASING

    /** Return true if there is no more animations to handle */
    fun isAnimationCompleted(): Boolean {
        return currentAnimation.isCompleted &&
                currentAnimation.withAnimations.filter { !it.isCompleted }.isEmpty() &&
                nextAnimations.isEmpty()
    }

    /** Returns true if current animation and "with" animations is completed */
    fun isCurrentAnimationCompleted(): Boolean {
        return currentAnimation.isCompleted &&
                currentAnimation.withAnimations.filter { !it.isCompleted }.isEmpty()
    }

    /** Returns true if next animation is exists */
    fun isNextAnimationExists(): Boolean = nextAnimations.isNotEmpty()

    /** Perform animation tick, check if animation is completed and if necessary run next animation */
    override fun tick(dt: Long) {
        handleAnimation(currentAnimation, dt)
        currentAnimation.withAnimations.forEach { handleAnimation(it, dt) }
        if (isCurrentAnimationCompleted()) {
            if (isNextAnimationExists()) {
                currentAnimation = nextAnimations.first()
                nextAnimations.removeAt(0)
            }
            else {
                cancel()
            }
        }
    }

    /**
     * Handle animation, method calculates value by passed easing function
     * @param animationData - contains data of animation and output value
     * @param dt - contains delta time of current tick
     */
    private fun handleAnimation(animationData: AnimationData, dt: Long) {
        with (animationData) {
            time += dt
            if (time >= duration) {
                value = target
                isCompleted = true
            }
            else {
                when (easing) {
                    EasingType.LINEAR -> value = linear(this)
                    EasingType.QUAD_EASE_IN -> value = quadEaseIn(this)
                    EasingType.QUAD_EASE_OUT -> value = quadEaseOut(this)
                    EasingType.QUAD_EASE_IN_OUT -> value = quadEaseInOut(this)
                    EasingType.CUBIC_EASE_IN -> value = cubicEaseIn(this)
                    EasingType.CUBIC_EASE_OUT -> value = cubicEaseOut(this)
                    EasingType.CUBIC_EASE_IN_OUT -> value = cubicEaseInOut(this)
                    EasingType.QUART_EASE_IN -> value = quartEaseIn(this)
                    EasingType.QUART_EASE_OUT -> value = quartEaseOut(this)
                    EasingType.QUART_EASE_IN_OUT -> value = quartEaseInOut(this)
                    EasingType.QUINT_EASE_IN -> value = quintEaseIn(this)
                    EasingType.QUINT_EASE_OUT -> value = quintEaseOut(this)
                    EasingType.QUINT_EASE_IN_OUT -> value = quintEaseInOut(this)
                    EasingType.SINUS_EASE_IN -> value = sinusEaseIn(this)
                    EasingType.SINUS_EASE_OUT -> value = sinusEaseOut(this)
                    EasingType.SINUS_EASE_IN_OUT -> value = sinusEaseInOut(this)
                    EasingType.EXPO_EASE_IN -> value = expoEaseIn(this)
                    EasingType.EXPO_EASE_OUT -> value = expoEaseOut(this)
                    EasingType.EXPO_EASE_IN_OUT -> value = expoEaseInOut(this)
                    EasingType.CIRC_EASE_IN -> value = circEaseIn(this)
                    EasingType.CIRC_EASE_OUT -> value = circEaseOut(this)
                    EasingType.CIRC_EASE_IN_OUT -> value = circEaseInOut(this)
                }
            }
            callback(this)
            if (isFirstTick) isFirstTick = false
        }
    }

    fun then(from: Float, to: Float, duration: Long, easing: EasingType = defaultEasingType, callback: (animData: AnimationData) -> Unit): Animation {
        nextAnimations.add(AnimationData(from, to, duration, easing, callback))
        return this
    }

    fun with(from: Float, to: Float, duration: Long, easing: EasingType = defaultEasingType, callback: (animData: AnimationData) -> Unit): Animation {
        // If we got at least one "then" animation -> add target the newest one "with" animation
        // else -> add target the "with" animations this one
        if (nextAnimations.isNotEmpty()) {
            nextAnimations.last().withAnimations.add(AnimationData(from, to, duration, easing, callback))
        }
        else {
            currentAnimation.withAnimations.add(AnimationData(from, to, duration, easing, callback))
        }
        return this
    }

    fun cancel() {
        currentAnimation.isCompleted = true
        currentAnimation.withAnimations.forEach { it.isCompleted = true }
        nextAnimations.clear()
    }

    private fun linear(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            return diff * time / duration + start
        }
    }

    private fun quadEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration
            return diff * ct * ct + start
        }
    }

    private fun quadEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration
            return -diff * ct * (ct - 2) + start
        }
    }

    private fun quadEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            var ct = time / duration / 2.0f
            if (ct < 1.0f) return diff / 2.0f * ct * ct + start
            ct -= 1.0f
            return -diff / 2.0f * (ct * (ct - 2.0f) - 1.0f) + start
        }
    }

    private fun cubicEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration
            return diff * ct * ct * ct + start
        }
    }

    private fun cubicEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration - 1
            return diff * (ct * ct * ct + 1) + start
        }
    }

    private fun cubicEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            var ct = time / duration / 2
            if (ct < 1) return diff / 2 * ct * ct * ct + start
            ct -= 2
            return diff / 2 * (ct * ct * ct + 2) + start
        }
    }

    private fun quartEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration
            return diff * ct * ct * ct * ct + start
        }
    }

    private fun quartEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration - 1
            return -diff * (ct * ct * ct * ct - 1) + start
        }
    }

    private fun quartEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            var ct = time / duration / 2
            if (ct < 1) return diff / 2 * ct * ct * ct * ct + start
            ct =- 2.0f
            return -diff / 2 * (ct * ct * ct * ct - 2) + start
        }
    }

    private fun quintEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration
            return diff * ct * ct * ct * ct * ct + start
        }
    }

    private fun quintEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration - 1
            return diff * (ct * ct * ct * ct * ct + 1) + start
        }
    }

    private fun quintEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            var ct = time / duration / 2
            if (ct < 1) return diff / 2 * ct * ct * ct * ct * ct + start
            ct -= 2.0f
            return diff / 2 * (ct * ct * ct * ct * ct + 2) + start
        }
    }

    private fun sinusEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            return (-diff * Math.cos(time / duration * (Math.PI / 2)) + diff + start).toFloat()
        }
    }

    private fun sinusEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            return (diff * Math.sin(time / duration * (Math.PI / 2)) + start).toFloat()
        }
    }

    private fun sinusEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            return (-diff / 2 * (Math.cos(Math.PI * time / duration) - 1) + start).toFloat()
        }
    }

    private fun expoEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            return (diff * Math.pow(2.0, (10 * (time / duration - 1)).toDouble()) + start).toFloat()
        }
    }

    private fun expoEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            return (diff * (-Math.pow(2.0, (-10 * time / duration).toDouble()) + 1) + start).toFloat()
        }
    }

    private fun expoEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            var ct = time / duration
            if (ct < 1) return (diff / 2 * Math.pow(2.0, (10 * (ct - 1)).toDouble()) + start).toFloat()
            ct -= 2
            return (diff / 2 * (-Math.pow(2.0, (-10 * time).toDouble()) + 2) + start).toFloat()
        }
    }

    private fun circEaseIn(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration
            return (-diff * (Math.sqrt((1 - ct * ct).toDouble())) + start).toFloat()
        }
    }

    private fun circEaseOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            val ct = time / duration - 1
            return (diff * Math.sqrt((1 - ct * ct).toDouble()) + start).toFloat()
        }
    }

    private fun circEaseInOut(animData: AnimationData): Float {
        with (animData) {
            val time = this.time.toFloat()
            val duration = this.duration.toFloat()
            var ct = time / duration
            if (ct < 1) return (-diff / 2 * (Math.sqrt((1 - ct * ct).toDouble()) - 1) + start).toFloat()
            ct -= 2.0f
            return (diff / 2 * (Math.sqrt((1 - ct * ct).toDouble()) + 1) + start).toFloat()
        }
    }

    class AnimationData(val start: Float, val target: Float, val duration: Long, val easing: EasingType, val callback: (AnimationData) -> Unit) {
        /** List of animations that should be played with main animation */
        var withAnimations: MutableList<AnimationData> = mutableListOf()
        /** Difference between start value and target value */
        val diff: Float = target - start
        /** Current time of animation execution */
        var time: Long = 0
        /** If tick() callback called first time for this animation - true, else - false */
        var isFirstTick: Boolean = true
        /** If animation has been completed - true, else - false */
        var isCompleted: Boolean = false
        /** Animated value output */
        var value: Float = start
    }

    /** Enumeration of possible easing algorithms */
    enum class EasingType {
        LINEAR,
        // Quadratic easing
        QUAD_EASE_IN,
        QUAD_EASE_OUT,
        QUAD_EASE_IN_OUT,
        // Cubic easing
        CUBIC_EASE_IN,
        CUBIC_EASE_OUT,
        CUBIC_EASE_IN_OUT,
        // Quartic easing
        QUART_EASE_IN,
        QUART_EASE_OUT,
        QUART_EASE_IN_OUT,
        // Quintic easing
        QUINT_EASE_IN,
        QUINT_EASE_OUT,
        QUINT_EASE_IN_OUT,
        // Sinusoidal easing
        SINUS_EASE_IN,
        SINUS_EASE_OUT,
        SINUS_EASE_IN_OUT,
        // Exponential easing
        EXPO_EASE_IN,
        EXPO_EASE_OUT,
        EXPO_EASE_IN_OUT,
        // Circular easing
        CIRC_EASE_IN,
        CIRC_EASE_OUT,
        CIRC_EASE_IN_OUT
    }

    companion object {
        val DEFAULT_EASING: EasingType = EasingType.QUART_EASE_OUT
        /** Delay between tick() calls. Less value - more smooth animation, but more CPU consumption. */
        private const val TICK_RATE = 4L
        /** Max count of possible animations at the same time */
        private const val MAX_ANIMS = 50
        /** Job that perform tick() call on animations every TICK_RATE milliseconds */
        private var tickJob: Job? = null
        /** List of animations (contains even completed animations) */
        private var animations: MutableList<Animation> = mutableListOf()

        /**
         * Creates an instance of animation, puts it in list of animations, returns reference of the
         * instantiated animation target the user and initializes tickJob if necessary
         */
        fun animate(start: Float, target: Float, duration: Long, easing: EasingType = DEFAULT_EASING, callback: (animationData: AnimationData) -> Unit): Animation {
            if (tickJob == null) tickJob = GlobalScope.launch(UI) {
                while (this.isActive) {
                    if (animations.size > MAX_ANIMS) animations.removeAt(0)
                    delay(TICK_RATE)
                    animations.filter { !it.isCurrentAnimationCompleted() }.forEach { it.tick(TICK_RATE) }
                }
            }

            val animation = Animation(start, target, duration, easing, callback)
            animations.add(animation)
            return animation
        }
    }
}