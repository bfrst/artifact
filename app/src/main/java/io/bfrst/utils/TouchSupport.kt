package io.bfrst.utils

import android.view.View
import io.bfrst.artifact.Artifact
import kotlin.math.absoluteValue

/**
 * Класс-помошник для работы с касаниями пользователя
 * */
class TouchSupport {
    private var isDebug: Boolean = false
    private var differencesX: MutableList<Float> = mutableListOf()
    private var differencesY: MutableList<Float> = mutableListOf()
    private var differencesVectorSize: Int = 3
    var gesture: Touch.Gesture = Touch.Gesture.NONE
    var latest: Touch = Touch()
    var first: Touch = Touch()

    val differenceX: Float get() { return latest.x - first.x }
    val differenceY: Float get() { return latest.y - first.y }

    var accelPeakX: Float = 0.0f
    val accelX: Float
        get() {
            var accel: Float = 0.0f
            differencesX.forEach { accel += it }
            if (accel.absoluteValue > accelPeakX.absoluteValue) accelPeakX = accel
            return accel
        }

    var accelPeakY: Float = 0.0f
    val accelY: Float
        get() {
            var accel: Float = 0.0f
            differencesY.forEach { accel += it }
            if (accel.absoluteValue > accelPeakY.absoluteValue) accelPeakY = accel
            return accel
        }

    var moveHSens: Float = -1.0f
        get() {
            if (field <= 0.0f && Artifact.Display.screenDensity > 0)
                field = GestureSettings.moveHSens * Artifact.Display.screenDensity
            return field
        }

    var moveVSens: Float = -1.0f
        get() {
            if (field <= 0.0f && Artifact.Display.screenDensity > 0)
                field = GestureSettings.moveVSens * Artifact.Display.screenDensity
            return field
        }

    var swipeHSens: Float = -1.0f
        get() {
            if (field <= 0.0f && Artifact.Display.screenDensity > 0)
                field = GestureSettings.swipeHSens * Artifact.Display.screenDensity
            return field
        }

    var swipeVSens: Float = -1.0f
        get() {
            if (field <= 0.0f && Artifact.Display.screenDensity > 0)
                field = GestureSettings.swipeVSens * Artifact.Display.screenDensity
            return field
        }

    var holdTimeSens: Long = GestureSettings.holdTimeSens

    fun handleTouch(touch: Touch) {
        if (touch.action == Touch.Action.MOVE) calculateDifference(touch)
        else if (touch.action == Touch.Action.START) {
            first = touch
            differencesX.clear()
            differencesY.clear()
        }
        latest = touch
        calculateGesture()
        if (isDebug) printDebugInfo()
    }

    fun debugOn() { isDebug = true }
    fun debugOff() { isDebug = false }
    fun printDebugInfo() {
        println("TOUCH ${latest.action} (${latest.x} : ${latest.y}) | first - latest diff " +
                "($differenceX : $differenceY) | accel ($accelX : $accelY) | accel peaks ($accelPeakX : $accelPeakY)")
        println("TOUCH GESTURE ($gesture) | move sensivity ($moveHSens : $moveVSens) | swipe sensivity ($swipeHSens : $swipeVSens)")
    }

    private fun calculateDifference(touch: Touch) {
        val differenceX = touch.x - latest.x
        val differenceY = touch.y - latest.y
        differencesX.add(differenceX)
        differencesY.add(differenceY)
        if (differencesX.size > differencesVectorSize) differencesX.removeAt(0)
        if (differencesY.size > differencesVectorSize) differencesY.removeAt(0)
    }

    private fun calculateGesture() {
        if (latest.action == Touch.Action.MOVE && gesture == Touch.Gesture.NONE) {
            if ((first.y - latest.y).absoluteValue > moveVSens) gesture = Touch.Gesture.MOVE_VERTICAL
            else if ((first.x - latest.x).absoluteValue > moveHSens) gesture = Touch.Gesture.MOVE_HORIZONTAL
        }
        else if (latest.action == Touch.Action.END) {
            if (accelX < -swipeHSens) gesture = Touch.Gesture.SWIPE_LEFT
            else if (accelX > swipeHSens) gesture = Touch.Gesture.SWIPE_RIGHT
            else if (accelY < -swipeVSens) gesture = Touch.Gesture.SWIPE_UP
            else if (accelY > swipeVSens) gesture = Touch.Gesture.SWIPE_DOWN
        }
        else if (latest.action == Touch.Action.START) {
            gesture = Touch.Gesture.NONE
        }
    }

    fun isInBoundsOfView(view: View): Boolean {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        return latest.x > location[0] && latest.x < location[0] + view.width - 1 &&
                latest.y > location[1] && latest.y < location[1] + view.height
    }
}