package io.bfrst.utils

import android.util.Log
import android.view.MotionEvent

class Touch(val x: Float = 0.0f, val y:Float = 0.0f, val action: Action = Action.START, val time: Long = 0) {
    enum class Gesture {
        NONE,
        MOVE_VERTICAL,
        MOVE_HORIZONTAL,
        SWIPE_UP,
        SWIPE_DOWN,
        SWIPE_LEFT,
        SWIPE_RIGHT
    }

    enum class Action {
        START,
        MOVE,
        END
    }

    companion object {
        fun fromMotionEvent(motionEvent: MotionEvent): Touch {
            val action: Action
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> action = Action.START
                MotionEvent.ACTION_MOVE -> action = Action.MOVE
                MotionEvent.ACTION_UP -> action = Action.END
                else -> {
                    // Task handle this situation
                    action = Action.START
                }
            }
            return Touch(motionEvent.rawX, motionEvent.rawY, action, motionEvent.eventTime)
        }
    }
}