package io.bfrst.utils

object GestureSettings {
    const val moveHSens     :Float = 15.0f
    const val swipeHSens    :Float = 10.0f

    const val moveVSens     :Float = 20.0f
    const val swipeVSens    :Float = 15.0f

    const val holdTimeSens  :Long = 1000
}