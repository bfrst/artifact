package io.bfrst.utils

fun trace(message: String) {
    println("[ARTIFACT] (${System.currentTimeMillis()}): $message")
}

fun warn(message: String) {
    println("[ARTIFACT] !!! (WARNING) (${System.currentTimeMillis()}): $message")
}