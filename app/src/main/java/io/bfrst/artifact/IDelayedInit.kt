package io.bfrst.artifact

interface IDelayedInit {
    fun delayedInit()
    fun registerDelayedInit() {
        Artifact.registerDelayedInit(this)
    }
}