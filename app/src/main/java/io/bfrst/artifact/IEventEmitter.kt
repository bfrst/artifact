package io.bfrst.artifact

interface IEventEmitter {
    fun emitEvent(event: ArtifactEvent) {
        Artifact.emitEvent(event)
    }
}