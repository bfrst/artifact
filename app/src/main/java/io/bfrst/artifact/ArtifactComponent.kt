package io.bfrst.artifact

interface ArtifactComponent {
    var listenForEvents: MutableList<String>
    fun onInitComponent() {}
    fun handleEvent(event: ArtifactEvent) {}
    fun fireEvent(event: ArtifactEvent) {
        Artifact.fireEvent(event)
    }
}