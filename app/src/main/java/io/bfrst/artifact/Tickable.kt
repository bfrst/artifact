package io.bfrst.artifact

interface Tickable {
    fun tick(dt: Long)
}