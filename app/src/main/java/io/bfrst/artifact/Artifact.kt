package io.bfrst.artifact

import android.app.Activity
import android.media.Image
import io.bfrst.R
import io.bfrst.data.Track
import io.bfrst.ui.fragment.PlayerMiniFragment
import io.bfrst.utils.trace

object Artifact {
    // ---------- V2 stuff ----------
    fun registerListener(listener: IEventListener) {
        eventListeners.add(listener)
    }

    fun emitEvent(event: ArtifactEvent) {
        eventListeners.forEach { if (it.listenEvents.contains(event.id)) it.handleEvent(event) }
    }

    // ---------- V1 stuff ----------

    var activeTrackIndex: Int = 0
    // @Task pull Track class out of fragment class
    var tracks: List<Track> = listOf(
            Track("anacondaz_album", "Дубак", "Anacondaz"),
            Track("anacondaz_album", "Твоему новому парню", "Anacondaz"),
            Track("anacondaz_album", "Драма", "Anacondaz"),
            Track("ember_j", "Someting more", "ember j"),
            Track("kasabian", "In love with psycho", "Kasabian"),
            Track("the_aces", "Waiting for you", "The Aces"),
            Track("top_trench", "Morph", "Twenty One Pilots"),
            Track("top_trench", "Cut My Lip", "Twenty One Pilots"),
            Track("top_trench", "The Hype", "Twenty One Pilots")
    )

    private var components: MutableList<ArtifactComponent> = mutableListOf()
    private var eventListeners: MutableList<IEventListener> = mutableListOf()
    private var delayedInitItems: MutableList<IDelayedInit> = mutableListOf()
    private var isInitialized: Boolean = false

    fun fireEvent(event: ArtifactEvent) {
        components.forEach {
            if (it.listenForEvents.contains(event.id)) it.handleEvent(event)
        }
    }

    fun registerComponent(component: ArtifactComponent) {
        components.add(component)
        if (isInitialized) component.onInitComponent()
    }

    fun registerDelayedInit(delayedInitItem: IDelayedInit) {
        delayedInitItems.add(delayedInitItem)
        if (isInitialized) delayedInitItem.delayedInit()
    }

    fun init(act: Activity) {
        with (Display) {
            screenHeight = act.resources.displayMetrics.heightPixels.toFloat()
            screenWidth = act.resources.displayMetrics.widthPixels.toFloat()
            screenDensity = act.resources.displayMetrics.density
            val resourceId = act.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) statusBarHeight = act.resources.getDimensionPixelSize(resourceId).toFloat()
            headerHeight = act.resources.getDimensionPixelSize(R.dimen.header_height).toFloat()
            infobarHeight = act.resources.getDimensionPixelSize(R.dimen.infobar_height).toFloat()
            sceneWidth = screenWidth
            sceneHeight = screenHeight - statusBarHeight - headerHeight
            traceDebugInfo()
            components.forEach {
                it.onInitComponent()
            }
            delayedInitItems.forEach { it.delayedInit() }
        }
    }

    object Display {
        var screenDensity       = -1.0f
        var screenWidth         = -1.0f
        var screenHeight        = -1.0f
        var sceneWidth          = -1.0f
        var sceneHeight         = -1.0f
        var statusBarHeight     = -1.0f
        var headerHeight        = -1.0f
        var infobarHeight       = -1.0f
        fun traceDebugInfo() {
            trace("HWD ($screenHeight, $screenWidth, $screenDensity), " +
                    "status and header ($statusBarHeight, $headerHeight), " +
                    "scene ($sceneWidth, $sceneHeight), " +
                    "infobar ($infobarHeight)")
        }
    }
}