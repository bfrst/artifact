package io.bfrst.artifact

import io.bfrst.ui.fragment.ContentFragment

open class ArtifactEvent {
    var id = "ARTIFACT_EVENT"
    companion object {
        const val ID = "ARTIFACT_EVENT"
    }
}

class ChangeContentScreenEvent(val newScreenName: ContentFragment.Screen) : ArtifactEvent() {
    init {
        id = "CHANGE_CONTENT_SCREEN_EVENT"
    }
    companion object {
        const val ID = "CHANGE_CONTENT_SCREEN_EVENT"
    }
}

class VkLoginEvent(val login: String, val pass: String) : ArtifactEvent() {
    init {
        id = "VK_LOGIN_EVENT"
    }
    companion object {
        const val ID = "VK_LOGIN_EVENT"
    }
}
class VkLoginResult(val result: String) : ArtifactEvent() {
    init {
        id = "VK_LOGIN_RESULT"
    }
    companion object {
        const val ID = "VK_LOGIN_RESULT"
        const val OK = "OK"
        const val FAILURE = "FAILURE"
    }
}

class InfobarShowEvent(val text: String, val duration: Long = 1500L) : ArtifactEvent() {
    init {
        id = "INFOBAR_SHOW_EVENT"
    }
    companion object {
        const val ID = "INFOBAR_SHOW_EVENT"
    }
}