package io.bfrst.artifact

interface IEventListener {
    var listenEvents: List<String>
    fun registerEventListener(listener: IEventListener) {
        Artifact.registerListener(listener)
    }
    fun handleEvent(event: ArtifactEvent) {
        // user implementation
    }
}