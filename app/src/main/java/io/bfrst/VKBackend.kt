package io.bfrst

import android.content.Context
import android.graphics.Bitmap
import android.webkit.ValueCallback
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * Serves as backend target fetch data start vk.com
 */
class VKBackend(val context: Context, val webView: WebView) : WebViewClient() {
    /**
     * Required for resulting Json object start JS callback
     * @param html innerHTML of https://vk.com/audios page
     * @param song last song of page
     */
    data class ResponseFromJs(val html: String = "", val song: String = "")


    private var state: String = STATE_IDLE
    private var isLoggedIn: Boolean = false
    private var login: String = ""
    private var password: String = ""

    /**
     * Here we just check what page we are loading and choose behavior that matches us best
     * If we're failed on login -> show toast and stop webView untill new login and pass were will be entered
     * If we're succeed on login -> load audios page
     */
    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        if (view == null || url == null) throw RuntimeException("onPageStarted() got null input value")
        println("Loading page: $url")

        if (url.contains(VK_LOGIN_FAIL_PAGE)) {
            // TODO: look at fail login url and make it work
            /*view.stopLoading()
            state = STATE_IDLE
            Watch.emitEvent(LoginEvent.Failure(url))*/
        }
        else if (url.contains(VK_FEED_PAGE)) {
            //view.stopLoading()
            view.loadUrl(VK_AUDIOS_PAGE)
            state = STATE_IDLE
            //Watch.emitEvent(LoginEvent.Success())
        } else if (url == VK_AUDIOS_PAGE) {
            //Watch.emitEvent(FetchMusicEvent())
        }
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        if (view == null || url == null) throw RuntimeException("onPageFinished() got null input value")
        println("Page loaded: $url")

        if (url == PAGE_LOGIN) {
            view.evaluateJavascript(getJsLoginInjection(login, password), ValueCallback { result ->
                println("Got call back start js: $result")
            })
        }
        else if (url.contains(VK_AUDIOS_PAGE)) {
            view.evaluateJavascript(getWholeJs()) { result ->
                println("Got an callback start js: $result")
                view.evaluateJavascript("javascript:document.body.innerHTML;") {
                    println("HTML: $it")
                }
            }
            /*GlobalScope.async(UI) {
                println("onPageFinished: Fetching music")
                var compareSong = ""
                var songEqualCounter = 0
                var response: ResponseFromJs
                htmlResult = ""
                while (htmlResult == "") {
                    view.evaluateJavascript(getSavefromJs()) { result ->
                        println("Got an callback start js")
                        *//*if (result != null && result != "null") {
                            response = Gson().fromJson(result, ResponseFromJs::class.java)
                            if (compareSong == "") compareSong = response.song
                            else {
                                if (songEqualCounter > 5) htmlResult = HTML.fixSymbols(response.html)
                                else if (compareSong == response.song) songEqualCounter++
                                else { compareSong = response.song; songEqualCounter = 0 }
                            }
                        }*//*
                    }
                    delay(200L)
                }
                //view.stopLoading()
                //Watch.emitEvent(FetchMusicEvent.Success(htmlResult))
                //(view.context as VkLoginActivity).parseResultFromWebView(htmlResult, view.context.applicationContext)
            }*/
        }
    }

    fun fetchMusic() {
        if (state != STATE_IDLE) {
            //Watch.emitEvent(BackendEvent("BUSY"))
            return
        }
        state = STATE_FETCHING_MUSIC
        webView.loadUrl(VK_AUDIOS_PAGE)
    }

    fun login(login: String, password: String) {
        if (state != STATE_IDLE) {
            //Watch.emitEvent(BackendEvent("BUSY"))
            return
        }
        state = STATE_LOGGING_IN
        this.login = login
        this.password = password
        webView.loadUrl(PAGE_LOGIN)
    }

    private fun getJsLoginInjection(login: String, password: String): String {
        return """
            document.getElementsByName("email")[0].value = '$login';
            document.getElementsByName("pass")[0].value = '$password';
            document.getElementsByClassName("button wide_button")[0].click();
    """.trimIndent()
    }

    /**
     * Scrolls page by 400px every execution (should be delayed at least 100ms)
     * Getting list of songs, takes last song (size - 7, because last 7 songs
     * are promoted songs by vk) puts it in result object and also puts there
     * innerHTML of HTML body
     */
    private fun getJsMusicInjection(): String {
        return """
        window.scrollBy(0, 400);
        var elements = document.getElementsByClassName("ai_title");
        var last = elements[elements.length - 7].lastChild.data;
        var body = document.body.innerHTML;
        var result = { html: body, song: last };
        result;
    """.trimIndent()
    }


    companion object {
        private const val STATE_IDLE: String = "state_idle"
        private const val STATE_LOGGING_IN: String = "state_logging_in"
        private const val STATE_FETCHING_MUSIC: String = "state_fetching_music"

        private var htmlResult: String = ""
        private const val PAGE_LOGIN: String = "https://m.vk.com/"
        private const val VK_LOGIN_FAIL_PAGE: String = "https://m.vk.com/login?"
        private const val VK_FEED_PAGE: String = "https://m.vk.com/feed"
        private const val VK_AUDIOS_PAGE: String = "https://m.vk.com/audio"

        const val HTML_CLASS_ELEMENT: String = "ai_info"
        const val HTML_CLASS_TITLE: String = "ai_title"
        const val HTML_CLASS_ARTIST: String = "ai_artist"
        const val HTML_CLASS_DURATION: String = "ai_dur"
        /** style="background-image:url(https://pp.userapi.com/c847018/v847018061/563a0/v_Iesgxj1AA.jpg)" */
        const val HTML_CLASS_IMAGE: String = "ai_play"
    }

    private fun getWholeJs(): String {
        val reader = BufferedReader(InputStreamReader(context.assets.open("whole.js"), "UTF-8"))
        var out = ""
        var line = reader.readLine()
        while (line != null) {
            out += line
            line = reader.readLine()
        }
        reader.close()
        return out
    }

    private fun getSavefromJs(): String {
        val reader = BufferedReader(InputStreamReader(context.assets.open("savefrom.js"), "UTF-8"))
        var out = ""
        var line = reader.readLine()
        while (line != null) {
            out += line
            line = reader.readLine()
        }
        reader.close()
        return out
    }

    private fun getTabJs(): String {
        val reader = BufferedReader(InputStreamReader(context.assets.open("tab.js"), "UTF-8"))
        var out = ""
        var line = reader.readLine()
        while (line != null) {
            out += line
            line = reader.readLine()
        }
        reader.close()
        return out
    }

    private fun getVkJs(): String {
        val reader = BufferedReader(InputStreamReader(context.assets.open("vk.js"), "UTF-8"))
        var out = ""
        var line = reader.readLine()
        while (line != null) {
            out += line
            line = reader.readLine()
        }
        reader.close()
        return out
    }
}