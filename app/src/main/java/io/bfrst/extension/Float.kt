package io.bfrst.extension

fun Float.normalize(start: Float, target: Float): Float {
    return (this - start) / (target - start)
}