package io.bfrst.extension

import android.view.View
import kotlinx.coroutines.experimental.*

var View.scale: Float
    get() {
        return this.scaleX
    }
    set(value) {
        this.scaleX = value
        this.scaleY = value
    }