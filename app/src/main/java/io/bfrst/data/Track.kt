package io.bfrst.data

import android.media.Image

class Track(val assetName: String, val name: String, val artist: String)