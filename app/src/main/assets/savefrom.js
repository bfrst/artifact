!window.savefromContentScriptWebpackJsonp && (window.savefromContentScriptWebpackJsonp = window.savefromContentScriptWebpackJsonp || []).push([ [ 0 ], [ function(e, t, n) {
    "use strict";
    var o = class {
        constructor() {
            this.listeners = [];
        }
        addListener(e) {
            -1 === this.listeners.indexOf(e) && this.listeners.push(e);
        }
        dispatch() {
            for (var e = arguments.length, t = new Array(e), n = 0; n < e; n++) t[n] = arguments[n];
            this.listeners.forEach(e => {
                e(...t);
            });
        }
        hasListener(e) {
            return -1 !== this.listeners.indexOf(e);
        }
        hasListeners() {
            return this.listeners.length > 0;
        }
        removeListener(e) {
            var t = this.listeners.indexOf(e);
            -1 !== t && this.listeners.splice(t, 1);
        }
    }, i = n(9), r = Object(i.a)("mono");
    var a = class {
        constructor() {
            this.onDestroy = new o(), this._lastErrorFired = !1, this._lastError = null;
        }
        get lastError() {
            return this._lastErrorFired = !0, this._lastError;
        }
        set lastError(e) {
            this._lastErrorFired = !e, this._lastError = e;
        }
        clearLastError() {
            this._lastError && !this._lastErrorFired && r.error("Unhandled mono.lastError error:", this.lastError),
            this._lastError = null;
        }
        unimplemented() {
            throw new Error("Unimplemented");
        }
        destroy() {
            this.onDestroy.dispatch();
        }
    }, s = n(25), l = e => (class extends e {
        callFn(e, t) {
            return this.waitPromise({
                action: "callFn",
                fn: e,
                args: t
            });
        }
        waitPromise(e) {
            return new Promise((t, n) => {
                this.sendMessage(e, e => {
                    if (e) {
                        if (e.err) {
                            var o = s(e.err);
                            return n(o);
                        }
                        return t(e.result);
                    }
                    var i = this.lastError || new Error("Unexpected response");
                    return n(i);
                });
            });
        }
    }), d = e => (class extends e {}), u = e => (class extends(d(e)){});
    var c = class extends(u(l(a))){
        initMessages() {
            this.sendMessage = this.transport.sendMessage.bind(this.transport), this.onMessage = {
                addListener: this.transport.addListener.bind(this.transport),
                hasListener: this.transport.hasListener.bind(this.transport),
                hasListeners: this.transport.hasListeners.bind(this.transport),
                removeListener: this.transport.removeListener.bind(this.transport)
            };
        }
    };
    var p = class {
        constructor(e) {
            this.mono = e, this.onChanged = {
                addListener: e => {
                    browser.storage.onChanged.addListener(e);
                },
                hasListener: e => browser.storage.onChanged.hasListener(e),
                hasListeners: () => browser.storage.onChanged.hasListeners(),
                removeListener: e => {
                    browser.storage.onChanged.removeListener(e);
                }
            };
        }
        callback(e, t, n) {
            this.mono.lastError = browser.runtime.lastError, (n || e) && e(t), this.mono.clearLastError();
        }
        get(e, t) {
            browser.storage.local.get(e, e => this.callback(t, e, !0));
        }
        set(e, t) {
            browser.storage.local.set(e, () => this.callback(t));
        }
        remove(e, t) {
            browser.storage.local.remove(e, () => this.callback(t));
        }
        clear(e) {
            browser.storage.local.clear(() => this.callback(e));
        }
    }, f = e => (class extends e {
        constructor() {
            super(), this.isFirefox = !0;
        }
        get isFirefoxMobile() {
            return /(?:Mobile|Tablet);/.test(navigator.userAgent);
        }
    }), h = e => (class extends(f(e)){});
    var m = new class extends(h(c)){
        constructor() {
            super(), this.initMessages(), this.initStorage(), this.initI18n();
        }
        initI18n() {
            this.i18n = {
                getMessage: browser.i18n.getMessage.bind(browser.i18n)
            };
        }
        initMessages() {
            this.transport = {
                sendMessage: (e, t) => {
                    t ? browser.runtime.sendMessage(e, e => {
                        this.lastError = browser.runtime.lastError, t(e), this.clearLastError();
                    }) : browser.runtime.sendMessage(e);
                },
                addListener: e => {
                    browser.runtime.onMessage.addListener(e);
                },
                hasListener: e => browser.runtime.onMessage.hasListener(e),
                hasListeners: () => browser.runtime.onMessage.hasListeners(),
                removeListener: e => {
                    browser.runtime.onMessage.removeListener(e);
                }
            }, super.initMessages();
        }
        initStorage() {
            this.storage = new p(this);
        }
    }();
    t.a = m;
}, function(e, t, n) {
    "use strict";
    var o = n(2), i = {
        create: function(e, t) {
            var n, o;
            for (var i in n = "object" != typeof e ? document.createElement(e) : e, t) {
                var a = t[i];
                (o = r[i]) ? o(n, a) : n[i] = a;
            }
            return n;
        }
    }, r = {
        text: function(e, t) {
            e.textContent = t;
        },
        data: function(e, t) {
            for (var n in t) e.dataset[n] = t[n];
        },
        class: function(e, t) {
            if (Array.isArray(t)) for (var n = 0, o = t.length; n < o; n++) e.classList.add(t[n]); else e.setAttribute("class", t);
        },
        style: function(e, t) {
            if ("object" == typeof t) for (var n in t) {
                var o = n;
                "float" === o && (o = "cssFloat");
                var i = t[n];
                if (Array.isArray(i)) for (var r = 0, a = i.length; r < a; r++) e.style[o] = i[r]; else e.style[o] = i;
            } else e.setAttribute("style", t);
        },
        append: function(e, t) {
            Array.isArray(t) || (t = [ t ]);
            for (var n = 0, o = t.length; n < o; n++) {
                var i = t[n];
                (i || 0 === i) && ("object" != typeof i && (i = document.createTextNode(i)), e.appendChild(i));
            }
        },
        on: function(e, t) {
            "object" != typeof t[0] && (t = [ t ]);
            for (var n = 0, i = t.length; n < i; n++) {
                var r = t[n];
                Array.isArray(r) && o.a.on.apply(o.a, [ e ].concat(r));
            }
        },
        one: function(e, t) {
            "object" != typeof t[0] && (t = [ t ]);
            for (var n = 0, i = t.length; n < i; n++) {
                var r = t[n];
                Array.isArray(r) && o.a.one.apply(o.a, [ e ].concat(r));
            }
        },
        onCreate: function(e, t) {
            t.call(e, e);
        },
        attr: function(e, t) {
            var n, o;
            for (n in t) o = t[n], e.setAttribute(n, o);
        }
    };
    t.a = i;
}, function(e, t, n) {
    "use strict";
    var o = {
        on: function(e, t, n, o) {
            e.addEventListener(t, n, o);
        },
        off: function(e, t, n, o) {
            e.removeEventListener(t, n, o);
        },
        one: function(e, t, n, i) {
            var r = [ "oneFn", t, !!i ].join("_"), a = n[r];
            a || (n[r] = a = function(e) {
                o.off(this, t, a, i), n.apply(this, arguments);
            }), o.on(e, t, a, i), e = null;
        }
    }, i = "sf-removed-" + Math.floor(1e6 * Math.random()), r = "sf-notify-on-remove-" + Math.floor(1e6 * Math.random());
    o.onRemoveEventName = i, o.onRemoveClassName = r, o.onRemoveListener = function(e) {
        o.trigger(e, i);
    }, o.onRemoveEvent = ((e, t) => {
        e.classList.add(r), e.addEventListener(i, t);
    }), o.offRemoveEvent = function(e, t) {
        e.removeEventListener(o.onRemoveEventName, t);
    }, o.trigger = function(e, t, n) {
        void 0 === n && (n = {}), void 0 === n.bubbles && (n.bubbles = !1), void 0 === n.cancelable && (n.cancelable = !1);
        var o = null;
        o = "function" == typeof MouseEvent && -1 !== [ "click" ].indexOf(t) ? new MouseEvent(t, n) : new CustomEvent(t, n),
        e.dispatchEvent(o);
    }, t.a = o;
}, function(e, t, n) {
    "use strict";
    var o = (e, t) => {
        var n = [];
        Array.isArray(e) || (e = [ e ]), t && !Array.isArray(t) && (t = [ t ]);
        var i = function(e, t) {
            var n = [];
            for (var o in t) {
                var i = t[o];
                "cssFloat" === o && (o = "float");
                var r = o.replace(/([A-Z])/g, function(e, t) {
                    return "-" + t.toLowerCase();
                });
                n.push(r + ":" + i);
            }
            return n.length ? [ e.join(","), "{", n.join(";"), "}" ].join("") : "";
        }, r = function(e, n) {
            if (Array.isArray(n) || (n = [ n ]), t) {
                var o = [], i = e.join || "" === e.join ? e.join : " ";
                t.forEach(function(e) {
                    n.forEach(function(t) {
                        o.push(e + i + t);
                    });
                }), n = o;
            }
            return n;
        };
        return e.forEach(function(e) {
            var a = null, s = e.media, l = e.selector, d = e.style, u = e.append;
            if (s && u) n.push([ s, "{", o(u, t), "}" ].join("")); else if (l || d) a = r(e, l),
            n.push(i(a, d)), u && n.push(o(u, a)); else for (var c in e) -1 === [ "append", "join" ].indexOf(c) && (l = c,
            (u = (d = e[c]).append) && delete d.append, a = r(e, l), n.push(i(a, d)), u && n.push(o(u, a)));
        }), n.join("");
    };
    t.a = o;
}, function(e, t, n) {
    "use strict";
    var o = n(22), i = {
        maxLength: 80,
        rtrim: /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        illegalRe: /[\/?<>\\:*|"~\u202B]/g,
        controlRe: /[\x00-\x1f\x80-\x9f]/g,
        zeroWidthJoinerRe: /\u200D/g,
        reservedRe: /^\.+/,
        trim: function(e) {
            return e.replace(this.rtrim, "");
        },
        partsRe: /^(.+)\.([a-z0-9]{1,4})$/i,
        getParts: function(e) {
            return e.match(this.partsRe);
        },
        specialChars: "nbsp,iexcl,cent,pound,curren,yen,brvbar,sect,uml,copy,ordf,laquo,not,shy,reg,macr,deg,plusmn,sup2,sup3,acute,micro,para,middot,cedil,sup1,ordm,raquo,frac14,frac12,frac34,iquest,Agrave,Aacute,Acirc,Atilde,Auml,Aring,AElig,Ccedil,Egrave,Eacute,Ecirc,Euml,Igrave,Iacute,Icirc,Iuml,ETH,Ntilde,Ograve,Oacute,Ocirc,Otilde,Ouml,times,Oslash,Ugrave,Uacute,Ucirc,Uuml,Yacute,THORN,szlig,agrave,aacute,acirc,atilde,auml,aring,aelig,ccedil,egrave,eacute,ecirc,euml,igrave,iacute,icirc,iuml,eth,ntilde,ograve,oacute,ocirc,otilde,ouml,divide,oslash,ugrave,uacute,ucirc,uuml,yacute,thorn,yuml".split(","),
        specialCharsList: [ [ "amp", "quot", "lt", "gt" ], [ 38, 34, 60, 62 ] ],
        specialCharsRe: /&([^;]{2,6});/g,
        decodeSpecialChars: function(e) {
            var t = this;
            return e.replace(this.specialCharsRe, function(e, n) {
                var o = null;
                if ("#" === n[0]) return o = parseInt(n.substr(1)), isNaN(o) ? "" : String.fromCharCode(o);
                var i = t.specialCharsList[0].indexOf(n);
                return -1 !== i ? (o = t.specialCharsList[1][i], String.fromCharCode(o)) : -1 !== (i = t.specialChars.indexOf(n)) ? (o = i + 160,
                String.fromCharCode(o)) : "";
            });
        },
        decodeHexChars: function(e) {
            return e.replace(/(\\x[a-zA-Z0-9]{2})/g, function(e, t) {
                var n = t;
                try {
                    n = String.fromCharCode(parseInt("0x" + n.substr(2), 16));
                } catch (e) {}
                return n;
            });
        },
        rnRe: /\r?\n/g,
        re1: /[*?"]/g,
        re2: /</g,
        re3: />/g,
        spaceRe: /[\s\t\uFEFF\xA0]+/g,
        dblRe: /(\.|!|\?|_|,|-|:|\+){2,}/g,
        re4: /[.,:;\/\-_+=']$/g,
        modify: function(e) {
            if (!e) return "";
            e = Object(o.a)(e);
            try {
                e = decodeURIComponent(e);
            } catch (t) {
                e = unescape(e);
            }
            if (e = (e = this.decodeSpecialChars(e)).replace(this.rnRe, " "), (e = (e = this.trim(e)).replace(this.zeroWidthJoinerRe, "").replace(this.re1, "").replace(this.re2, "(").replace(this.re2, "(").replace(this.re3, ")").replace(this.spaceRe, " ").replace(this.dblRe, "$1").replace(this.illegalRe, "_").replace(this.controlRe, "").replace(this.reservedRe, "").replace(this.re4, "")).length > this.maxLength) {
                var t = this.getParts(e);
                t && 3 == t.length && (t[1] = t[1].substr(0, this.maxLength), e = t[1] + "." + t[2]);
            }
            return this.trim(e);
        }
    };
    t.a = i;
}, function(e, t, n) {
    "use strict";
    t.a = (e => "data-" + e.replace(/[A-Z]/g, function(e) {
        return "-" + e.toLowerCase();
    }));
}, function(e, t, n) {
    "use strict";
    n.d(t, "b", function() {
        return a;
    });
    var o = n(0), i = [];
    o.a.onMessage.addListener((e, t, n) => {
        e && "getLoadedModules" === e.action && (e.url && e.url !== location.href || n(i));
    });
    var r = (e, t, n) => Promise.resolve().then(() => !n || n()).then(n => {
        n && (-1 === i.indexOf(e) && i.push(e), t());
    }), a = (e, t, n) => r(e, () => o.a.callFn("getPreferences").then(n => {
        t(e, {
            preferences: n
        });
    }), n);
    t.a = r;
}, function(e, t, n) {
    "use strict";
    var o = function(e, t) {
        var n = document.createElement("div");
        return o = "function" == typeof n.matches ? function(e, t) {
            return e.matches(t);
        } : "function" == typeof n.matchesSelector ? function(e, t) {
            return e.matchesSelector(t);
        } : "function" == typeof n.webkitMatchesSelector ? function(e, t) {
            return e.webkitMatchesSelector(t);
        } : "function" == typeof n.mozMatchesSelector ? function(e, t) {
            return e.mozMatchesSelector(t);
        } : "function" == typeof n.oMatchesSelector ? function(e, t) {
            return e.oMatchesSelector(t);
        } : "function" == typeof n.msMatchesSelector ? function(e, t) {
            return e.msMatchesSelector(t);
        } : function(e, t) {
            return !1;
        }, n = null, o(e, t);
    };
    t.a = function(e, t) {
        return o(e, t);
    };
}, function(e, t, n) {
    "use strict";
    var o = n(7);
    t.a = function(e, t) {
        if (!e || 1 !== e.nodeType) return null;
        if (Object(o.a)(e, t)) return e;
        if (!Object(o.a)(e, t + " " + e.tagName)) return null;
        for (var n = e = e.parentNode; n; n = n.parentNode) {
            if (1 !== n.nodeType) return null;
            if (Object(o.a)(n, t)) return n;
        }
        return null;
    };
}, function(e, t, n) {
    "use strict";
    t.a = (e => {
        var t = null;
        return (t = (() => {})).log = t.info = t.warn = t.error = t.debug = t, t;
    });
}, function(e, t, n) {
    "use strict";
    var o = n(9), i = Object(o.a)("extensionMarker"), r = "savefrom-helper-extension", a = {
        getItem(e) {
            var t = null;
            try {
                t = window.sessionStorage.getItem(e);
            } catch (t) {
                i.error("getItem error", e, t);
            }
            return t;
        },
        setItem(e, t) {
            try {
                window.sessionStorage.setItem(e, t);
            } catch (n) {
                i.error("setMarker error", e, t, n);
            }
        },
        hash(e) {
            var t = e.length, n = 0, o = 0;
            if (t > 0) for (;o < t; ) n = (n << 5) - n + e.charCodeAt(o++) | 0;
            return "" + n;
        },
        getMarker() {
            var e = null;
            return e = browser.runtime.id, this.hash("" + e);
        },
        getCurrentMarker() {
            return this.getItem(r);
        },
        setMarker(e) {
            return this.setItem(r, e);
        },
        getFallbackMarker() {
            return this.getItem(`${r}-fallback`);
        },
        setFallbackMarker() {
            return this.setItem(`${r}-fallback`, "1");
        },
        isSingle() {
            var e = this.getMarker(), t = this.getCurrentMarker();
            return "1" === t && null === this.getFallbackMarker() && (this.setFallbackMarker(),
            t = null), null === t && this.setMarker(t = e), t === e;
        }
    };
    t.a = a;
}, function(e, t, n) {
    "use strict";
    var o = n(0), i = function() {
        for (var e = arguments[0], t = 1, n = arguments.length; t < n; t++) {
            var o = arguments[t];
            for (var i in o) void 0 !== o[i] && (delete e[i], e[i] = o[i]);
        }
        return e;
    }, r = function(e, t) {
        var n = null;
        return function() {
            var o = this, i = arguments;
            clearTimeout(n), n = setTimeout(function() {
                e.apply(o, i);
            }, t);
        };
    }, a = n(3), s = n(7), l = n(8), d = n(21), u = n(2), c = {
        animation: "none 0s ease 0s 1 normal none running",
        backfaceVisibility: "visible",
        background: "transparent none repeat 0 0 / auto auto padding-box border-box scroll",
        border: "medium none currentColor",
        borderCollapse: "separate",
        borderImage: "none",
        borderRadius: "0",
        borderSpacing: "0",
        bottom: "auto",
        boxShadow: "none",
        boxSizing: "content-box",
        captionSide: "top",
        clear: "none",
        clip: "auto",
        color: "inherit",
        columns: "auto",
        columnCount: "auto",
        columnFill: "balance",
        columnGap: "normal",
        columnRule: "medium none currentColor",
        columnSpan: "1",
        columnWidth: "auto",
        content: "normal",
        counterIncrement: "none",
        counterReset: "none",
        cursor: "auto",
        direction: "ltr",
        display: "inline",
        emptyCells: "show",
        float: "none",
        font: "normal normal normal normal medium/normal inherit",
        height: "auto",
        hyphens: "none",
        left: "auto",
        letterSpacing: "normal",
        listStyle: "disc outside none",
        margin: "0",
        maxHeight: "none",
        maxWidth: "none",
        minHeight: "0",
        minWidth: "0",
        opacity: "1",
        orphans: "0",
        outline: "medium none invert",
        overflow: "visible",
        overflowX: "visible",
        overflowY: "visible",
        padding: "0",
        pageBreakAfter: "auto",
        pageBreakBefore: "auto",
        pageBreakInside: "auto",
        perspective: "none",
        perspectiveOrigin: "50% 50%",
        position: "static",
        right: "auto",
        tabSize: "8",
        tableLayout: "auto",
        textAlign: "inherit",
        textAlignLast: "auto",
        textDecoration: "none solid currentColor",
        textIndent: "0",
        textShadow: "none",
        textTransform: "none",
        top: "auto",
        transform: "none",
        transformOrigin: "50% 50% 0",
        transformStyle: "flat",
        transition: "none 0s ease 0s",
        unicodeBidi: "normal",
        verticalAlign: "baseline",
        visibility: "visible",
        whiteSpace: "normal",
        widows: "0",
        width: "auto",
        wordSpacing: "normal",
        zIndex: "auto",
        all: "initial"
    }, p = n(1), f = n(19), h = n(4), m = n(15), g = n(9);
    var v = (e, t) => {
        if (!(e.dataset.sfTitleTooltip > 0)) {
            e.dataset.sfTitleTooltip = 1;
            var n = new class {
                constructor(e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    this.target = e, this.options = Object.assign({
                        content: "",
                        defaultWidth: 0,
                        defaultHeight: 0
                    }, t), this.handleMouseleave = this.handleMouseleave.bind(this), this.isVisible = !1,
                    this.timeout = null;
                }
                handleMouseleave() {
                    this.hide();
                }
                show() {
                    if (this.isVisible) this.startHideTimeout(); else {
                        this.node || (this.node = this.createTooltip()), this.isVisible = !0;
                        var e = document.body;
                        e && (this.node.classList.add("hidden"), e.appendChild(this.node), this.setPos(),
                        this.node.classList.remove("hidden")), this.target.addEventListener("mouseleave", this.handleMouseleave),
                        this.startHideTimeout();
                    }
                }
                hide() {
                    this.isVisible && (this.isVisible = !1, this.stopHideTimeout(), this.node.classList.add("hidden"),
                    this.target.removeEventListener("mouseleave", this.handleMouseleave), setTimeout(() => {
                        if (!this.isVisible && this.node) {
                            var e = this.node.parentNode;
                            e && e.removeChild(this.node), this.node = null;
                        }
                    }, 250));
                }
                startHideTimeout() {
                    this.stopHideTimeout(), this.timeout = setTimeout(() => {
                        this.hide();
                    }, 3e3);
                }
                stopHideTimeout() {
                    clearTimeout(this.timeout);
                }
                createTooltip() {
                    return p.a.create("div", {
                        class: [ "sf-paper-tooltip-ctr" ],
                        append: [ p.a.create("div", {
                            class: "sf-paper-tooltip",
                            text: this.options.content
                        }), p.a.create("style", {
                            text: Object(a.a)({
                                ".sf-paper-tooltip-ctr": {
                                    display: "block",
                                    outline: "none",
                                    userSelect: "none",
                                    cursor: "default",
                                    position: "absolute",
                                    zIndex: 1e4,
                                    transition: "opacity 0.25s",
                                    maxWidth: "400px"
                                },
                                ".sf-paper-tooltip-ctr.hidden": {
                                    opacity: 0
                                },
                                ".sf-paper-tooltip": {
                                    display: "block",
                                    outline: "none",
                                    fontFamily: "Arial",
                                    fontSize: "12px",
                                    backgroundColor: "#616161",
                                    opacity: .9,
                                    color: "white",
                                    padding: "8px",
                                    borderRadius: "2px",
                                    margin: "8px"
                                }
                            })
                        }) ]
                    });
                }
                setPos() {
                    var e = window, t = e.pageXOffset, n = e.pageYOffset, o = e.innerWidth, i = e.innerHeight, r = i + n, a = o + t, s = this.node.getBoundingClientRect();
                    s.width || s.height || (s.width = this.options.defaultWidth, s.height = this.options.defaultHeight);
                    var l = this.target.getBoundingClientRect(), d = {}, u = [ "top", "bottom", "left", "right" ].map(e => {
                        var u = null, c = null, p = 0;
                        if (-1 !== [ "left", "right" ].indexOf(e)) {
                            var f = (l.height - s.height) / 2;
                            if (u = Math.round(l.top + n + f), s.height < i) {
                                var h = u + s.height;
                                h > r && (u -= h - r, p = 1), u < 0 && (u = 0, p = 1);
                            }
                        } else "top" === e ? u = Math.round(l.top + n) - s.height : "bottom" === e && (u = Math.round(l.top + n) + l.height);
                        if (-1 !== [ "top", "bottom" ].indexOf(e)) {
                            var m = (l.width - s.width) / 2;
                            if (c = Math.round(l.left + t + m), s.width < o) {
                                var g = c + s.width;
                                g > a && (c -= g - a, p = 1), c < 0 && (c = 0, p = 1);
                            }
                        } else "left" === e ? c = Math.round(l.left + t - s.width) : "right" === e && (c = Math.round(l.left + t + l.width));
                        var v = c + s.width, y = u + s.height, b = s.width, x = s.height, w = b;
                        u < 0 && (x -= -1 * u), y > r && (x -= y - r), c < 0 && (w -= -1 * c), v > a && (w -= v - a);
                        var C = 100 / (s.width * s.height) * (w * x) - p;
                        return d[e] = {
                            top: u,
                            left: c,
                            quality: C
                        };
                    });
                    u.sort((e, t) => {
                        var n = e.quality, o = t.quality;
                        return n === o ? 0 : n > o ? -1 : 1;
                    });
                    var c = u[0];
                    this.node.style.top = c.top + "px", this.node.style.left = c.left + "px";
                }
            }(e, t);
            e.addEventListener("show_tooltip", () => {
                n.show();
            }), e.addEventListener("hide_tooltip", () => {
                n.hide();
            });
        }
        u.a.trigger(e, "show_tooltip");
    }, y = n(20), b = Object(g.a)("components"), x = null, w = {
        downloadParam: "sfh--download",
        setStyle: function(e, t) {
            if (e && t) for (var n in t) e.style[n] = t[n];
        },
        getStyle: function(e, t) {
            return e && window.getComputedStyle && window.getComputedStyle(e, null).getPropertyValue(t);
        },
        addStyleRules: function(e, t, n) {
            var o = n ? document.querySelector("#savefrom-styles." + n) : document.getElementById("savefrom-styles");
            if (!o) {
                (o = document.createElement("style")).id = "savefrom-styles", n && o.classList.add(n);
                var i = document.querySelector("head style");
                i ? i.parentNode.insertBefore(o, i) : document.querySelector("head").appendChild(o);
            }
            if ("object" == typeof t) {
                var r = [];
                for (var a in t) r.push(a + ":" + t[a]);
                t = r.join(";");
            }
            o.textContent += e + "{" + t + "}";
        },
        getPosition: function(e, t) {
            var n = e.getBoundingClientRect();
            if (t) {
                var o = t.getBoundingClientRect();
                return {
                    top: Math.round(n.top - o.top),
                    left: Math.round(n.left - o.left),
                    width: n.width,
                    height: n.height
                };
            }
            return {
                top: Math.round(n.top + window.pageYOffset),
                left: Math.round(n.left + window.pageXOffset),
                width: n.width,
                height: n.height
            };
        },
        getSize: function(e) {
            return {
                width: e.offsetWidth,
                height: e.offsetHeight
            };
        },
        getMatchFirst: function(e, t) {
            var n = e.match(t);
            return n && n.length > 1 ? n[1] : "";
        },
        getElementByIds: function(e) {
            for (var t = 0; t < e.length; t++) {
                var n = document.getElementById(e[t]);
                if (n) return n;
            }
            return null;
        },
        getParentByClass: function(e, t) {
            if (!e || "" == t) return !1;
            var n;
            if ("object" == typeof t && t.length > 0) for (n = e; n; n = n.parentNode) {
                if (1 !== n.nodeType) return null;
                for (var o = 0; o < t.length; o++) if (n.classList.contains(t[o])) return n;
            } else for (n = e; n; n = n.parentNode) {
                if (1 !== n.nodeType) return null;
                if (n.classList.contains(t)) return n;
            }
            return null;
        },
        getParentByTagName: function(e, t) {
            if (!e || !t) return !1;
            for (var n = e; n; n = n.parentNode) {
                if (1 !== n.nodeType) return null;
                if (n.tagName === t) return n;
            }
            return null;
        },
        getParentById: function(e, t) {
            for (var n = e; n; n = n.parentNode) {
                if (1 !== n.nodeType) return null;
                if (n.id === t) return n;
            }
            return null;
        },
        hasChildrenTagName: function(e, t) {
            for (var n, o = 0; n = e.childNodes[o]; o++) if (1 === n.nodeType && n.tagName === t) return !0;
            return !1;
        },
        isParent: function(e, t) {
            return !(!t || -1 === [ 1, 9, 11 ].indexOf(t.nodeType)) && t.contains(e);
        },
        emptyNode: function(e) {
            for (;e.firstChild; ) e.removeChild(e.firstChild);
        },
        download: function(e, t, n, i) {
            if (!t) return !1;
            if (!(e = e || this.getFileName(t))) return !1;
            if (!x.preferences.downloads) return !1;
            var r = n || {};
            return r.url = t, r.filename = e, i = i || void 0, o.a.sendMessage({
                action: "downloadFile",
                options: r
            }, i), !0;
        },
        downloadList: {
            showDownloadWarningPopup: function(e, t) {
                var n = w.playlist.getInfoPopupTemplate();
                o.a.sendMessage({
                    action: "getWarningIcon",
                    type: t
                }, function(e) {
                    n.icon.style.backgroundImage = "url(" + e + ")";
                }), p.a.create(n.textContainer, {
                    append: [ p.a.create("p", {
                        text: o.a.i18n.getMessage("warningPopupTitle"),
                        style: {
                            color: "#0D0D0D",
                            fontSize: "20px",
                            marginBottom: "11px",
                            marginTop: "13px"
                        }
                    }), p.a.create("p", {
                        text: o.a.i18n.getMessage("warningPopupDesc") + " ",
                        style: {
                            color: "#868686",
                            fontSize: "14px",
                            marginBottom: "13px",
                            lineHeight: "24px",
                            marginTop: "0px"
                        },
                        append: p.a.create("a", {
                            href: "ru" === o.a.i18n.getMessage("lang") || "uk" === o.a.i18n.getMessage("lang") ? "http://vk.com/page-55689929_49003549" : "http://vk.com/page-55689929_49004259",
                            text: o.a.i18n.getMessage("readMore"),
                            target: "_blank",
                            style: {
                                color: "#4A90E2"
                            }
                        })
                    }), p.a.create("p", {
                        style: {
                            marginBottom: "13px"
                        },
                        append: [ p.a.create("label", {
                            style: {
                                color: "#868686",
                                cursor: "pointer",
                                fontSize: "14px",
                                lineHeight: "19px"
                            },
                            append: [ p.a.create("input", {
                                type: "checkbox",
                                style: {
                                    cssFloat: "left",
                                    marginLeft: "0px"
                                },
                                on: [ "click", function() {
                                    o.a.sendMessage({
                                        action: "hideDownloadWarning",
                                        set: this.checked ? 1 : 0
                                    });
                                } ]
                            }), o.a.i18n.getMessage("noWarning") ]
                        }) ]
                    }) ]
                });
                var i = void 0, r = void 0;
                p.a.create(n.buttonContainer, {
                    append: [ i = p.a.create("button", {
                        text: o.a.i18n.getMessage("cancel"),
                        style: {
                            height: "27px",
                            width: "118px",
                            backgroundColor: "#ffffff",
                            border: "1px solid #9e9e9e",
                            margin: "12px",
                            marginBottom: "11px",
                            marginRight: "4px",
                            borderRadius: "5px",
                            fontSize: "14px",
                            cursor: "pointer"
                        }
                    }), r = p.a.create("button", {
                        text: o.a.i18n.getMessage("continue"),
                        style: {
                            height: "27px",
                            width: "118px",
                            backgroundColor: "#ffffff",
                            border: "1px solid #9e9e9e",
                            margin: "12px",
                            marginBottom: "11px",
                            marginRight: "8px",
                            borderRadius: "5px",
                            fontSize: "14px",
                            cursor: "pointer"
                        }
                    }) ]
                }), i.addEventListener("click", function(e) {
                    var t = n.body.parentNode;
                    u.a.trigger(t.lastChild, "click");
                }), r.addEventListener("click", function(t) {
                    t.preventDefault(), t.stopPropagation(), e(), u.a.trigger(i, "click");
                }), w.popupDiv(n.body, "dl_warning_box_popup");
            },
            startChromeDownloadList: function(e) {
                var t = e.folderName, n = e.list;
                return t && (t += "/"), o.a.sendMessage({
                    action: "downloadList",
                    fileList: n,
                    folder: t
                });
            },
            startOldChromeDownloadList: function(e, t) {
                var n = e.folderName, i = e.list, r = e.type;
                n && (n += "/");
                var a = 0, s = !1, l = 500, d = document.body;
                d.focus(), t || (d.onblur = function() {
                    s = !0;
                });
                !function e() {
                    var t = i[a];
                    if (a++, void 0 !== t) if (x.preferences.downloads ? w.download(n + t.filename, t.url) : u.a.trigger(p.a.create("a", {
                        download: t.filename,
                        href: t.url,
                        on: [ "click", function(e) {
                            w.downloadOnClick(e);
                        } ]
                    }), "click", {
                        cancelable: !0,
                        altKey: !0
                    }), s) w.downloadList.showDownloadWarningPopup(function() {
                        s = !1, d.focus(), e();
                    }, r); else {
                        if (a > 5 && l && (l = void 0, d.onblur = void 0, s = !1, x.preferences.downloads)) return void o.a.sendMessage({
                            action: "downloadList",
                            fileList: i.slice(a),
                            folder: n
                        });
                        setTimeout(function() {
                            e();
                        }, l);
                    }
                }();
            },
            startDownload: function(e) {
                return e.list.forEach(function(e) {
                    e.filename = h.a.modify(e.filename);
                }), e.folderName = h.a.modify(e.folderName), o.a.isChrome && x.preferences.downloads || o.a.isFirefox ? w.downloadList.startChromeDownloadList(e) : o.a.isGM || o.a.isSafari ? o.a.sendMessage({
                    action: "hideDownloadWarning"
                }, function(t) {
                    w.downloadList.startOldChromeDownloadList(e, t);
                }) : void 0;
            },
            showBeforeDownloadPopup: function(e, t) {
                t.list = e;
                var n = t.type, i = t.folderName, r = t.onContinue || w.downloadList.startDownload, a = t.onShowList || w.playlist.popupFilelist, s = t.count || e.length, l = w.playlist.getInfoPopupTemplate();
                o.a.sendMessage({
                    action: "getWarningIcon",
                    color: "#00CCFF",
                    type: n
                }, function(e) {
                    l.icon.style.backgroundImage = "url(" + e + ")";
                });
                var d = [];
                a && (d = [ " (", p.a.create("a", {
                    href: "#",
                    text: o.a.i18n.getMessage("vkListOfLinks").toLowerCase()
                }), ")" ])[1].addEventListener("click", function(e) {
                    e.preventDefault(), e.stopPropagation(), a(t.list), u.a.trigger(c, "click");
                }), p.a.create(l.textContainer, {
                    append: [ p.a.create("p", {
                        text: i || o.a.i18n.getMessage("playlistTitle"),
                        style: {
                            color: "#0D0D0D",
                            fontSize: "20px",
                            marginBottom: "11px",
                            marginTop: "13px"
                        }
                    }), p.a.create("p", {
                        text: o.a.i18n.getMessage("vkFoundFiles").replace("%d", s),
                        style: {
                            color: "#868686",
                            fontSize: "14px",
                            marginBottom: "13px",
                            lineHeight: "24px",
                            marginTop: "0px"
                        },
                        append: d
                    }), p.a.create("p", {
                        text: o.a.i18n.getMessage("beforeDownloadPopupWarn"),
                        style: {
                            color: "#868686",
                            fontSize: "14px",
                            marginBottom: "13px",
                            lineHeight: "24px",
                            marginTop: "0px"
                        }
                    }) ]
                });
                var c = void 0, f = void 0;
                p.a.create(l.buttonContainer, {
                    append: [ c = p.a.create("button", {
                        text: o.a.i18n.getMessage("cancel"),
                        style: {
                            height: "27px",
                            width: "118px",
                            backgroundColor: "#ffffff",
                            border: "1px solid #9e9e9e",
                            margin: "12px",
                            marginBottom: "11px",
                            marginRight: "4px",
                            borderRadius: "5px",
                            fontSize: "14px",
                            cursor: "pointer"
                        }
                    }), f = p.a.create("button", {
                        text: o.a.i18n.getMessage("continue"),
                        style: {
                            height: "27px",
                            width: "118px",
                            backgroundColor: "#ffffff",
                            border: "1px solid #9e9e9e",
                            margin: "12px",
                            marginBottom: "11px",
                            marginRight: "8px",
                            borderRadius: "5px",
                            fontSize: "14px",
                            cursor: "pointer"
                        }
                    }) ]
                }), c.addEventListener("click", function(e) {
                    var t = l.body.parentNode;
                    u.a.trigger(t.lastChild, "click");
                }), f.addEventListener("click", function(e) {
                    e.preventDefault(), e.stopPropagation(), r(t), u.a.trigger(c, "click");
                }), w.popupDiv(l.body, "dl_confirm_box_popup");
            }
        },
        downloadLink: function(e, t) {
            if (!e.href) return !1;
            var n = e.getAttribute("download");
            return this.download(n, e.href, null, t);
        },
        safariDlLink: function(e) {
            if (!(e.button || e.ctrlKey || e.altKey || e.shitfKey)) {
                var t = null;
                try {
                    if ("function" != typeof MouseEvent) throw "legacy";
                    t = new MouseEvent("click", {
                        bubbles: !0,
                        cancelable: e.cancelable,
                        screenX: e.screenX,
                        screenY: e.screenY,
                        clientX: e.clientX,
                        clientY: e.clientY,
                        ctrlKey: !1,
                        altKey: !0,
                        shiftKey: !1,
                        metaKey: e.metaKey,
                        button: e.button,
                        relatedTarget: e.relatedTarget
                    });
                } catch (n) {
                    t = function(e) {
                        var t = document.createEvent("MouseEvents");
                        return t.initMouseEvent("click", !0, e.cancelable, window, 0, e.screenX, e.screenY, e.clientX, e.clientY, !1, !0, !1, e.metaKey, e.button, e.relatedTarget),
                        t;
                    }(e);
                }
                e.preventDefault(), e.stopPropagation(), this.dispatchEvent(t);
            }
        },
        downloadOnClick: function(e, t, n) {
            var i = w, r = (n = n || {}).el || e.target;
            if ("A" !== r.tagName && (r = Object(l.a)(r, "A")), r) {
                if (o.a.isSafari) {
                    if (!e.altKey && !e.ctrlKey) return e.preventDefault(), void v(r, {
                        content: o.a.i18n.getMessage("downloadTitle"),
                        defaultWidth: 400,
                        defaultHeight: 60
                    });
                    (e => {
                        e.dataset.sfTitleTooltip > 0 && u.a.trigger(e, "hide_tooltip");
                    })(r);
                }
                if (o.a.isSafari) return i.safariDlLink.call(r, e);
                x.preferences.downloads && ((o.a.isFirefox || o.a.isGM) && /^blob:|^data:/.test(r.href) || 2 !== e.button && (e.preventDefault(),
                e.stopPropagation(), i.downloadLink(r, t)));
            }
        },
        getQueryString: function(e, t, n) {
            if (!e || "object" != typeof e) return "";
            void 0 === t && (t = ""), void 0 === n && (n = "");
            var o = "";
            for (var i in e) o.length && (o += "&"), e[i] instanceof Object ? (t || (t = ""),
            n || (n = ""), o += w.getQueryString(e[i], t + i + "[", "]" + n)) : o += t + escape(i) + n + "=" + escape(e[i]);
            return o;
        },
        decodeUnicodeEscapeSequence: function(e) {
            return e.replace(/\\u([0-9a-f]{4})/g, function(e, t) {
                if (t = parseInt(t, 16), !isNaN(t)) return String.fromCharCode(t);
            });
        },
        getFileExtension: function(e, t) {
            var n = this.getMatchFirst(e, /\.([a-z0-9]{3,4})(\?|$)/i);
            return n ? n.toLowerCase() : t || "";
        },
        getFileName: function(e) {
            var t = this.getMatchFirst(e, /\/([^\?#\/]+\.[a-z\d]{2,6})(?:\?|#|$)/i);
            return t ? h.a.modify(t) : t;
        },
        getTopLevelDomain: function(e) {
            if (!e) return "";
            if (!e.match(/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}/)) return e;
            var t = e.split("."), n = t.length;
            return 2 == n ? e : t[n - 2] + "." + t[n - 1];
        },
        dateToObj: function(e, t) {
            var n = null === e || void 0 === e ? new Date() : new Date(e);
            void 0 === t && (t = !0);
            var o = {
                year: n.getFullYear(),
                month: n.getMonth() + 1,
                day: n.getDate(),
                hour: n.getHours(),
                min: n.getMinutes(),
                sec: n.getSeconds()
            };
            if (t) for (var i in o) 1 == o[i].toString().length && (o[i] = "0" + o[i]);
            return o;
        },
        utf8Encode: function(e) {
            e = e.replace(/\r\n/g, "\n");
            for (var t = "", n = 0; n < e.length; n++) {
                var o = e.charCodeAt(n);
                o < 128 ? t += String.fromCharCode(o) : o > 127 && o < 2048 ? (t += String.fromCharCode(o >> 6 | 192),
                t += String.fromCharCode(63 & o | 128)) : (t += String.fromCharCode(o >> 12 | 224),
                t += String.fromCharCode(o >> 6 & 63 | 128), t += String.fromCharCode(63 & o | 128));
            }
            return t;
        },
        sizeHuman: function(e, t) {
            void 0 != t && null != t || (t = 2);
            var n = e, i = 0, r = "", a = [ o.a.i18n.getMessage("vkFileSizeByte"), o.a.i18n.getMessage("vkFileSizeKByte"), o.a.i18n.getMessage("vkFileSizeMByte"), o.a.i18n.getMessage("vkFileSizeGByte"), o.a.i18n.getMessage("vkFileSizeTByte") ];
            for (n < 0 && (r = "-", n = Math.abs(n)); n >= 1e3; ) i++, n /= 1024;
            if (t >= 0) {
                var s = 10 * t;
                n = Math.round(n * s) / s;
            }
            return i < a.length ? r + n + " " + a[i] : e;
        },
        secondsToDuration: function(e) {
            if (!e || isNaN(e)) return "";
            function t(e) {
                return e < 10 ? "0" + e : e.toString();
            }
            var n = Math.floor(e / 3600);
            e %= 3600;
            var o = Math.floor(e / 60);
            return e %= 60, n > 0 ? n + ":" + t(o) + ":" + t(e) : o + ":" + t(e);
        },
        svg: {
            icon: {
                download: "M 4,0 4,8 0,8 8,16 16,8 12,8 12,0 4,0 z",
                info: "M 8,1.55 C 11.6,1.55 14.4,4.44 14.4,8 14.4,11.6 11.6,14.4 8,14.4 4.44,14.4 1.55,11.6 1.55,8 1.55,4.44 4.44,1.55 8,1.55 M 8,0 C 3.58,0 0,3.58 0,8 0,12.4 3.58,16 8,16 12.4,16 16,12.4 16,8 16,3.58 12.4,0 8,0 L 8,0 z M 9.16,12.3 H 6.92 V 7.01 H 9.16 V 12.3 z M 8.04,5.91 C 7.36,5.91 6.81,5.36 6.81,4.68 6.81,4 7.36,3.45 8.04,3.45 8.72,3.45 9.27,4 9.27,4.68 9.27,5.36 8.72,5.91 8.04,5.91 z",
                noSound: "M 11.4,5.05 13,6.65 14.6,5.05 16,6.35 14.4,7.95 16,9.55 14.6,11 13,9.35 11.4,11 10,9.55 11.6,7.95 10,6.35 z M 8,1.75 8,14.3 4,10.5 l -4,0 0,-4.75 4,0 z"
            },
            cache: {},
            getSrc: function(e, t) {
                return this.icon[e] ? (this.cache[e] || (this.cache[e] = {}), this.cache[e][t] || (this.cache[e][t] = btoa('<?xml version="1.0" encoding="UTF-8"?><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="16" height="16" viewBox="0 0 16 16" id="svg2" xml:space="preserve"><path d="' + this.icon[e] + '" fill="' + t + '" /></svg>')),
                this.cache[e][t] ? "data:image/svg+xml;base64," + this.cache[e][t] : "") : "";
            },
            getSvg: function(e, t, n, o) {
                var i = document.createElementNS("http://www.w3.org/2000/svg", "svg"), r = i.namespaceURI;
                i.setAttribute("width", n || "16"), i.setAttribute("height", o || n || "16"), i.setAttribute("viewBox", "0 0 16 16");
                var a = document.createElementNS(r, "path");
                return i.appendChild(a), a.setAttribute("d", this.icon[e]), t && a.setAttribute("fill", t),
                i;
            }
        },
        appendDownloadInfo: function(e, t, n, i) {
            t || (t = "#a0a0a0");
            var r = document.createElement("span");
            r.appendChild(document.createTextNode(o.a.i18n.getMessage("downloadTitle"))), this.setStyle(r, {
                display: "inline-block",
                position: "relative",
                border: "1px solid " + t,
                borderRadius: "5px",
                fontSize: "13px",
                lineHeight: "17px",
                padding: "2px 19px 2px 5px",
                marginTop: "5px",
                opacity: .9
            }), n && this.setStyle(r, n);
            var a = document.createElement("span");
            a.textContent = String.fromCharCode(215), this.setStyle(a, {
                color: t,
                width: "14px",
                height: "14px",
                fontSize: "14px",
                fontWeight: "bold",
                lineHeight: "14px",
                position: "absolute",
                top: 0,
                right: 0,
                overflow: "hidden",
                cursor: "pointer"
            }), i && this.setStyle(a, i), a.addEventListener("click", function() {
                r.parentNode.removeChild(r), o.a.sendMessage({
                    action: "updateOption",
                    key: "moduleShowDownloadInfo",
                    value: 0
                });
            }, !1), r.appendChild(a), e.appendChild(r);
        },
        getFileSizeIcon: function(e, t, n, i) {
            var r = this;
            i = i || {}, e = e || {}, t = t || {}, n = n || {};
            var a = function(e) {
                return p.a.create("div", {
                    style: t,
                    append: [ p.a.create(w.svg.getSvg("info", e), {
                        style: n
                    }) ]
                });
            }, s = p.a.create("div", {
                style: e,
                append: [ p.a.create(a("#333333"), {
                    title: o.a.i18n.getMessage("getFileSizeTitle"),
                    on: [ "click", function e(t) {
                        t.stopPropagation(), t.preventDefault(), s.textContent = "...";
                        var n = i.url;
                        n || (n = i.link && i.link.href), Object(m.a)({
                            action: "getFileSize",
                            url: n
                        }).then(function(e) {
                            if (e.error || !e.fileSize) throw new Error(JSON.stringify(e));
                            var t = e.fileType || "", n = r.sizeHuman(e.fileSize, 2), a = "";
                            if (i.link && /^audio\//i.test(t)) {
                                var l = parseInt(i.link.dataset.savefromHelperDuration);
                                l > 0 && (a += Math.floor(e.fileSize / l / 125), a += " " + o.a.i18n.getMessage("kbps"));
                            }
                            var d = "";
                            d += a ? n + " ~ " + a : n, i.brackets && (d = "(" + d + ")"), s.textContent = d,
                            s.title = t;
                        }).catch(function(t) {
                            var n;
                            b.error(t), "ZERO" === t.message ? (n = a("#ffac00")).title = o.a.i18n.getMessage("getFileSizeTitle") : (n = a("#ff0000")).title = o.a.i18n.getMessage("getFileSizeFailTitle"),
                            n.addEventListener("click", e), s.textContent = "", s.appendChild(n);
                        });
                    } ]
                }) ]
            });
            return {
                node: s
            };
        },
        appendFileSizeIcon: function(e, t, n, i, r, a) {
            t = t || {}, n = n || {};
            var s = "#333333";
            "0" === i ? s = "#ffac00" : i ? s = "#ff0000" : t.color && (s = t.color);
            var l = {
                width: "14px",
                height: "14px",
                marginLeft: "3px",
                verticalAlign: "middle",
                position: "relative",
                top: "-1px",
                cursor: "pointer"
            };
            Object.assign(l, t);
            var d = {
                fontSize: "75%",
                fontWeight: "normal",
                marginLeft: "3px",
                whiteSpace: "nowrap"
            };
            Object.assign(d, n);
            var u = p.a.create("img", {
                src: w.svg.getSrc("info", s),
                title: i ? o.a.i18n.getMessage("getFileSizeFailTitle") : o.a.i18n.getMessage("getFileSizeTitle"),
                style: l
            }), c = this;
            return a ? a.appendChild(u) : e.nextSibling ? e.parentNode.insertBefore(u, e.nextSibling) : e.parentNode.appendChild(u),
            u.addEventListener("click", function(i) {
                i.preventDefault(), i.stopPropagation();
                var a = p.a.create("span", {
                    text: "...",
                    style: d
                });
                return u.parentNode.replaceChild(a, u), o.a.sendMessage({
                    action: "getFileSize",
                    url: e.href
                }, function(i) {
                    if (i.fileSize > 0) {
                        var s = i.fileType || "", l = c.sizeHuman(i.fileSize, 2), d = "";
                        if (/^audio\//i.test(s)) {
                            var u = e.getAttribute("data-savefrom-helper-duration");
                            (u = u && parseInt(u)) > 0 && (d = Math.floor(i.fileSize / u / 125), d += " " + o.a.i18n.getMessage("kbps"));
                        }
                        var p = "";
                        p = d ? l + " ~ " + d : l, r || (p = "(" + p + ")"), a.textContent = p, a.title = s;
                    } else if (i.error) {
                        var f = c.appendFileSizeIcon(e, t, n, !0, r, document.createDocumentFragment());
                        a.parentNode.replaceChild(f, a);
                    } else {
                        var h = c.appendFileSizeIcon(e, t, n, "0", r, document.createDocumentFragment());
                        a.parentNode.replaceChild(h, a);
                    }
                });
            }, !1), u;
        },
        appendNoSoundIcon: function(e, t) {
            var n = "#ff0000";
            (t = t || {}).color && (n = t.color);
            var i = {
                width: "14px",
                height: "14px",
                marginLeft: "3px",
                verticalAlign: "middle",
                position: "relative",
                top: "-1px",
                cursor: "pointer"
            };
            Object.assign(i, t);
            var r = p.a.create("img", {
                src: w.svg.getSrc("noSound", n),
                title: o.a.i18n.getMessage("withoutAudio"),
                style: i
            });
            e.nextSibling ? e.parentNode.insertBefore(r, e.nextSibling) : e.parentNode ? e.parentNode.appendChild(r) : e.appendChild(r);
        },
        video: {
            dataAttr: "data-savefrom-video-visible",
            yt: {
                inited: !1,
                show3D: !1,
                showMP4NoAudio: !1,
                showFormat: {
                    FLV: !0,
                    MP4: !0,
                    WebM: !1,
                    "3GP": !1,
                    "Audio AAC": !1,
                    "Audio Vorbis": !1,
                    "Audio Opus": !1
                },
                format: {
                    FLV: {
                        5: {
                            quality: "240"
                        },
                        6: {
                            quality: "270"
                        },
                        34: {
                            quality: "360"
                        },
                        35: {
                            quality: "480"
                        }
                    },
                    MP4: {
                        18: {
                            quality: "360"
                        },
                        22: {
                            quality: "720"
                        },
                        37: {
                            quality: "1080"
                        },
                        38: {
                            quality: "8K"
                        },
                        59: {
                            quality: "480"
                        },
                        78: {
                            quality: "480"
                        },
                        82: {
                            quality: "360",
                            "3d": !0
                        },
                        83: {
                            quality: "240",
                            "3d": !0
                        },
                        84: {
                            quality: "720",
                            "3d": !0
                        },
                        85: {
                            quality: "1080",
                            "3d": !0
                        },
                        160: {
                            quality: "144",
                            noAudio: !0
                        },
                        133: {
                            quality: "240",
                            noAudio: !0
                        },
                        134: {
                            quality: "360",
                            noAudio: !0
                        },
                        135: {
                            quality: "480",
                            noAudio: !0
                        },
                        136: {
                            quality: "720",
                            noAudio: !0
                        },
                        137: {
                            quality: "1080",
                            noAudio: !0
                        },
                        212: {
                            quality: "480",
                            noAudio: !0
                        },
                        213: {
                            quality: "480",
                            noAudio: !0
                        },
                        214: {
                            quality: "720",
                            noAudio: !0
                        },
                        215: {
                            quality: "720",
                            noAudio: !0
                        },
                        264: {
                            quality: "1440",
                            noAudio: !0
                        },
                        138: {
                            quality: "8K",
                            noAudio: !0
                        },
                        298: {
                            quality: "720",
                            noAudio: !0,
                            sFps: !0
                        },
                        299: {
                            quality: "1080",
                            noAudio: !0,
                            sFps: !0
                        },
                        266: {
                            quality: "4K",
                            noAudio: !0
                        }
                    },
                    WebM: {
                        43: {
                            quality: "360"
                        },
                        44: {
                            quality: "480"
                        },
                        45: {
                            quality: "720"
                        },
                        46: {
                            quality: "1080"
                        },
                        167: {
                            quality: "360",
                            noAudio: !0
                        },
                        168: {
                            quality: "480",
                            noAudio: !0
                        },
                        169: {
                            quality: "720",
                            noAudio: !0
                        },
                        170: {
                            quality: "1080",
                            noAudio: !0
                        },
                        218: {
                            quality: "480",
                            noAudio: !0
                        },
                        219: {
                            quality: "480",
                            noAudio: !0
                        },
                        242: {
                            quality: "240",
                            noAudio: !0
                        },
                        243: {
                            quality: "360",
                            noAudio: !0
                        },
                        244: {
                            quality: "480",
                            noAudio: !0
                        },
                        245: {
                            quality: "480",
                            noAudio: !0
                        },
                        246: {
                            quality: "480",
                            noAudio: !0
                        },
                        247: {
                            quality: "720",
                            noAudio: !0
                        },
                        248: {
                            quality: "1080",
                            noAudio: !0
                        },
                        271: {
                            quality: "1440",
                            noAudio: !0
                        },
                        272: {
                            quality: "8K",
                            noAudio: !0
                        },
                        278: {
                            quality: "144",
                            noAudio: !0
                        },
                        100: {
                            quality: "360",
                            "3d": !0
                        },
                        101: {
                            quality: "480",
                            "3d": !0
                        },
                        102: {
                            quality: "720",
                            "3d": !0
                        },
                        302: {
                            quality: "720",
                            noAudio: !0,
                            sFps: !0
                        },
                        303: {
                            quality: "1080",
                            noAudio: !0,
                            sFps: !0
                        },
                        308: {
                            quality: "1440",
                            noAudio: !0,
                            sFps: !0
                        },
                        313: {
                            quality: "4K",
                            noAudio: !0
                        },
                        315: {
                            quality: "4K",
                            noAudio: !0,
                            sFps: !0
                        },
                        330: {
                            quality: "144",
                            noAudio: !0,
                            sFps: !0
                        },
                        331: {
                            quality: "240",
                            noAudio: !0,
                            sFps: !0
                        },
                        332: {
                            quality: "360",
                            noAudio: !0,
                            sFps: !0
                        },
                        333: {
                            quality: "480",
                            noAudio: !0,
                            sFps: !0
                        },
                        334: {
                            quality: "720",
                            noAudio: !0,
                            sFps: !0
                        },
                        335: {
                            quality: "1080",
                            noAudio: !0,
                            sFps: !0
                        },
                        336: {
                            quality: "1440",
                            noAudio: !0,
                            sFps: !0
                        },
                        337: {
                            quality: "2160",
                            noAudio: !0,
                            sFps: !0
                        }
                    },
                    "3GP": {
                        17: {
                            quality: "144"
                        },
                        36: {
                            quality: "240"
                        }
                    },
                    "Audio AAC": {
                        139: {
                            quality: "48",
                            ext: "m4a",
                            noVideo: !0
                        },
                        140: {
                            quality: "128",
                            ext: "m4a",
                            noVideo: !0
                        },
                        141: {
                            quality: "256",
                            ext: "m4a",
                            noVideo: !0
                        },
                        256: {
                            quality: "192",
                            ext: "m4a",
                            noVideo: !0
                        },
                        258: {
                            quality: "384",
                            ext: "m4a",
                            noVideo: !0
                        }
                    },
                    "Audio Vorbis": {
                        171: {
                            quality: "128",
                            ext: "webm",
                            noVideo: !0
                        },
                        172: {
                            quality: "192",
                            ext: "webm",
                            noVideo: !0
                        }
                    },
                    "Audio Opus": {
                        249: {
                            quality: "48",
                            ext: "opus",
                            noVideo: !0
                        },
                        250: {
                            quality: "128",
                            ext: "opus",
                            noVideo: !0
                        },
                        251: {
                            quality: "256",
                            ext: "opus",
                            noVideo: !0
                        }
                    }
                },
                init: function() {
                    if (!w.video.yt.inited) {
                        [ "Audio AAC", "Audio Vorbis", "Audio Opus" ].forEach(function(e) {
                            var t = w.video.yt.format[e];
                            for (var n in t) t[n].quality += " " + o.a.i18n.getMessage("kbps");
                        }), w.video.yt.show3D = "0" == x.preferences.ytHide3D, w.video.yt.showMP4NoAudio = "0" == x.preferences.ytHideMP4NoAudio;
                        var e = !1, t = !1;
                        for (var n in w.video.yt.showFormat) {
                            var i = "ytHide" + n.replace(" ", "_");
                            "ytHideAudio_AAC" === i && (i = "ytHideAudio_MP4");
                            var r = "0" == x.preferences[i];
                            "Audio AAC" === n && (t = r), w.video.yt.showFormat[n] = r, r && (e = !0);
                        }
                        w.video.yt.showFormat["Audio Vorbis"] = t, w.video.yt.showFormat["Audio Opus"] = t,
                        e || (w.video.yt.showFormat.FLV = !0), w.video.yt.inited = !0;
                    }
                },
                show: function(e, t, n, i, r) {
                    i = i || {};
                    var a = document.createElement("div");
                    w.setStyle(a, {
                        display: "inline-block",
                        margin: "0 auto"
                    }), t.appendChild(a);
                    var s = document.createElement("div");
                    w.setStyle(s, {
                        display: "inline-block",
                        padding: "0 90px 0 0",
                        position: "relative"
                    }), a.appendChild(s);
                    var l = document.createElement("table");
                    w.setStyle(l, {
                        emptyCells: "show",
                        borderCollapse: "collapse",
                        margin: "0 auto",
                        padding: "0",
                        width: "auto"
                    }), s.appendChild(l);
                    var d = !1;
                    for (var u in w.video.yt.format) w.video.yt.append(e, u, w.video.yt.format[u], l, i, r) && (d = !0);
                    for (var u in e) if ("ummy" !== u && "ummyAudio" !== u && "meta" !== u) {
                        w.video.yt.append(e, "", null, l, i, r) && (d = !0);
                        break;
                    }
                    if (l.firstChild) {
                        if (d) {
                            var c = document.createElement("span");
                            if (c.textContent = o.a.i18n.getMessage("more") + " " + String.fromCharCode(187),
                            w.setStyle(c, {
                                color: "#555",
                                border: "1px solid #a0a0a0",
                                borderRadius: "3px",
                                display: "block",
                                fontFamily: "Arial",
                                fontSize: "15px",
                                lineHeight: "17px",
                                padding: "1px 5px",
                                position: "absolute",
                                bottom: "3px",
                                right: "0",
                                cursor: "pointer"
                            }), i.btn && "object" == typeof i.btn && w.setStyle(c, i.btn), s.appendChild(c),
                            c.addEventListener("click", function(e) {
                                e.preventDefault(), e.stopPropagation();
                                for (var n = t.querySelectorAll("*[" + w.video.dataAttr + "]"), i = 0; i < n.length; i++) {
                                    var r = n[i].getAttribute(w.video.dataAttr), a = "none", s = String.fromCharCode(187);
                                    "0" == r ? (r = "1", a = "", s = String.fromCharCode(171)) : r = "0", n[i].style.display = a,
                                    n[i].setAttribute(w.video.dataAttr, r), this.textContent = o.a.i18n.getMessage("more") + " " + s;
                                }
                                return !1;
                            }, !1), 1 === n) {
                                l.querySelector("td a");
                                a.appendChild(document.createElement("br")), w.appendDownloadInfo(a, "#a0a0a0", null, {
                                    width: "16px",
                                    height: "16px",
                                    fontSize: "16px",
                                    lineHeight: "16px"
                                });
                            }
                        }
                    } else t.textContent = o.a.i18n.getMessage("noLinksFound");
                },
                append: function(e, t, n, i, r, a) {
                    var s = !1, l = {
                        whiteSpace: "nowrap"
                    }, d = {
                        fontSize: "75%",
                        fontWeight: "normal",
                        marginLeft: "3px",
                        whiteSpace: "nowrap"
                    }, u = document.createElement("tr"), c = document.createElement("td");
                    c.appendChild(document.createTextNode(t || "???")), t && w.video.yt.showFormat[t] || (u.setAttribute(w.video.dataAttr, "0"),
                    u.style.display = "none", s = !0), w.setStyle(c, {
                        border: "none",
                        padding: "3px 15px 3px 0",
                        textAlign: "left",
                        verticalAlign: "middle"
                    }), u.appendChild(c), c = document.createElement("td"), w.setStyle(c, {
                        border: "none",
                        padding: "3px 0",
                        textAlign: "left",
                        verticalAlign: "middle",
                        lineHeight: "17px"
                    }), u.appendChild(c);
                    var p = e.meta || {}, f = !1;
                    if (n) {
                        for (var m in n) if (e[m]) {
                            var g = n[m].quality;
                            f && (c.lastChild.style.marginRight = "15px", c.appendChild(document.createTextNode(" ")));
                            var v = document.createElement("span");
                            v.style.whiteSpace = "nowrap";
                            var y = document.createElement("a");
                            if (y.href = e[m], y.title = o.a.i18n.getMessage("downloadTitle"), p[m] && (p[m].quality && (g = p[m].quality),
                            n[m].sFps && (g += " " + (p[m].fps || 60))), n[m]["3d"] ? y.textContent = "3D" : y.textContent = g,
                            a) {
                                var b = n[m].ext;
                                b || (b = t.toLowerCase()), y.setAttribute("download", h.a.modify(a + "." + b)),
                                y.addEventListener("click", function(e) {
                                    w.downloadOnClick(e);
                                }, !1);
                            }
                            if (w.setStyle(y, l), r.link && "object" == typeof r.link && w.setStyle(y, r.link),
                            v.appendChild(y), w.appendFileSizeIcon(y, r.fsIcon, r.fsText), n[m]["3d"]) {
                                w.video.yt.show3D || (s = !0, v.setAttribute(w.video.dataAttr, "0"), v.style.display = "none");
                                var x = document.createElement("span");
                                x.textContent = g, w.setStyle(x, d), r.text && "object" == typeof r.text && w.setStyle(x, r.text),
                                y.appendChild(x);
                            }
                            n[m].noAudio && (w.video.yt.showMP4NoAudio || (s = !0, v.setAttribute(w.video.dataAttr, "0"),
                            v.style.display = "none"), w.appendNoSoundIcon(y, !!r && r.noSoundIcon)), c.appendChild(v),
                            f = !0, delete e[m];
                        }
                    } else for (var m in e) {
                        f && (c.lastChild.style.marginRight = "15px", c.appendChild(document.createTextNode(" ")));
                        var C = document.createElement("span");
                        C.style.whiteSpace = "nowrap";
                        var k = document.createElement("a");
                        k.href = e[m], k.title = o.a.i18n.getMessage("downloadTitle"), k.textContent = m,
                        w.setStyle(k, l), r.link && "object" == typeof r.link && w.setStyle(k, r.link),
                        C.appendChild(k), w.appendFileSizeIcon(k, r.fsIcon, r.fsText), c.appendChild(C),
                        f = !0, delete e[m];
                    }
                    if (!1 !== f) return i.appendChild(u), s;
                }
            }
        },
        playlist: {
            btnStyle: {
                display: "block",
                fontWeight: "bold",
                border: "none",
                textDecoration: "underline"
            },
            getFilelistHtml: function(e) {
                if (e && 0 != e.length) {
                    for (var t, n = 0, i = "", r = 0; r < e.length; r++) e[r].url && (i += e[r].url + "\r\n",
                    n++);
                    if (i) return n < 5 ? n = 5 : n > 14 && (n = 14), p.a.create(document.createDocumentFragment(), {
                        append: [ p.a.create("p", {
                            text: o.a.i18n.getMessage("filelistTitle"),
                            style: {
                                color: "#0D0D0D",
                                fontSize: "20px",
                                marginBottom: "11px",
                                marginTop: "5px"
                            }
                        }), p.a.create("p", {
                            style: {
                                marginBottom: "11px"
                            },
                            append: Object(f.a)(o.a.i18n.getMessage("filelistInstruction"))
                        }), p.a.create("p", {
                            text: o.a.i18n.getMessage("vkFoundFiles").replace("%d", e.length),
                            style: {
                                color: "#000",
                                marginBottom: "11px"
                            },
                            append: p.a.create("a", {
                                text: o.a.i18n.getMessage("playlist"),
                                href: "#",
                                class: "sf__playlist",
                                style: {
                                    display: "none",
                                    cssFloat: "right"
                                }
                            })
                        }), t = p.a.create("textarea", {
                            text: i,
                            rows: n,
                            cols: 60,
                            style: {
                                width: "100%",
                                whiteSpace: o.a.isFirefox || o.a.isGM && !o.a.isTM ? "normal" : "nowrap"
                            }
                        }), o.a.isChrome || o.a.isFirefox ? p.a.create("button", {
                            text: o.a.i18n.getMessage("copy"),
                            style: {
                                height: "27px",
                                backgroundColor: "#ffffff",
                                border: "1px solid #9e9e9e",
                                marginTop: "6px",
                                paddingLeft: "10px",
                                paddingRight: "10px",
                                borderRadius: "5px",
                                fontSize: "14px",
                                cursor: "pointer",
                                cssFloat: "right"
                            },
                            on: [ "click", function(e) {
                                var n = this;
                                n.disabled = !0, o.a.isFirefox ? (t.select(), document.execCommand("copy")) : o.a.sendMessage({
                                    action: "addToClipboard",
                                    text: i
                                }), setTimeout(function() {
                                    n.disabled = !1;
                                }, 1e3);
                            } ],
                            append: p.a.create("style", {
                                text: Object(a.a)({
                                    "#savefrom_popup_box": {
                                        append: {
                                            "button:hover:not(:disabled)": {
                                                backgroundColor: "#597A9E !important",
                                                borderColor: "#597A9E !important",
                                                color: "#fff"
                                            },
                                            "button:active": {
                                                opacity: .9
                                            }
                                        }
                                    }
                                })
                            })
                        }) : void 0 ]
                    });
                }
            },
            popupFilelist: function(e, t, n, o) {
                var i = w.playlist.getFilelistHtml(e);
                if (i) {
                    var r = w.popupDiv(i, o);
                    if (n) {
                        var a = r.querySelector("a.sf__playlist");
                        a && (a.addEventListener("click", function(n) {
                            return setTimeout(function() {
                                w.playlist.popupPlaylist(e, t, !0, o);
                            }, 100), n.preventDefault(), !1;
                        }, !1), w.setStyle(a, w.playlist.btnStyle));
                    }
                }
            },
            getInfoPopupTemplate: function() {
                var e = p.a.create("div", {
                    class: "sf-infoPopupTemplate",
                    style: {
                        width: "400px",
                        minHeight: "40px"
                    }
                }), t = p.a.create("div", {
                    style: {
                        backgroundSize: "48px",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center top",
                        display: "inline-block",
                        width: "60px",
                        height: "60px",
                        cssFloat: "left",
                        marginTop: "16px",
                        marginRight: "10px"
                    }
                }), n = p.a.create("div", {
                    style: {
                        display: "inline-block",
                        width: "330px"
                    }
                }), o = p.a.create("div", {
                    style: {
                        textAlign: "right"
                    },
                    append: p.a.create("style", {
                        text: Object(a.a)({
                            ".sf-infoPopupTemplate": {
                                append: [ {
                                    "a.sf-button": {
                                        padding: "1px 6px",
                                        display: "inline-block",
                                        textAlign: "center",
                                        height: "23px",
                                        lineHeight: "23px",
                                        textDecoration: "none"
                                    }
                                }, {
                                    selector: [ "button:hover", "a.sf-button:hover" ],
                                    style: {
                                        backgroundColor: "#597A9E !important",
                                        borderColor: "#597A9E !important",
                                        color: "#fff"
                                    }
                                } ]
                            }
                        })
                    })
                });
                return e.appendChild(t), e.appendChild(n), e.appendChild(o), {
                    icon: t,
                    buttonContainer: o,
                    textContainer: n,
                    body: e
                };
            },
            getM3U: function(e) {
                for (var t = "#EXTM3U\r\n", n = 0; n < e.length; n++) e[n].duration || (e[n].duration = "-1"),
                (e[n].title || e[n].duration) && (t += "#EXTINF:" + e[n].duration + "," + e[n].title + "\r\n"),
                t += e[n].url + "\r\n";
                return t;
            },
            getPlaylistHtml: function(e, t) {
                if (e && 0 != e.length) {
                    var n = e.length, i = w.dateToObj(), r = i.year + "-" + i.month + "-" + i.day + " " + i.hour + "-" + i.min, a = w.playlist.getM3U(e);
                    a = a.replace(/\r\n/g, "\n");
                    var s = Object(d.a)(a, "audio/x-mpegurl"), l = w.playlist.getInfoPopupTemplate();
                    return o.a.sendMessage({
                        action: "getWarningIcon",
                        color: "#00CCFF",
                        type: "playlist"
                    }, function(e) {
                        l.icon.style.backgroundImage = "url(" + e + ")";
                    }), p.a.create(l.textContainer, {
                        append: [ p.a.create("p", {
                            text: t || o.a.i18n.getMessage("playlistTitle"),
                            style: {
                                color: "#0D0D0D",
                                fontSize: "20px",
                                marginBottom: "11px",
                                marginTop: "13px"
                            }
                        }), p.a.create("p", {
                            text: o.a.i18n.getMessage("playlistInstruction"),
                            style: {
                                color: "#868686",
                                fontSize: "14px",
                                marginBottom: "13px",
                                lineHeight: "24px",
                                marginTop: "0px"
                            }
                        }), p.a.create("a", {
                            text: o.a.i18n.getMessage("filelist") + " (" + n + ")",
                            href: "#",
                            class: "sf__playlist",
                            style: {
                                display: "none",
                                fontSize: "14px",
                                marginBottom: "13px",
                                lineHeight: "24px",
                                marginTop: "0px"
                            }
                        }) ]
                    }), t || (t = "playlist"), t += " " + r, p.a.create(l.buttonContainer, {
                        append: [ p.a.create("a", {
                            text: o.a.i18n.getMessage("download"),
                            href: s,
                            download: h.a.modify(t + ".m3u"),
                            class: "sf-button",
                            style: {
                                width: "118px",
                                backgroundColor: "#ffffff",
                                border: "1px solid #9e9e9e",
                                margin: "12px",
                                marginBottom: "11px",
                                marginRight: "8px",
                                borderRadius: "5px",
                                fontSize: "14px",
                                cursor: "pointer"
                            }
                        }) ]
                    }), l.body;
                }
            },
            popupPlaylist: function(e, t, n, o) {
                var i = w.playlist.getPlaylistHtml(e, t);
                if (i) {
                    var r = w.popupDiv(i, o);
                    if (n) {
                        var a = r.querySelector("a.sf__playlist");
                        a && (a.addEventListener("click", function(n) {
                            return setTimeout(function() {
                                w.playlist.popupFilelist(e, t, !0, o);
                            }, 100), n.preventDefault(), !1;
                        }, !1), a.style.display = "inline", a = null);
                    }
                    for (var s, l = r.querySelectorAll("a[download]"), d = 0; s = l[d]; d++) s.addEventListener("click", w.downloadOnClick, !1);
                }
            }
        },
        popupCloseBtn: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAWUlEQVQ4y2NgGHHAH4j1sYjrQ+WIAvFA/B+I36MZpg8V+w9VQ9Al/5EwzDBkQ2AYr8uwaXiPQ0yfkKuwGUayIYQMI8kQqhlEFa9RLbCpFv1US5BUzSLDBAAARN9OlWGGF8kAAAAASUVORK5CYII=",
        popupDiv: function(e, t, n, o, i) {
            t || (t = "savefrom_popup_box"), n || (n = 580), o || (o = 520);
            var r = document.getElementById(t);
            r && r.parentNode.removeChild(r), r = p.a.create("div", {
                id: t,
                style: {
                    zIndex: "9999",
                    display: "block",
                    cssFloat: "none",
                    position: "fixed",
                    margin: "0",
                    padding: "0",
                    visibility: "hidden",
                    color: "#000",
                    background: "#fff",
                    border: "3px solid #c0cad5",
                    borderRadius: "7px",
                    overflow: "auto"
                }
            });
            var a = p.a.create("div", {
                style: {
                    display: "block",
                    cssFloat: "none",
                    position: "relative",
                    overflow: "auto",
                    margin: "0",
                    padding: "10px 15px"
                }
            });
            "function" == typeof e ? e(a) : a.appendChild(e);
            var s = p.a.create("img", {
                src: w.popupCloseBtn,
                alt: "x",
                width: 18,
                height: 18,
                style: {
                    position: "absolute",
                    top: "10px",
                    right: "15px",
                    opacity: "0.5",
                    cursor: "pointer"
                },
                on: [ [ "mouseenter", function() {
                    this.style.opacity = "0.9";
                } ], [ "mouseleave", function() {
                    this.style.opacity = "0.5";
                } ], [ "click", function() {
                    return r.parentNode && r.parentNode.removeChild(r), i && i(), !1;
                } ] ]
            });
            a.appendChild(s), r.appendChild(a), document.body.appendChild(r), r.offsetWidth > n && (r.style.width = n + "px"),
            r.offsetHeight > o && (r.style.height = o + "px", r.style.width = n + 20 + "px"),
            setTimeout(function() {
                var e = Math.floor((window.innerWidth - r.offsetWidth) / 2), t = Math.floor((window.innerHeight - r.offsetHeight) / 2);
                t < 0 && (t = 0), -1 !== location.host.indexOf("youtu") && t < 92 && (t = 92, r.style.height = r.offsetHeight - t - 10 + "px"),
                e < 0 && (e = 0), w.setStyle(r, {
                    top: t + "px",
                    left: e + "px",
                    visibility: "visible"
                });
            });
            var l = function e(t) {
                var n = t.target;
                n === r || w.isParent(n, r) || (r.parentNode && r.parentNode.removeChild(r), document.removeEventListener("click", e, !1),
                i && i());
            };
            return setTimeout(function() {
                document.addEventListener("click", l, !1);
            }, 100), r.addEventListener("close", function() {
                r.parentNode && r.parentNode.removeChild(r), document.removeEventListener("click", l, !1),
                i && i();
            }), r.addEventListener("kill", function() {
                r.parentNode && r.parentNode.removeChild(r), document.removeEventListener("click", l, !1);
            }), r;
        },
        popupDiv2: function(e) {
            var t = {
                id: "savefrom_popup_box",
                containerStyle: null,
                bodyStyle: null,
                content: null,
                container: null,
                body: null,
                _onClose: function() {
                    document.removeEventListener("click", t._onClose), n.parentNode && n.parentNode.removeChild(n),
                    t.onClose && t.onClose();
                }
            };
            Object.assign(t, e);
            var n = t.container = p.a.create("div", {
                id: t.id,
                style: {
                    zIndex: 9999,
                    display: "block",
                    position: "fixed",
                    background: "#fff",
                    border: "3px solid #c0cad5",
                    borderRadius: "7px"
                },
                append: [ p.a.create("style", {
                    text: Object(a.a)({
                        selector: "#" + t.id,
                        style: c
                    })
                }) ],
                on: [ [ "click", function(e) {
                    e.stopPropagation();
                } ] ]
            }), o = p.a.create("img", {
                src: w.popupCloseBtn,
                alt: "x",
                width: 18,
                height: 18,
                style: {
                    position: "absolute",
                    top: "10px",
                    right: "15px",
                    opacity: "0.5",
                    cursor: "pointer"
                },
                on: [ [ "mouseenter", function() {
                    this.style.opacity = "0.9";
                } ], [ "mouseleave", function() {
                    this.style.opacity = "0.5";
                } ], [ "click", t._onClose ] ]
            });
            n.appendChild(o);
            var r = t.body = p.a.create("div", {
                style: i({
                    display: "block",
                    position: "relative",
                    padding: "10px 15px",
                    overflow: "auto"
                }, t.bodyStyle)
            });
            return "function" == typeof t.content ? t.content(r) : r.appendChild(t.content),
            n.appendChild(r), document.body.appendChild(n), document.addEventListener("click", t._onClose),
            t;
        },
        showTooltip: function(e, t, n, o) {
            if (e) {
                var i = document.querySelector(".savefrom-tooltip");
                i || ((i = document.createElement("div")).className = "savefrom-tooltip", w.setStyle(i, {
                    position: "absolute",
                    opacity: 0,
                    zIndex: -1
                }), o && w.setStyle(i, o)), i.textContent = t, i.lastNode && i.lastNode === e ? r() : (i.lastNode && (u.a.off(i.lastNode, "mouseleave", a),
                u.a.off(i.lastNode, "mousemove", r), i.lastRow && u.a.off(i.lastRow, "mouseleave", a)),
                i.lastNode = e, n && (i.lastRow = n), u.a.on(e, "mouseleave", a), u.a.on(e, "mousemove", r, !1),
                n && u.a.on(n, "mouseleave", a), document.body.appendChild(i), r());
            }
            function r(t) {
                void 0 !== t && t.stopPropagation();
                var n = w.getPosition(e), o = w.getSize(i);
                0 == n.top && 0 == n.left || (n.top = n.top - o.height - 10, n.left = n.left - o.width / 2 + w.getSize(e).width / 2,
                n.left = Math.min(n.left, document.body.clientWidth + document.body.scrollLeft - o.width),
                n.top < document.body.scrollTop && (n.top = n.top + o.height + w.getSize(e).height + 20),
                n.top += "px", n.left += "px", n.zIndex = 9999, n.opacity = 1, w.setStyle(i, n));
            }
            function a() {
                i.parentNode && document.body.removeChild(i), i.lastNode = null, i.lastRow = null,
                w.setStyle(i, {
                    zIndex: -1,
                    opacity: 0
                }), u.a.off(e, "mouseleave", a), u.a.off(e, "mousemove", r), n && u.a.off(n, "mouseleave", a);
            }
        },
        embedDownloader: {
            dataAttr: "data-savefrom-get-links",
            dataIdAttr: "data-savefrom-container-id",
            containerClass: "savefrom-links-container",
            linkClass: "savefrom-link",
            panel: null,
            lastLink: null,
            style: null,
            hostings: {
                youtube: {
                    re: [ /^https?:\/\/(?:[a-z]+\.)?youtube\.com\/(?:#!?\/)?watch\?.*v=([\w\-]+)/i, /^https?:\/\/(?:[a-z0-9]+\.)?youtube\.com\/(?:embed|v)\/([\w\-]+)/i, /^https?:\/\/(?:[a-z]+\.)?youtu\.be\/([\w\-]+)/i ],
                    action: "getYoutubeLinks",
                    prepareLinks: function(e) {
                        var t = [], n = w.video.yt.format, o = e.meta || {};
                        for (var i in n) for (var r in n[i]) {
                            var a = o[r] || {};
                            if (e[r]) {
                                var s = i;
                                n[i][r].ext && (s = n[i][r].ext);
                                var l = n[i][r].quality;
                                a.quality && (l = a.quality), n[i][r].sFps && (l += " " + (a.fps || 60)), n[i][r]["3d"] && (l += " (3d)"),
                                t.push({
                                    name: i + " " + l,
                                    type: s,
                                    url: e[r],
                                    noSound: n[i][r].noAudio
                                });
                            }
                        }
                        return t;
                    }
                },
                vimeo: {
                    re: [ /^https?:\/\/(?:[\w\-]+\.)?vimeo\.com\/(?:\w+\#)?(\d+)/i, /^https?:\/\/player\.vimeo\.com\/video\/(\d+)/i, /^https?:\/\/(?:[\w\-]+\.)?vimeo\.com\/channels\/(?:[^\/]+)\/(\d+)$/i, /^https?:\/\/vimeo\.com\/(?:.+)clip_id=(\d+)/i ],
                    action: "getVimeoLinks",
                    prepareLinks: function(e) {
                        return e.map(function(e) {
                            var t = e.ext;
                            return t || (t = "MP4", -1 != e.url.search(/\.flv($|\?)/i) && (t = "FLV")), e.name = e.name ? e.name : t,
                            e.type = e.type ? e.type : t, e.ext = t, e;
                        });
                    }
                },
                vk: {
                    re: [ /^https?:\/\/(?:[\w\-]+\.)?(?:vk\.com|vkontakte\.ru)\/(?:[^\/]+\/)*(?:[\w\-\.]+\?.*z=)?(video-?\d+_-?\d+\?list=[0-9a-z]+|video-?\d+_-?\d+)/i, /^https?:\/\/(?:[\w\-]+\.)?(?:vk\.com|vkontakte\.ru)\/video_ext\.php\?(.+)/i ],
                    action: "getVKLinks"
                },
                dailymotion: {
                    re: [ /^http:\/\/(?:www\.)?dai\.ly\/([a-z0-9]+)_?/i, /^https?:\/\/(?:[\w]+\.)?dailymotion\.com(?:\/embed|\/swf)?\/video\/([a-z0-9]+)_?/i ],
                    action: "getDailymotionLinks"
                },
                facebook: {
                    re: [ /^https?:\/\/(?:[\w]+\.)?facebook\.com(?:\/video)?\/video.php.*[?&]{1}v=([0-9]+).*/i, /^https?:\/\/(?:[\w]+\.)?facebook\.com\/.+\/videos(?:\/\w[^\/]+)?\/(\d+)/i ],
                    action: "getFacebookLinks"
                }
            },
            init: function(e) {
                this.style = e, this.panel && w.popupMenu.removePanel(), this.panel = null, this.lastLink = null;
                var t, n = document.querySelectorAll("a[" + this.dataAttr + "]"), o = n.length;
                for (t = 0; t < o; t++) [ "savefrom.net", "sf-addon.com" ].indexOf(w.getTopLevelDomain(n[t].hostname)) > -1 && (n[t].removeEventListener("click", this.onClick, !1),
                n[t].addEventListener("click", this.onClick, !1));
                document.body && (document.body.removeEventListener("click", this.onBodyClick, !0),
                document.body.addEventListener("click", this.onBodyClick, !0));
            },
            checkUrl: function(e) {
                for (var t in this.hostings) for (var n = this.hostings[t], o = 0, i = n.re.length; o < i; o++) {
                    var r = e.match(n.re[o]);
                    if (r) return {
                        hosting: t,
                        action: n.action,
                        extVideoId: r[1]
                    };
                }
                return null;
            },
            reMapHosting: function(e) {
                return {
                    getYoutubeLinks: "youtube",
                    getVimeoLinks: "vimeo",
                    getDailymotionLinks: "dailymotion",
                    getFacebookLinks: "facebook",
                    getVKLinks: "vk"
                }[e];
            },
            onClick: function(e, t) {
                var n = w.embedDownloader;
                if (!t) {
                    for (t = e.target; t.parentNode && "A" !== t.nodeName; ) t = t.parentNode;
                    if (!t) return;
                }
                var i = t.getAttribute("data-savefrom-get-links");
                if (i && 0 === e.button && !e.ctrlKey && !e.shiftKey) {
                    if (n.lastLink === t && n.panel && "none" != n.panel.style.display) return n.lastLink = null,
                    n.panel.style.display = "none", e.preventDefault(), void e.stopPropagation();
                    n.lastLink = t;
                    var r = n.checkUrl(i);
                    if (r) {
                        e.preventDefault(), e.stopPropagation();
                        var a = {
                            action: r.action,
                            extVideoId: r.extVideoId
                        };
                        return n.showLinks(o.a.i18n.getMessage("download") + " ...", null, t), o.a.sendMessage(a, function(e) {
                            var i = r.hosting;
                            e.action != a.action && (i = n.reMapHosting(e.action)), e.links ? n.showLinks(e.links, e.title, t, i, !0) : n.showLinks(o.a.i18n.getMessage("noLinksFound"), null, t, void 0, !0);
                        }), !1;
                    }
                }
            },
            onBodyClick: function(e) {
                var t = w.embedDownloader, n = e.target;
                if (!t.panel || "none" == t.panel.style.display) {
                    if ("A" !== n.tagName && Object(s.a)(n, "A " + n.tagName)) for (;n.parentNode && "A" !== n.tagName; ) n = n.parentNode;
                    if ("A" !== n.nodeName) return;
                    return n.hasAttribute(t.dataAttr) && [ "savefrom.net", "sf-addon.com" ].indexOf(w.getTopLevelDomain(n.hostname)) > -1 ? t.onClick(e, n) : void 0;
                }
                t.panel === n || t.panel.contains(n) || (t.lastLink = null, t.panel.style.display = "none",
                e.preventDefault(), e.stopPropagation());
            },
            hidePanel: function() {
                this.panel && (this.panel.style.display = "none");
            },
            createMenu: function(e, t, n, i, r) {
                var a = o.a.i18n.getMessage("noLinksFound");
                "string" == typeof e ? a = e : void 0 !== w.popupMenu.prepareLinks[i] && e && (a = w.popupMenu.prepareLinks[i](e, t));
                var s = {
                    links: a,
                    button: n,
                    popupId: void 0,
                    showFileSize: !0,
                    containerClass: this.containerClass,
                    linkClass: this.linkClass,
                    style: {
                        popup: this.style ? this.style.container : void 0,
                        item: this.style ? this.style.link : void 0
                    },
                    isUpdate: r
                };
                r && this.panel ? w.popupMenu.update(this.panel, s) : this.panel = w.popupMenu.create(s);
            },
            showLinks: function(e, t, n, i, r) {
                var a, s = n.getAttribute(this.dataIdAttr);
                if (s && (a = document.getElementById(s)), a) if (this.panel && (this.panel.style.display = "none"),
                "string" == typeof e) a.textContent = e; else if (e && 0 != e.length) {
                    i && this.hostings[i] && this.hostings[i].prepareLinks && (e = this.hostings[i].prepareLinks(e)),
                    a.textContent = "";
                    for (var l = 0; l < e.length; l++) if (e[l].url && e[l].name) {
                        (n = document.createElement("a")).href = e[l].url, n.title = o.a.i18n.getMessage("downloadTitle"),
                        n.appendChild(document.createTextNode(e[l].name));
                        var d = document.createElement("span");
                        d.className = this.linkClass, d.appendChild(n), a.appendChild(d), w.appendFileSizeIcon(n),
                        e[l].noSound && w.appendNoSoundIcon(n), t && !e[l].noTitle && e[l].type && (n.setAttribute("download", h.a.modify(t + "." + e[l].type.toLowerCase())),
                        n.addEventListener("click", w.downloadOnClick, !1));
                    }
                } else a.textContent = o.a.i18n.getMessage("noLinksFound"); else this.createMenu(e, t, n, i, r);
            }
        },
        createUmmyInfo: function(e, t) {
            e = e || {};
            var n, i, r, s, l, d, c, h, m = Object.assign({
                vid: 111,
                utm_source: "savefrom-helper",
                utm_medium: "youtube-helper"
            }, e.params);
            m.utm_campaign || ("hd" === e.itemType ? m.utm_campaign = "youtube-helper-hd" : "mp3" === e.itemType && (m.utm_campaign = "youtube-helper-mp3")),
            n = /^Mac/.test(navigator.platform) && /^yt-/.test(m.video) ? "http://videodownloader.ummy.net/save-from-youtube.html?" + y.stringify({
                vid: m.vid,
                video: m.video,
                utm_source: "savefrom-helper",
                utm_medium: "youtube-helper",
                utm_campaign: "ummy",
                utm_content: "ummy_integration_h"
            }) : "http://videodownloader.ummy.net/?" + y.stringify(m), e.posLeft ? (d = {
                border: "8px solid transparent",
                borderLeft: "10px solid rgb(192, 187, 187)",
                borderRight: 0,
                top: "8px",
                right: "11px"
            }, c = Object.assign({}, d, {
                right: "12px",
                borderLeft: "10px solid #fff"
            }), h = {
                right: "21px"
            }, e.darkTheme && (i = {
                borderLeftColor: "rgba(255, 255, 255, 0.4)"
            }, r = {
                borderLeftColor: "rgba(28,28,28, 0.6)"
            })) : (d = {
                border: "8px solid transparent",
                borderRight: "10px solid rgb(192, 187, 187)",
                borderLeft: 0,
                top: "8px",
                left: "11px"
            }, c = Object.assign({}, d, {
                left: "12px",
                borderRight: "10px solid #fff"
            }), h = {
                left: "21px"
            }, e.darkTheme && (i = {
                borderRightColor: "#fff"
            }, r = {
                borderRightColor: "#000"
            })), l = e.darkTheme ? {
                a: {
                    color: "#eee"
                }
            } : {}, s = e.darkTheme ? {
                backgroundColor: "rgba(28,28,28,0.8)",
                border: "1px solid rgba(255, 255, 255, 0.4)",
                color: "#fff"
            } : {
                backgroundColor: "#fff",
                border: "1px solid #ccc"
            };
            var g = p.a.create(document.createDocumentFragment(), {
                append: [ p.a.create("span", {
                    style: Object.assign({
                        display: "inline-block",
                        width: 0,
                        position: "absolute"
                    }, d, i)
                }), p.a.create("span", {
                    style: Object.assign({
                        display: "inline-block",
                        width: 0,
                        position: "absolute",
                        zIndex: 1
                    }, c, r)
                }) ]
            }), v = null, b = null, w = p.a.create("div", {
                class: "sf-ummy-info-popup-container",
                style: {
                    position: "absolute",
                    zIndex: 9999
                },
                append: [ g, b = p.a.create("div", {
                    class: "sf-ummy-info-popup",
                    style: Object.assign({
                        position: "relative",
                        backgroundColor: "#fff",
                        border: "1px solid #ccc",
                        padding: "6px 5px",
                        textAlign: "center",
                        maxWidth: "240px",
                        lineHeight: "16px",
                        fontSize: "12px",
                        fontFamily: "arial, sans-serif",
                        cursor: "default"
                    }, h, s),
                    append: [ Object(f.a)(o.a.i18n.getMessage("ummyMenuInfo").replace("{url}", n)), p.a.create("label", {
                        style: {
                            verticalAlign: "middle",
                            display: "block"
                        },
                        append: [ v = p.a.create("input", {
                            type: "checkbox",
                            name: "showUmmyInfo",
                            style: {
                                verticalAlign: "middle"
                            }
                        }), o.a.i18n.getMessage("tooltipHide") ]
                    }), p.a.create("style", {
                        text: Object(a.a)({
                            ".sf-ummy-info-popup": {
                                append: Object.assign({
                                    "> p": {
                                        margin: "0 0 .8em 0"
                                    },
                                    "> p.center": {
                                        textAlign: "center"
                                    },
                                    "> p > .green-btn-2.arrow": {
                                        color: "#fff",
                                        background: "#84bd07",
                                        borderRadius: "5px",
                                        display: "inline-block",
                                        position: "relative",
                                        lineHeight: 1,
                                        padding: "8px 34px 8px 10px",
                                        textDecoration: "none",
                                        fontSize: "12px"
                                    },
                                    "> p > .green-btn-2.arrow:hover": {
                                        color: "#fff",
                                        opacity: .8
                                    },
                                    "> p > .green-btn-2.arrow:after": {
                                        background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAAjklEQVQoke3RsRGCQBCF4YuJsQDoQMpjKMImtAjth9xMEj4DF4c5QDH3n7lk773b3XsJNzTpR9DglrwYcUG9w1iHdoTpgYkBJ5QrxkPcDXNDQm/JHR2KOF3UcvoUgnZL8KFBi2I+Yrk2YsZjsaIsBVQ4i08KxqhVu1OYBLji+E/hzTKFlV13pfAVGynkPAFtrlNTMRczMgAAAABJRU5ErkJggg==) 0 0 no-repeat",
                                        content: '""',
                                        display: "block",
                                        position: "absolute",
                                        width: "16px",
                                        height: "14px",
                                        top: "50%",
                                        right: "10px",
                                        marginTop: "-7px"
                                    },
                                    input: {
                                        display: "inline-block"
                                    }
                                }, l)
                            }
                        })
                    }) ]
                }) ],
                on: [ [ "mouseclick", function(e) {
                    e.stopPropagation();
                } ], [ "mousedown", function(e) {
                    e.stopPropagation();
                } ] ]
            });
            return o.a.sendMessage({
                action: "getUmmyIcon"
            }, function(e) {
                var t = b.querySelector("img");
                t && (t.src = e, t.style.verticalAlign = "text-bottom");
            }), v.checked = !x.preferences.showUmmyInfo, v.addEventListener("change", function(e) {
                e.preventDefault(), e.stopPropagation(), this.checked ? (u.a.trigger(t, "sfRmInfoPopup"),
                x.preferences.showUmmyInfo = 0) : (u.a.trigger(t, "sfAddInfoPopup"), x.preferences.showUmmyInfo = 1),
                o.a.sendMessage({
                    action: "updateOption",
                    key: "showUmmyInfo",
                    value: x.preferences.showUmmyInfo
                });
            }), e.onCreateUmmyInfo && e.onCreateUmmyInfo(w), w;
        },
        bindUmmyInfo: function(e, t) {
            if (((t = t || {}).noUmmy || x.preferences.showUmmyInfo) && !(t.widthLimit && document.documentElement.offsetWidth < t.widthLimit)) {
                t.leftOffset = t.leftOffset || 21;
                var n = null, o = null, i = null, r = 8, a = null, s = null, l = null, d = function() {
                    clearTimeout(o), o = setTimeout(function() {
                        n && (n.style.display = "none");
                    }, 50);
                }, c = function() {
                    if (t.noUmmy || x.preferences.showUmmyInfo) {
                        clearTimeout(o);
                        var c = w.getPosition(e);
                        n ? i = c.top - 4 : (n = t.expUmmyInfo ? t.expUmmyInfo(t.createUmmyInfoDetails, e) : w.createUmmyInfo(t.createUmmyInfoDetails, e),
                        a = n.firstChild, s = a.nextElementSibling, i = c.top - 4, u.a.on(n, "mouseenter", function() {
                            clearTimeout(o);
                        }), u.a.on(n, "mouseleave", d)), n.style.top = i + "px", "1" !== n.dataset.hide && (!function(e) {
                            var o = w.getPosition(e);
                            if (t.posLeft) n.style.right = document.documentElement.clientWidth - o.left - t.leftOffset + "px"; else {
                                var i = w.getSize(e);
                                n.style.left = i.width + o.left - t.leftOffset + "px";
                            }
                        }(e), n.parentNode || (n.style.display = "none", (t.container || document.body).appendChild(n)),
                        "block" !== n.style.display && (n.style.display = "block", u.a.trigger(n, "sfShowInfoPopup")),
                        setTimeout(function() {
                            var e = window.innerHeight, t = n.clientHeight, o = window.scrollY;
                            if (t + i > e + o) {
                                var l = e - t + o;
                                if (l < 0) return;
                                if (i === l) return;
                                n.style.top = l + "px";
                                var d = 8 - (e - (t + i) + o);
                                r !== d && (r = d, a.style.top = r + "px", s.style.top = r + "px");
                            } else 8 !== r && (r = 8, a.style.top = r + "px", s.style.top = r + "px");
                        }), function e() {
                            clearTimeout(l), l = setTimeout(function() {
                                if (n && n.parentNode) {
                                    if ("none" !== n.style.display) return e();
                                    n.parentNode.removeChild(n);
                                }
                            }, 3e4);
                        }());
                    }
                };
                u.a.on(e, "mouseenter", c), u.a.on(e, "mouseleave", d), u.a.on(e, "sfRmInfoPopup", function() {
                    u.a.off(e, "mouseenter", c);
                }), u.a.on(e, "sfAddInfoPopup", function() {
                    u.a.on(e, "mouseenter", c);
                });
            }
        },
        popupMenu: {
            popupId: "sf_popupMenu",
            popup: void 0,
            popupStyle: void 0,
            dataArrtVisible: "data-isVisible",
            extStyleCache: void 0,
            ummyIcon: null,
            badgeQualityList: [ "8K", "4K", "2160", "1440", "1080", "720", "ummy" ],
            createBadge: function(e, t) {
                var n = this;
                t = t || {};
                var i = {
                    display: "inline-block",
                    lineHeight: "18px",
                    width: "19px",
                    height: "17px",
                    color: "#fff",
                    fontSize: "12px",
                    borderRadius: "2px",
                    verticalAlign: "middle",
                    textAlign: "center",
                    paddingRight: "2px",
                    fontWeight: "bold",
                    marginLeft: "3px"
                };
                for (var r in t.containerStyle) i[r] = t.containerStyle[r];
                var a = p.a.create("div", {
                    style: i
                });
                return "1080" === e || "2160" === e || "1440" === e || "720" === e ? (a.textContent = "HD",
                a.style.backgroundColor = "#505050", a.style.paddingRight = "1px") : "8K" === e || "4K" === e ? (a.textContent = "HD",
                a.style.paddingRight = "1px", a.style.backgroundColor = "rgb(247, 180, 6)") : "mp3" === e || "MP3" === e ? (a.textContent = "MP3",
                a.style.width = "26px", a.style.paddingRight = "1px", a.style.backgroundColor = "#505050") : "ummy" === e && (this.ummyIcon ? a.style.background = "url(" + this.ummyIcon + ") center center no-repeat" : o.a.sendMessage({
                    action: "getUmmyIcon"
                }, function(e) {
                    a.style.background = "url(" + (n.ummyIcon = e) + ") center center no-repeat";
                })), a;
            },
            getTitleNode: function(e) {
                var t = w.popupMenu, n = p.a.create("span", {
                    style: {
                        cssFloat: "left"
                    }
                });
                if ("converter" === e.extra) {
                    var o = document.createDocumentFragment();
                    -1 !== [ "MP3", "8K", "4K", "1440", "1080", "720" ].indexOf(e.format) ? o.appendChild(t.createBadge(e.format, {
                        containerStyle: {
                            marginLeft: 0
                        }
                    })) : o.appendChild(document.createTextNode(e.format)), p.a.create(n, {
                        append: [ o, " ", e.quality ]
                    }), o = null;
                } else if ("ummy" === e.quality) {
                    o = document.createDocumentFragment();
                    null !== e.uQuality && (-1 !== [ "8K", "4K", "1440", "1080", "720" ].indexOf(e.uQuality) ? o.appendChild(document.createTextNode(e.uQuality)) : o.appendChild(t.createBadge(e.uQuality, {
                        containerStyle: {
                            marginLeft: 0
                        }
                    }))), p.a.create(n, {
                        append: [ o, " ", "Ummy" ]
                    }), o = null;
                } else if (e.itemText) n.textContent = e.itemText; else {
                    var i = e.quality ? " " + e.quality : "", r = e.format ? e.format : "???", a = e["3d"] ? "3D " : "", s = "";
                    e.sFps && (s += " " + (e.fps || 60)), n.textContent = a + r + i + s;
                }
                return -1 !== t.badgeQualityList.indexOf(String(e.quality)) && n.appendChild(t.createBadge(String(e.quality))),
                n;
            },
            createPopupItem: function(e, t) {
                var n, i = w.popupMenu;
                if ("-" === (n = "string" == typeof e ? e : e.href)) return {
                    el: p.a.create("div", {
                        style: {
                            display: "block",
                            margin: "1px 0",
                            borderTop: "1px solid rgb(214, 214, 214)"
                        }
                    })
                };
                var r = document.createElement("-text-" === n ? "div" : "a");
                t.linkClass && r.classList.add(t.linkClass);
                var a = {
                    display: "block",
                    padding: "0 5px",
                    textDecoration: "none",
                    whiteSpace: "nowrap",
                    overflow: "hidden"
                };
                if (e.isHidden && (r.setAttribute(i.dataArrtVisible, "0"), a.display = "none"),
                w.setStyle(r, a), "-text-" === n) return r.style.lineHeight = "22px", {
                    el: r
                };
                if (r.href = n, "#" === n) return {
                    el: r
                };
                if ((o.a.isGM || o.a.isSafari) && ("ummy" === e.quality || e.extra || (r.title = o.a.i18n.getMessage("downloadTitle"))),
                e.forceDownload) {
                    var s = "";
                    if (e.title) {
                        var l = (e.ext || e.format || "").toLowerCase();
                        l && (l = "." + l), s = e.title + l;
                    }
                    r.setAttribute("download", h.a.modify(s)), r.addEventListener("click", function(e) {
                        w.downloadOnClick(e, null, {
                            el: this
                        });
                    }, !1);
                }
                var d = [];
                e.func && (Array.isArray(e.func) ? d.push.apply(d, e.func) : d.push(e.func)), t.onItemClick && -1 === d.indexOf(t.onItemClick) && d.push(t.onItemClick),
                d.length && r.addEventListener("click", function(t) {
                    var n = this;
                    d.forEach(function(o) {
                        return o.call(n, t, e);
                    });
                }, !1), e.isBlank && r.setAttribute("target", "_blank"), r.appendChild(i.getTitleNode(e));
                var c = p.a.create("span", {
                    style: {
                        cssFloat: "right",
                        lineHeight: "22px",
                        height: "22px"
                    }
                }), f = {
                    top: "5px",
                    verticalAlign: "top"
                };
                for (var m in t.sizeIconStyle) f[m] = t.sizeIconStyle[m];
                e.noAudio && w.appendNoSoundIcon(c, f);
                var g = null;
                if (e.noSize || (c.addEventListener("click", function e(t) {
                    "IMG" === c.firstChild.tagName && (t.preventDefault(), t.stopPropagation(), u.a.trigger(c.firstChild, "click", {
                        cancelable: !0
                    })), this.removeEventListener("click", e);
                }), g = w.appendFileSizeIcon(r, f, {
                    marginLeft: 0
                }, void 0, !0, c, e)), r.appendChild(c), "ummy" === e.quality) {
                    var v = Object.assign({}, t.bindUmmyInfoDetails), y = v.createUmmyInfoDetails = Object.assign({
                        itemType: e.uIsAudio ? "mp3" : "hd"
                    }, v.createUmmyInfoDetails);
                    y.params = Object.assign({
                        video: e.videoId,
                        vid: e.vid
                    }, y.params), w.bindUmmyInfo(r, v);
                }
                return {
                    el: r,
                    sizeIcon: g,
                    prop: e
                };
            },
            sortMenuItems: function(e, t) {
                void 0 === t && (t = {});
                var n = [ "ummy", "Audio Opus", "Audio Vorbis", "Audio AAC", "3GP", "WebM", "FLV", "MP4" ], o = {
                    Mobile: 280,
                    LD: 280,
                    SD: 360,
                    HD: 720,
                    ummy: 1
                };
                t.strQualityExtend && Object.assign(o, t.strQualityExtend);
                var i = {}, r = [], a = [], s = [], l = [], d = [], u = [], c = [], p = [];
                e.forEach(function(e) {
                    var n = e.prop;
                    t.noProp && (n = e);
                    var f = n.sort || {};
                    if (!n.format) return p.push(e), 1;
                    if (n.isOther) p.push(e); else if (n.isSubtitle) l.push(e); else if (n.noVideo) r[n.quality] = parseInt(n.quality),
                    s.push(e); else {
                        var h = f.size || o[n.quality] || -1;
                        if (-1 === h && (h = "K" === String(n.quality).substr(-1) ? 1e3 * parseInt(n.quality) : parseInt(n.quality)),
                        t.maxSize && h > t.maxSize) return 1;
                        if (t.minSize && h < t.minSize) return 1;
                        i[n.quality] = h, n.noAudio ? n.sFps ? d.push(e) : u.push(e) : n["3d"] ? c.push(e) : a.push(e);
                    }
                });
                var f = function(e, t) {
                    return e.noVideo && t.noVideo ? function(e, t) {
                        return r[e.quality] > r[t.quality] ? -1 : r[e.quality] === r[t.quality] ? 0 : 1;
                    }(e, t) : e.noVideo ? 1 : t.noVideo ? -1 : n.indexOf(e.format) > n.indexOf(t.format) ? -1 : n.indexOf(e.format) === n.indexOf(t.format) ? 0 : 1;
                }, h = function(e, n) {
                    var o = e.prop, r = n.prop;
                    t.noProp && (o = e, r = n);
                    var a = function(e, t) {
                        return i[e.quality] > i[t.quality] ? -1 : i[e.quality] === i[t.quality] ? 0 : 1;
                    }(o, r);
                    return 0 !== a ? a : f(o, r);
                };
                a.sort(h), c.sort(h), s.sort(h), d.sort(h), u.sort(h);
                var m = null;
                return t.typeList ? (m = [], -1 !== t.typeList.indexOf("video") && (m = m.concat(a)),
                -1 !== t.typeList.indexOf("3d") && (m = m.concat(c)), -1 !== t.typeList.indexOf("audio") && (m = m.concat(s)),
                -1 !== t.typeList.indexOf("mute") && (m = m.concat(u)), -1 !== t.typeList.indexOf("mute60") && (m = m.concat(d)),
                -1 !== t.typeList.indexOf("subtitles") && (m = m.concat(l)), -1 !== t.typeList.indexOf("other") && (m = m.concat(p))) : m = a.concat(c, s, l, d, u, p),
                t.groupCompare && m.sort(h), m;
            },
            removePanel: function() {
                null !== this.popup.parentNode && this.popup.parentNode.removeChild(this.popup),
                void 0 !== this.popupStyle && null !== this.popupStyle.parentNode && this.popupStyle.parentNode.removeChild(this.popupStyle),
                this.popup = void 0, this.popupStyle = void 0;
            },
            getHiddenList: function(e, t) {
                var n = this, i = document.createDocumentFragment();
                if (e.length < 8) p.a.create(i, {
                    append: e
                }); else {
                    var r = p.a.create("div", {
                        style: {
                            maxHeight: "192px",
                            overflowY: "scroll",
                            display: "none"
                        },
                        on: [ [ "wheel", function(e) {
                            e.wheelDeltaY > 0 && 0 === this.scrollTop ? e.preventDefault() : e.wheelDeltaY < 0 && this.scrollHeight - (this.offsetHeight + this.scrollTop) <= 0 && e.preventDefault();
                        } ], function() {
                            var e = !1;
                            return [ "scroll", function() {
                                if (0 !== this.scrollTop) {
                                    if (e) return;
                                    e = !0, this.style.boxShadow = "rgba(0, 0, 0, 0.40) -2px 1px 2px 0px inset";
                                } else {
                                    if (!e) return;
                                    e = !1, this.style.boxShadow = "";
                                }
                            } ];
                        }() ],
                        append: e
                    });
                    r.setAttribute(n.dataArrtVisible, "0"), i.appendChild(r);
                }
                var a = n.createPopupItem("-", t).el;
                i.appendChild(a);
                var s = n.createPopupItem("#", t).el;
                return p.a.create(s, {
                    text: o.a.i18n.getMessage("more") + " " + String.fromCharCode(187),
                    data: {
                        visible: "0"
                    },
                    on: [ "click", function(e) {
                        e.preventDefault();
                        var t, i = this.dataset.visible;
                        i > 0 ? (i--, t = 187) : (i++, t = 171), this.textContent = o.a.i18n.getMessage("more") + " " + String.fromCharCode(t),
                        this.dataset.visible = i;
                        for (var r, a = this.parentNode.querySelectorAll("*[" + n.dataArrtVisible + "]"), s = 0; r = a[s]; s++) r.style.display = 1 === i ? "block" : "none",
                        r.setAttribute(n.dataArrtVisible, i);
                    } ]
                }), i.appendChild(s), 0 === t.visibleCount && u.a.trigger(s, "click", {
                    cancelable: !0
                }), i;
            },
            getContent: function(e) {
                var t = this, n = e.links, i = document.createDocumentFragment(), r = [];
                if ("string" == typeof n) {
                    var a = t.createPopupItem("-text-", e).el;
                    a.textContent = n, i.appendChild(a);
                } else if (0 === n.length) {
                    var s = t.createPopupItem("-text-", e).el;
                    s.textContent = o.a.i18n.getMessage("noLinksFound"), i.appendChild(s);
                } else {
                    var l = [];
                    n.forEach(function(n) {
                        l.push(t.createPopupItem(n, e));
                    });
                    var d = [];
                    (l = t.sortMenuItems(l, e.sortDetails)).forEach(function(t) {
                        if (t.prop.isHidden) return d.push(t.el), 1;
                        i.appendChild(t.el), e.showFileSize && t.sizeIcon && r.push(t.sizeIcon);
                    }), e.visibleCount = l.length - d.length, d.length > 0 && (e.getHiddenListFunc ? i.appendChild(e.getHiddenListFunc(d, e)) : i.appendChild(t.getHiddenList(d, e)));
                }
                return {
                    sizeIconList: r,
                    content: i
                };
            },
            create: function(e) {
                var t = e.button, n = w.popupMenu;
                if (e.linkClass = e.linkClass || "sf-menu-item", e.offsetRight = e.offsetRight || 0,
                e.offsetTop = e.offsetTop || 0, e.parent = e.parent || document.body, !e.isUpdate || void 0 !== n.popup && "none" !== n.popup.style.display) {
                    n.popup && n.removePanel();
                    var o = n.popup = document.createElement("div"), i = "#" + n.popupId;
                    e.popupId ? (i = "#" + e.popupId, o.id = e.popupId) : e.containerClass ? (i = "." + e.containerClass,
                    o.classList.add(e.containerClass)) : o.id = n.popupId;
                    var r = {
                        display: "block",
                        position: "absolute",
                        minHeight: "24px",
                        cursor: "default",
                        textAlign: "left",
                        whiteSpace: "nowrap",
                        fontFamily: "arial, sans-serif"
                    };
                    e.extStyle && delete r.display;
                    var s = w.getPosition(t, e.parent), l = w.getSize(t);
                    r.top = s.top + e.offsetTop + l.height + "px", r.left = s.left + e.offsetRight + "px",
                    w.setStyle(o, r);
                    var d = {
                        "background-color": "#fff",
                        "z-index": "9999",
                        "box-shadow": "0 2px 10px 0 rgba(0,0,0,0.2)",
                        border: "1px solid #ccc",
                        "border-radius": "3px",
                        "font-size": "12px",
                        "font-weight": "bold",
                        "min-width": "190px"
                    };
                    if (e.style && e.style.popup) for (var c in e.style.popup) {
                        var f = e.style.popup[c];
                        d[c] = f;
                    }
                    w.addStyleRules(i, d);
                    var h = {
                        "line-height": "24px",
                        color: "#3D3D3D"
                    };
                    if (e.style && e.style.item) for (var c in e.style.item) {
                        f = e.style.item[c];
                        h[c] = f;
                    }
                    w.addStyleRules(i + " ." + e.linkClass, h);
                    var m = function(e) {
                        e.stopPropagation();
                    };
                    for (p.a.create(o, {
                        on: [ [ "click", m ], [ "mouseover", m ], [ "mouseup", m ], [ "mousedown", m ], [ "mouseout", m ] ]
                    }); null !== o.firstChild; ) o.removeChild(o.firstChild);
                    var g = n.getContent.call(n, e), v = g.sizeIconList;
                    g = g.content, o.appendChild(g);
                    var y = "#2F8AFF", b = "#fff";
                    e.style && e.style.hover && (y = e.style.hover.backgroundColor || y, b = e.style.hover.color || b);
                    var x = n.popupStyle = document.createElement("style");
                    if (x.textContent = Object(a.a)({
                        selector: i,
                        append: {
                            "a:hover": {
                                backgroundColor: y,
                                color: b
                            },
                            "> a:first-child": {
                                borderTopLeftRadius: "3px",
                                borderTopRightRadius: "3px"
                            },
                            "> a:last-child": {
                                borderBottomLeftRadius: "3px",
                                borderBottomRightRadius: "3px"
                            }
                        }
                    }), e.parent.appendChild(x), e.parent.appendChild(o), e.extStyle) {
                        void 0 !== w.popupMenu.extStyleCache && null !== w.popupMenu.extStyleCache.parentNode && w.popupMenu.extStyleCache.parentNode.removeChild(w.popupMenu.extStyleCache);
                        var C = "sf-extElStyle_" + i.substr(1), k = "sf-extBodyStyle_" + i.substr(1);
                        null === document.querySelector("style." + k) && document.body.appendChild(p.a.create("style", {
                            class: k,
                            text: Object(a.a)({
                                selector: i,
                                style: {
                                    display: "none"
                                }
                            })
                        })), w.popupMenu.extStyleCache = e.extStyle.appendChild(p.a.create("style", {
                            class: C,
                            text: Object(a.a)({
                                selector: "body " + i,
                                style: {
                                    display: "block"
                                }
                            })
                        }));
                    }
                    return setTimeout(function() {
                        v.forEach(function(e) {
                            u.a.trigger(e, "click", {
                                bubbles: !1,
                                cancelable: !0
                            });
                        });
                    }), o;
                }
            },
            update: function(e, t) {
                for (var n = w.popupMenu; null !== e.firstChild; ) e.removeChild(e.firstChild);
                var o = n.getContent.call(n, t), i = o.sizeIconList;
                o = o.content, e.appendChild(o), setTimeout(function() {
                    i.forEach(function(e) {
                        u.a.trigger(e, "click", {
                            bubbles: !1,
                            cancelable: !0
                        });
                    });
                });
            },
            preprocessItem: {
                srt2url: function(e, t) {
                    var n = e.srt, o = Object(d.a)(n, "text/plain");
                    t.ext = "srt", t.format = "SRT", t.href = o, t.noSize = !0;
                }
            },
            prepareLinks: {
                youtube: function(e, t, n, i) {
                    i = i || {}, n = n || [], e = Object.assign({}, e);
                    var r = w.video.yt;
                    r.init();
                    var a = [], s = null, l = e.meta || {};
                    return Object.keys(r.format).forEach(function(n) {
                        var o = r.format[n];
                        return Object.keys(o).forEach(function(i) {
                            var d = e[i];
                            if (d) {
                                var u = !1;
                                r.showFormat[n] || (u = !0);
                                var c = o[i];
                                c["3d"] && !r.show3D && (u = !0), c.noAudio && !r.showMP4NoAudio && (u = !0), s = {
                                    href: d,
                                    isHidden: u,
                                    title: t,
                                    format: n,
                                    itag: i,
                                    forceDownload: !0
                                }, Object.assign(s, c);
                                var p = l[i];
                                p && (p.quality && (s.quality = p.quality), p.fps && (s.fps = p.fps)), a.push(s),
                                delete e[i];
                            }
                        });
                    }), (e.ummy || e.ummyAudio) && function() {
                        var t = null, n = -1, o = w.popupMenu.badgeQualityList;
                        a.forEach(function(e) {
                            var t = o.indexOf(e.quality);
                            -1 !== t && (-1 === n || t < n) && (n = t);
                        }), -1 !== n && (t = o[n]);
                        var r = l.videoId;
                        r && [ "ummy", "ummyAudio" ].forEach(function(n) {
                            var o = e[n];
                            o && (s = {
                                href: o,
                                quality: "ummy",
                                noSize: !0,
                                format: "ummy",
                                videoId: "yt-" + r
                            }, "ummy" === n ? (s.itag = "ummy", s.uQuality = t) : "ummyAudio" === n && (s.itag = "ummyAudio",
                            s.uQuality = "mp3", s.uIsAudio = !0), i.ummyVid && (s.vid = i.ummyVid), a.push(s),
                            delete e[n]);
                        });
                    }(), Object.keys(e).forEach(function(n) {
                        "meta" !== n && (s = {
                            href: e[n],
                            isHidden: !0,
                            title: t,
                            quality: n,
                            itag: n,
                            forceDownload: !0
                        }, a.push(s), delete e[n]);
                    }), n.forEach(function(e) {
                        s = {
                            href: e.url,
                            isHidden: !0,
                            quality: "SRT" + (e.isAuto ? "A" : ""),
                            itemText: o.a.i18n.getMessage("subtitles") + " (" + e.lang + ")",
                            title: t + "-" + e.langCode,
                            ext: "vtt",
                            format: "VTT",
                            isSubtitle: !0,
                            langCode: e.langCode,
                            forceDownload: !0
                        }, "srt2url" === e.preprocess && w.popupMenu.preprocessItem.srt2url(e, s), a.push(s);
                    }), l.extra && l.extra.forEach(function(e) {
                        s = {
                            href: "#" + e.extra,
                            noSize: !0,
                            isHidden: !1
                        }, Object.assign(s, e), e.itag && Object.keys(r.format).some(function(t) {
                            var n = r.format[t][e.itag];
                            if (n) return Object.assign(s, n), !0;
                        }), e.request && (s.func = function(t) {
                            return t.preventDefault(), o.a.sendMessage(e.request);
                        }), s.noAudio = !1, a.push(s);
                    }), a;
                },
                vimeo: function(e, t) {
                    var n, o = [];
                    return e.forEach(function(e) {
                        var i = e.ext;
                        i || (i = "mp4", -1 != e.url.search(/\.flv($|\?)/i) && (i = "flv"));
                        var r = e.height || "", a = e.type;
                        n = {
                            href: e.url,
                            title: t,
                            ext: i,
                            format: a,
                            quality: r,
                            forceDownload: !0
                        }, o.push(n);
                    }), o;
                },
                vk: function(e, t) {
                    var n, o = [];
                    return e.forEach(function(e) {
                        var i = e.name || e.ext;
                        i && (i = i.toLowerCase());
                        var r = i && i.toUpperCase() || "", a = e.subname || "";
                        n = {
                            href: e.url,
                            title: t,
                            ext: i,
                            format: r,
                            quality: a,
                            forceDownload: !0
                        }, o.push(n);
                    }), o;
                },
                dailymotion: function(e, t) {
                    var n = [];
                    return e.forEach(function(e) {
                        var o = null;
                        "ummy" === e.extra ? (o = {
                            href: e.url,
                            quality: "ummy",
                            noSize: !0,
                            format: "ummy",
                            videoId: e.videoId,
                            sort: {
                                size: 480
                            }
                        }, "ummyAudio" === e.type && (o.uQuality = "mp3", o.uIsAudio = !0)) : o = {
                            href: e.url,
                            title: t,
                            ext: e.ext,
                            format: e.ext,
                            quality: e.height || "",
                            forceDownload: !0
                        }, n.push(o);
                    }), n;
                },
                facebook: function(e, t) {
                    var n, o = [];
                    return e.forEach(function(e) {
                        var i = e.ext, r = i ? i.toUpperCase() : "", a = e.name;
                        n = {
                            href: e.url,
                            title: t,
                            ext: i,
                            format: r,
                            quality: a,
                            forceDownload: !0
                        }, o.push(n);
                    }), o;
                },
                rutube: function(e) {
                    if (Array.isArray(e) && (e = e[0]), "string" == typeof e) {
                        var t = [], n = e.match(/\/embed\/(\d+)/);
                        (n = n && n[1] || void 0) || (n = (n = e.match(/\/video\/([0-9a-z]+)/)) && n[1] || void 0),
                        /\/\/video\./.test(e) && (e = e.replace(/\/\/video\./, "//"), n || (n = (n = e.match(/\/(\d+)$/)) && n[1] || void 0)),
                        n && (n = "rt-" + n);
                        var o = e.replace(/^.*(\/\/.*)$/, "ummy:$1"), i = {
                            href: o,
                            quality: "ummy",
                            noSize: !0,
                            format: "ummy",
                            itag: "ummy",
                            uQuality: "720",
                            vid: 114,
                            videoId: n
                        }, r = "?";
                        -1 !== o.indexOf(r) && (r = "&");
                        var a = {
                            href: o += r + "sf_type=audio",
                            quality: "ummy",
                            noSize: !0,
                            format: "ummy",
                            itag: "ummyAudio",
                            uQuality: "mp3",
                            uIsAudio: !0,
                            vid: 114,
                            videoId: n
                        };
                        return t.push(i), t.push(a), t;
                    }
                },
                mailru: function(e, t) {
                    var n, o = [];
                    return e.forEach(function(e) {
                        var i = e.ext, r = e.name, a = e.subname;
                        n = {
                            href: e.url,
                            title: t,
                            ext: i,
                            format: r,
                            quality: a,
                            forceDownload: !0
                        }, o.push(n);
                    }), o;
                }
            },
            quickInsert: function(e, t, n, o) {
                o = o || {};
                var i = {}, r = function t(n) {
                    n && (n.target === e || e.contains(n.target)) || i.isShow && (s.style.display = "none",
                    u.a.off(document, "mousedown", t), i.isShow = !1, o.onHide && o.onHide(s));
                }, a = {
                    links: t,
                    button: e,
                    popupId: n,
                    showFileSize: !0
                };
                Object.assign(a, o);
                var s = w.popupMenu.create(a);
                return o.onShow && o.onShow(s), u.a.off(document, "mousedown", r), u.a.on(document, "mousedown", r),
                Object.assign(i, {
                    button: e,
                    isShow: !0,
                    el: s,
                    hide: r,
                    update: function(e) {
                        a.links = e, w.popupMenu.update(s, a);
                    }
                });
            }
        },
        frameMenu: {
            getBtn: function(e) {
                var t = {
                    verticalAlign: "middle",
                    position: "absolute",
                    zIndex: 999,
                    fontFamily: "arial, sans-serif"
                };
                for (var n in e.containerStyle) t[n] = e.containerStyle[n];
                var o = e.quickBtnStyleObj || {
                    display: "inline-block",
                    fontSize: "inherit",
                    height: "22px",
                    border: "1px solid rgba(255, 255, 255, 0.4)",
                    borderRadius: "3px",
                    borderTopRightRadius: 0,
                    borderBottomRightRadius: 0,
                    paddingRight: 0,
                    paddingLeft: "28px",
                    cursor: "pointer",
                    verticalAlign: "middle",
                    position: "relative",
                    lineHeight: "22px",
                    textDecoration: "none",
                    zIndex: 1,
                    color: "#fff"
                };
                e.singleBtn && !e.quickBtnStyleObj && (delete o.borderTopRightRadius, delete o.borderBottomRightRadius);
                var i = {
                    position: "relative",
                    display: "inline-block",
                    fontSize: "inherit",
                    height: "24px",
                    padding: 0,
                    paddingRight: "21px",
                    border: "1px solid rgba(255, 255, 255, 0.4)",
                    borderLeft: 0,
                    borderRadius: "3px",
                    borderTopLeftRadius: "0",
                    borderBottomLeftRadius: "0",
                    cursor: "pointer",
                    color: "#fff",
                    zIndex: 0,
                    verticalAlign: "middle",
                    marginLeft: 0,
                    boxSizing: "border-box",
                    lineHeight: "22px"
                };
                for (var n in e.selectBtnStyle) i[n] = e.selectBtnStyle[n];
                var r, s = e.quickBtnIcon || p.a.create("i", {
                    style: {
                        position: "absolute",
                        display: "inline-block",
                        left: "6px",
                        top: "3px",
                        backgroundImage: "url(" + w.svg.getSrc("download", "#ffffff") + ")",
                        backgroundSize: "12px",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center",
                        width: "16px",
                        height: "16px"
                    }
                }), l = e.selectBtnIcon || p.a.create("i", {
                    style: {
                        position: "absolute",
                        display: "inline-block",
                        top: "9px",
                        right: "6px",
                        border: "5px solid #FFF",
                        borderBottomColor: "transparent",
                        borderLeftColor: "transparent",
                        borderRightColor: "transparent"
                    }
                }), d = p.a.create("div", {
                    id: e.btnId,
                    style: t,
                    on: e.on,
                    append: [ r = p.a.create("a", {
                        class: "sf-quick-btn",
                        style: o,
                        href: "#",
                        append: [ s ]
                    }), p.a.create("style", {
                        text: Object(a.a)({
                            selector: "#" + e.btnId,
                            style: e.nodeCssStyle || {
                                opacity: .8,
                                display: "none"
                            },
                            append: [ {
                                "button::-moz-focus-inner": {
                                    padding: 0,
                                    margin: 0
                                },
                                ".sf-quick-btn": e.quickBtnCssStyle || {
                                    backgroundColor: "rgba(28,28,28,0.1)"
                                },
                                ".sf-select-btn": {
                                    backgroundColor: "rgba(28,28,28,0.1)"
                                }
                            }, {
                                selector: [ ":hover", ".sf-over" ],
                                join: "",
                                style: {
                                    opacity: 1
                                },
                                append: {
                                    ".sf-quick-btn": e.quickBtnOverCssStyle || {
                                        backgroundColor: "rgba(0, 163, 80, 0.5)"
                                    },
                                    ".sf-select-btn": {
                                        backgroundColor: "rgba(60, 60, 60, 0.5)"
                                    }
                                }
                            }, {
                                join: "",
                                ".sf-over": {
                                    append: {
                                        ".sf-select-btn": {
                                            backgroundColor: "rgba(28,28,28,0.8)"
                                        }
                                    }
                                },
                                ".sf-show": {
                                    display: "block"
                                }
                            } ]
                        })
                    }) ]
                }), u = null, c = null;
                return e.singleBtn || (c = function(e) {
                    var t = "object" == typeof e ? e : document.createTextNode(e), n = u.firstChild;
                    n === l ? u.insertBefore(t, n) : u.replaceChild(t, n);
                }, u = p.a.create("button", {
                    class: "sf-select-btn",
                    style: i,
                    on: e.onSelectBtn,
                    append: [ l ]
                }), d.appendChild(u)), {
                    node: d,
                    setQuality: c,
                    setLoadingState: function() {
                        c(p.a.create("img", {
                            src: w.svg.getSrc("info", "#ffffff"),
                            style: {
                                width: "14px",
                                height: "14px",
                                marginLeft: "6px",
                                verticalAlign: "middle",
                                top: "-1px",
                                position: "relative"
                            }
                        }));
                    },
                    selectBtn: u,
                    quickBtn: r
                };
            },
            getHiddenList: function(e, t) {
                var n = w.popupMenu, i = n.createPopupItem("-text-", t).el;
                p.a.create(i, {
                    text: o.a.i18n.getMessage("more") + " " + String.fromCharCode(187),
                    style: {
                        cursor: "pointer"
                    },
                    on: [ "click", function() {
                        for (var e, t = this.parentNode.querySelectorAll("*[" + n.dataArrtVisible + "]"), o = 0; e = t[o]; o++) e.style.display = "block",
                        e.setAttribute(n.dataArrtVisible, 1);
                        this.parentNode.removeChild(this);
                    } ]
                });
                var r = document.createDocumentFragment();
                return r.appendChild(i), p.a.create(r, {
                    append: e
                }), 0 === t.visibleCount && u.a.trigger(i, "click", {
                    cancelable: !0
                }), r;
            },
            getMenuContainer: function(e) {
                var t = w.popupMenu, n = e.button, o = e.popupId, i = p.a.create("div", {
                    style: {
                        position: "absolute",
                        minHeight: "24px",
                        cursor: "default",
                        textAlign: "left",
                        whiteSpace: "nowrap",
                        overflow: "auto"
                    }
                });
                "#" === o[0] ? i.id = o.substr(1) : i.classList.add(o);
                var r = t.getContent(e);
                i.appendChild(r.content), setTimeout(function() {
                    r.sizeIconList.forEach(function(e) {
                        u.a.trigger(e, "click", {
                            bubbles: !1,
                            cancelable: !0
                        });
                    });
                });
                var s = w.getPosition(n, e.parent), l = w.getSize(n), d = function(e) {
                    e.stopPropagation();
                }, c = s.top + l.height, f = {
                    top: c + "px",
                    maxHeight: document.body.offsetHeight - c - 40 + "px"
                };
                return e.leftMenuPos ? f.left = s.left + "px" : f.right = document.body.offsetWidth - s.left - l.width + "px",
                p.a.create(i, {
                    style: f,
                    on: [ [ "click", d ], [ "mouseover", d ], [ "mouseup", d ], [ "mousedown", d ], [ "mouseout", d ], [ "wheel", function(e) {
                        e.wheelDeltaY > 0 && 0 === this.scrollTop ? e.preventDefault() : e.wheelDeltaY < 0 && this.scrollHeight - (this.offsetHeight + this.scrollTop) <= 0 && e.preventDefault();
                    } ] ],
                    append: [ p.a.create("style", {
                        text: Object(a.a)({
                            selector: ("#" === o[0] ? "" : ".") + o,
                            style: {
                                display: "none",
                                fontFamily: "arial, sans-serif",
                                backgroundColor: "rgba(28,28,28,0.8)",
                                zIndex: 9999,
                                borderRadius: "4px",
                                fontSize: "12px",
                                fontWeight: "bold",
                                minWidth: "190px",
                                color: "#fff"
                            },
                            append: [ {
                                join: "",
                                ".sf-show": {
                                    display: "block"
                                },
                                "::-webkit-scrollbar-track": {
                                    backgroundColor: "#424242"
                                },
                                "::-webkit-scrollbar": {
                                    width: "10px",
                                    backgroundColor: "#424242"
                                },
                                "::-webkit-scrollbar-thumb": {
                                    backgroundColor: "#8e8e8e"
                                }
                            }, {
                                ".sf-menu-item": {
                                    lineHeight: "24px",
                                    color: "#fff"
                                },
                                ".sf-menu-item:hover": {
                                    backgroundColor: "#1c1c1c"
                                }
                            } ]
                        })
                    }) ]
                }), i;
            },
            getMenu: function(e, t, n, o) {
                var i = {
                    links: t,
                    button: e,
                    popupId: n || "#sf-frame-menu",
                    showFileSize: !0,
                    sizeIconStyle: {
                        color: "#fff"
                    },
                    linkClass: "sf-menu-item",
                    bindUmmyInfoDetails: {
                        posLeft: !0,
                        widthLimit: 480,
                        container: o.container,
                        createUmmyInfoDetails: {
                            posLeft: !0,
                            darkTheme: !0
                        }
                    },
                    getHiddenListFunc: this.getHiddenList.bind(this)
                };
                for (var r in o) i[r] = o[r];
                var a = this.getMenuContainer(i);
                (i.container || document.body).appendChild(a);
                var s = function() {
                    a.parentNode && a.parentNode.removeChild(a), l.isShow = !1, i.onHide && i.onHide();
                };
                i.onShow && i.onShow(a), u.a.off(document, "mousedown", s), u.a.on(document, "mousedown", s);
                var l = {
                    isShow: !0,
                    el: a,
                    hide: s,
                    update: function(e) {
                        var t = w.popupMenu, n = a.lastChild;
                        a.textContent = "", i.links = e;
                        var o = t.getContent(i);
                        setTimeout(function() {
                            o.sizeIconList.forEach(function(e) {
                                u.a.trigger(e, "click", {
                                    bubbles: !1,
                                    cancelable: !0
                                });
                            });
                        }), a.appendChild(o.content), a.appendChild(n);
                    }
                };
                return l;
            }
        },
        mobileLightBox: {
            id: "sf-lightbox",
            clear: function() {
                var e = document.getElementById(w.mobileLightBox.id);
                null !== e && e.parentNode.removeChild(e);
            },
            getTitle: function(e) {
                var t = [];
                if (t.push(e.format || "???"), e.quality) {
                    var n = e.quality;
                    e.sFps && (n += " " + (e.fps || 60)), t.push(n);
                }
                return e["3d"] && t.push("3D"), e.noAudio && t.push(o.a.i18n.getMessage("withoutAudio")),
                t.join(" ");
            },
            createItem: function(e) {
                var t = w.mobileLightBox, n = p.a.create("a", {
                    style: {
                        display: "block",
                        marginBottom: "6px",
                        border: "solid 1px #d3d3d3",
                        lineHeight: "36px",
                        minHeight: "36px",
                        background: "#f8f8f8",
                        verticalAlign: "middle",
                        fontSize: "15px",
                        textAlign: "center",
                        color: "#333",
                        borderRadius: "2px",
                        overflow: "hidden",
                        position: "relative"
                    }
                }), o = "";
                if (e.title) {
                    var i = (e.ext || e.format || "").toLowerCase();
                    i && (i = "." + i), o = h.a.modify(e.title + i);
                }
                if ("string" == typeof e) return n.textContent = e, n;
                n.href = e.href, n.download = o, n.textContent = t.getTitle(e), e.forceDownload && n.addEventListener("click", function(e) {
                    w.downloadOnClick(e, null, {
                        el: this
                    });
                }), e.isHidden && (n.classList.add("isOptional"), n.style.display = "none");
                var r = w.getFileSizeIcon({
                    cssFloat: "right",
                    lineHeight: "36px",
                    fontSize: "75%",
                    marginRight: "10px"
                }, {
                    padding: "10px",
                    verticalAlign: "middle",
                    lineHeight: 0
                }, {
                    width: "16px",
                    height: "16px"
                }, {
                    url: e.href
                });
                return n.appendChild(r.node), n;
            },
            getItems: function(e) {
                var t = w.mobileLightBox;
                if ("string" == typeof e) return {
                    list: [ t.createItem(e) ],
                    hiddenCount: 0
                };
                for (var n, o = [], i = 0; n = e[i]; i++) "ummy" !== n.quality && (n.extra || o.push({
                    el: t.createItem(n),
                    prop: n
                }));
                o = w.popupMenu.sortMenuItems(o);
                var r = [], a = [];
                for (i = 0; n = o[i]; i++) n.prop.isHidden ? a.push(n.el) : r.push(n.el);
                return {
                    list: r.concat(a),
                    hiddenCount: a.length
                };
            },
            show: function(e) {
                var t, n = w.mobileLightBox, i = window.pageYOffset, r = window.innerHeight, a = parseInt(r / 100 * 15), s = void 0, l = function(e) {
                    return r - 46 * (e ? 2 : 1) - 2 * a;
                }, d = function(e) {
                    e.hiddenCount > 0 ? (s.style.height = l(1) + "px", t.style.display = "block") : (t.style.display = "none",
                    s.style.height = l(0) + "px"), e.hiddenCount === e.list.length && u(t);
                }, u = function(e) {
                    var t = "none", n = e.parentNode.querySelectorAll(".isOptional");
                    "open" !== e.dataset.state ? (e.dataset.state = "open", e.textContent = o.a.i18n.getMessage("more") + " " + String.fromCharCode(171),
                    t = "block") : (e.dataset.state = "close", e.textContent = o.a.i18n.getMessage("more") + " " + String.fromCharCode(187));
                    for (var i, r = 0; i = n[r]; r++) i.style.display = t;
                }, c = document.getElementById(n.id);
                null !== c && c.parentNode.removeChild(c);
                var f = window.innerWidth;
                f = f <= 250 ? "90%" : "70%", e && 0 !== e.length || (e = o.a.i18n.getMessage("noLinksFound"));
                var h = n.getItems(e), m = p.a.create("div", {
                    id: n.id,
                    style: {
                        position: "absolute",
                        top: 0,
                        left: 0,
                        width: "100%",
                        zIndex: 9e3,
                        height: document.body.scrollHeight + "px",
                        background: "rgba(0,0,0,0.85)",
                        textAlign: "center",
                        boxSizing: "content-box"
                    },
                    on: [ [ "click", function(e) {
                        e.preventDefault(), y();
                    } ] ],
                    append: p.a.create("div", {
                        style: {
                            display: "inline-block",
                            width: f,
                            backgroundColor: "#eee",
                            height: r - 2 * a + "px",
                            marginTop: a + i + "px",
                            borderRadius: "4px",
                            padding: "8px",
                            position: "relative",
                            boxSizing: "content-box"
                        },
                        append: [ s = p.a.create("div", {
                            style: {
                                overflowY: "auto",
                                marginBottom: "6px"
                            },
                            append: h.list,
                            on: [ "touchmove", function(e) {
                                e.stopPropagation();
                            } ]
                        }), t = p.a.create(n.createItem(o.a.i18n.getMessage("more") + " " + String.fromCharCode(187)), {
                            href: "#",
                            on: [ "click", function(e) {
                                e.preventDefault(), u(this);
                            } ]
                        }), p.a.create(n.createItem(o.a.i18n.getMessage("close")), {
                            style: {
                                marginBottom: 0
                            },
                            on: [ "click", function(e) {
                                e.preventDefault(), y();
                            } ]
                        }) ],
                        on: [ "click", function(e) {
                            e.stopPropagation();
                        } ]
                    })
                });
                d(h), document.body.appendChild(m);
                var g = document.body.scrollTop, v = {}, y = function() {
                    v.isShow && (document.body.scrollTop = g, v.hide());
                };
                return Object.assign(v, {
                    isShow: !0,
                    el: m,
                    hide: function() {
                        m.parentNode && m.parentNode.removeChild(m), v.isShow = !1;
                    },
                    close: y,
                    update: function(e) {
                        if (null !== m.parentNode) {
                            e && 0 !== e.length || (e = o.a.i18n.getMessage("noLinksFound")), s.textContent = "";
                            var t = n.getItems(e);
                            p.a.create(s, {
                                append: t.list
                            }), d(t);
                        }
                    }
                });
            }
        },
        bridge: function(e) {
            e.args = e.args || [], void 0 === e.timeout && (e.timeout = 300);
            var t = "sf-bridge-" + parseInt(1e3 * Math.random()) + "-" + Date.now();
            window.addEventListener("sf-bridge-" + t, function n(o) {
                var i;
                window.removeEventListener("sf-bridge-" + t, n), i = o.detail ? JSON.parse(o.detail) : void 0,
                e.cb(i);
            });
            var n = '(function(a,b,c,d){/* fix */var e=document.getElementById(c);e&&e.parentNode.removeChild(e);var f=!1,g=function(a){if(!f){f=!0;var b=new CustomEvent("sf-bridge-"+c,{detail:JSON.stringify(a)});window.dispatchEvent(b)}};d&&setTimeout(function(){g()},d),b.push(g),a.apply(null,b)})(' + [ e.func.toString(), JSON.stringify(e.args), JSON.stringify(t), parseInt(e.timeout) ].join(",") + ");";
            if (o.a.isSafari) {
                n = n.replace("/* fix */", "(" + function() {
                    "undefined" == typeof CustomEvent && (CustomEvent = function(e, t) {
                        t = t || {
                            bubbles: !1,
                            cancelable: !1
                        };
                        var n = document.createEvent("CustomEvent");
                        return n.initCustomEvent(e, t.bubbles, t.cancelable, t.detail), n;
                    }, CustomEvent.prototype = window.Event.prototype);
                }.toString() + ")();");
            }
            var i = p.a.create("script", {
                id: t,
                text: n
            });
            document.body.appendChild(i);
        },
        TutorialTooltip: function(e) {
            var t = this;
            this.details = {
                btnTopOffset: -3,
                btnLeftOffset: 0
            }, Object.assign(this.details, e), this.onResize = this.onResize.bind(this), this.onResizeDebouce = r(this.onResize, 250),
            this.onClose = this.onClose.bind(this), this.target = e.target, "1" !== this.target.dataset.sfHasTooltip && (this.target.dataset.sfHasTooltip = "1",
            this.tooltipNode = this.getNode(), this.target.addEventListener("mouseup", this.onClose),
            this.target.addEventListener(u.a.onRemoveEventName, function() {
                t.onClose && t.onClose(1);
            }), window.addEventListener("resize", this.onResizeDebouce), this.onResize(), (e.parent || document.body).appendChild(this.tooltipNode));
        }
    };
    w.TutorialTooltip.prototype.getNode = function() {
        var e = this, t = function() {
            var e = 1e3, t = document.getElementById("masthead-positioner"), n = t && window.getComputedStyle(t, null);
            return n && (e = parseInt(n.getPropertyValue("z-index")) + 1), e;
        }();
        return p.a.create("div", {
            class: "sf-tooltip",
            on: [ "mouseup", function(e) {
                e.stopPropagation();
            } ],
            append: [ p.a.create("span", {
                style: {
                    display: "inline-block",
                    border: "8px solid transparent",
                    borderRight: "10px solid #4D4D4D",
                    borderLeft: 0,
                    width: 0,
                    top: "8px",
                    left: "0px",
                    position: "absolute"
                }
            }), p.a.create("span", {
                style: {
                    display: "inline-block",
                    backgroundColor: "#4D4D4D",
                    marginLeft: "10px",
                    padding: "10px 10px",
                    maxWidth: "220px",
                    minWidth: "220px",
                    lineHeight: "16px",
                    fontSize: "14px",
                    fontFamily: "font-family: arial, sans-serif",
                    color: "#fff"
                },
                append: [ p.a.create("p", {
                    style: {
                        margin: 0
                    },
                    append: Object(f.a)(o.a.i18n.getMessage("tutorialTooltipText"))
                }), p.a.create("a", {
                    class: "sf-button",
                    text: "OK",
                    style: {
                        display: "inline-block",
                        textAlign: "center",
                        textDecoration: "none",
                        padding: "0 10px",
                        cssFloat: "right",
                        marginTop: "5px",
                        lineHeight: "20px",
                        borderRadius: "3px",
                        fontSize: "12px",
                        color: "#fff",
                        fontWeight: "bolder",
                        backgroundColor: "#167AC6",
                        cursor: "pointer"
                    },
                    on: [ "click", function(t) {
                        t.preventDefault(), e.onClose && e.onClose();
                    } ]
                }), p.a.create("style", {
                    text: Object(a.a)({
                        ".sf-tooltip": {
                            position: "absolute",
                            zIndex: t + 2,
                            append: {
                                ".sf-button:hover": {
                                    backgroundColor: "#126db3 !important"
                                },
                                ".sf-button:active": {
                                    opacity: .9
                                }
                            }
                        }
                    })
                }) ]
            }) ]
        });
    }, w.TutorialTooltip.prototype.onClose = function(e) {
        e && "mouseup" === e.type && (e = null), this.tooltipNode && (this.tooltipNode.parentNode && this.tooltipNode.parentNode.removeChild(this.tooltipNode),
        this.tooltipNode = null), window.removeEventListener("resize", this.onResizeDebouce),
        this.target.removeEventListener("mouseup", this.onClose), this.onClose = null, e || this.details.onClose && this.details.onClose();
    }, w.TutorialTooltip.prototype.onResize = function() {
        var e = this.target;
        if (!e.offsetParent || !e.parentNode) return this.onClose && this.onClose(1);
        var t = w.getPosition(e, this.details.parent), n = t.top + this.details.btnTopOffset, o = t.left + t.width + this.details.btnLeftOffset;
        this.tooltipNode.style.top = n + "px", this.tooltipNode.style.left = o + "px";
    }, w.mutationWatcher = {
        getMutationObserver: function() {
            var e = null;
            return "undefined" != typeof MutationObserver ? e = MutationObserver : "undefined" != typeof WebKitMutationObserver ? e = WebKitMutationObserver : "undefined" != typeof MozMutationObserver && (e = MozMutationObserver),
            e;
        },
        isAvailable: function() {
            return !!this.getMutationObserver();
        },
        disconnect: function(e) {
            e.observer.disconnect();
        },
        connect: function(e) {
            e.observer.observe(e.target, e.config);
        },
        joinMutations: function(e) {
            for (var t, n, o, i, r, a, s = [], l = [], d = {}; o = e.shift(); ) {
                for (-1 === (a = l.indexOf(o.target)) && (d[a = l.push(o.target) - 1] = {
                    target: o.target,
                    added: [],
                    removed: []
                }), t = d[a], n = void 0, i = 0; r = o.addedNodes[i]; i++) 1 === r.nodeType && (t.added.push(r),
                n = !0);
                for (i = 0; r = o.removedNodes[i]; i++) 1 === r.nodeType && (t.removed.push(r),
                n = !0);
                void 0 !== n && void 0 === t.inList && (t.inList = !0, s.push(t));
            }
            return s;
        },
        isMatched: null,
        prepareMatched: function() {
            if (!this.isMatched) {
                var e = document.createElement("div");
                "function" == typeof e.matches ? this.isMatched = function(e, t) {
                    return e.matches(t);
                } : "function" == typeof e.matchesSelector ? this.isMatched = function(e, t) {
                    return e.matchesSelector(t);
                } : "function" == typeof e.webkitMatchesSelector ? this.isMatched = function(e, t) {
                    return e.webkitMatchesSelector(t);
                } : "function" == typeof e.mozMatchesSelector ? this.isMatched = function(e, t) {
                    return e.mozMatchesSelector(t);
                } : "function" == typeof e.oMatchesSelector ? this.isMatched = function(e, t) {
                    return e.oMatchesSelector(t);
                } : "function" == typeof e.msMatchesSelector && (this.isMatched = function(e, t) {
                    return e.msMatchesSelector(t);
                }), e = null;
            }
        },
        match: function(e, t, n) {
            var o, i, r, a, s = this, l = e.queries, d = !1;
            return [ "added", "removed" ].forEach(function(e) {
                var u = n[e];
                for (a = 0; o = u[a]; a++) for (i = 0; r = l[i]; i++) if (void 0 === r.is || r.is === e) {
                    var c = t[i][e];
                    !0 === s.isMatched(o, r.css) ? c.push(o) : c.push.apply(c, o.querySelectorAll(r.css)),
                    !1 === d && (d = void 0 !== c[0]);
                }
            }), d;
        },
        filterTarget: function(e, t) {
            var n, o;
            for (n = 0; o = e[n]; n++) if (!0 === this.isMatched(t, o.css)) return !0;
            return !1;
        },
        run: function(e) {
            var t = this, n = {
                config: {
                    childList: !0,
                    subtree: !0
                },
                target: document.body,
                filterTarget: []
            };
            Object.assign(n, e), n._disconnect = this.disconnect.bind(this, n), n._connect = this.connect.bind(this, n),
            n._match = this.match.bind(this, n);
            for (var o = [], i = 0; i < n.queries.length; i++) o.push({
                added: [],
                removed: []
            });
            o = JSON.stringify(o), this.prepareMatched();
            var r = this.getMutationObserver();
            return n.observer = new r(function(e) {
                var i = t.joinMutations(e);
                if (0 !== i.length) {
                    for (var r, a = !1, s = JSON.parse(o); r = i.shift(); ) !1 === t.filterTarget(n.filterTarget, r.target) && !0 === n._match(s, r) && (a = !0);
                    !0 === a && n.callback(s);
                }
            }), n.trigger = function(e) {
                var t = !1, i = JSON.parse(o), r = {
                    added: [ e ],
                    removed: []
                };
                n._match(i, r) && (t = !0), !0 === t && n.callback(i);
            }, n.start = function() {
                n._disconnect(), n._connect(), n.trigger(n.target);
            }, n.stop = function() {
                n._disconnect();
            }, n.start(), n;
        }
    }, w.mutationAttrWatcher = {
        isAvailable: function() {
            return !!w.mutationWatcher.getMutationObserver();
        },
        disconnect: function(e) {
            e.observer.disconnect();
        },
        connect: function(e) {
            e.observer.observe(e.target, e.config);
        },
        run: function(e) {
            var t = {
                config: {
                    attributes: !0,
                    childList: !1,
                    attributeOldValue: !0
                },
                target: document.body
            };
            Object.assign(t, e), Array.isArray(t.attr) || (t.attr = [ t.attr ]), t.config.attributeFilter = t.attr,
            t._disconnect = this.disconnect.bind(this, t), t._connect = this.connect.bind(this, t);
            for (var n = [], o = 0; o < t.attr.length; o++) n.push({});
            n = JSON.stringify(n);
            var i = w.mutationWatcher.getMutationObserver();
            return t.observer = new i(function(e) {
                for (var o, i = !1, r = JSON.parse(n); o = e.shift(); ) {
                    var a = t.attr.indexOf(o.attributeName);
                    if (-1 !== a) {
                        var s = o.target.getAttribute(o.attributeName);
                        s !== o.oldValue && (r[a] = {
                            value: s,
                            oldValue: o.oldValue
                        }, i = !0);
                    }
                }
                !0 === i && t.callback(r);
            }), t.start = function() {
                t._disconnect(), t._connect();
                for (var e, o = !1, i = JSON.parse(n), r = 0; e = t.attr[r]; r++) {
                    var a = t.target.getAttribute(e);
                    null !== a && (i[r] = {
                        value: a,
                        oldValue: null
                    }, o = !0);
                }
                !0 === o && t.callback(i);
            }, t.stop = function() {
                t._disconnect();
            }, setTimeout(function() {
                t.start();
            }), t;
        }
    }, w.waitNodesBySelector = function(e) {
        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, n = !1, o = null, i = Promise.resolve().then(() => {
            var i = t.target || document.body;
            w.mutationWatcher.prepareMatched();
            var r = w.mutationWatcher.isMatched, a = w.mutationWatcher.getMutationObserver(), s = null, l = null, d = new Promise((e, t) => {
                s = e, l = t;
            }), u = null;
            t.timeout > 0 && (u = setTimeout(() => {
                o && o();
            }, t.timeout));
            var c = [], p = new a(t => {
                var n, o;
                for (n = 0; n < t.length; n++) {
                    var i = t[n];
                    for (o = 0; o < i.addedNodes.length; o++) {
                        var a = i.addedNodes[o];
                        1 === a.nodeType && (r(a, e) ? c.push(a) : c.push.apply(c, a.querySelectorAll(e)));
                    }
                }
                c.length && s(c);
            });
            return p.observe(i, {
                childList: !0,
                subtree: !0
            }), o = (() => {
                o = null, l(new Error("ABORTED"));
            }), c.push.apply(c, i.querySelectorAll(e)), c.length && s(c), n && o && o(), d.then(e => (p.disconnect(),
            clearTimeout(u), e), e => {
                throw p.disconnect(), clearTimeout(u), e;
            });
        });
        return i.abort = (() => {
            n = !0, o && o();
        }), i;
    };
    t.a = (e => (x = e, w));
}, function(e, t, n) {
    "use strict";
    t.a = function(e, t) {
        Array.isArray(t) || (t = [ t ]);
        for (var n = e; n; n = n.parentNode) {
            if (1 !== n.nodeType) return null;
            for (var o, i = 0; o = t[i]; i++) if (n.classList.contains(o)) return n;
        }
        return null;
    };
}, function(e, t, n) {
    "use strict";
    t.a = function(e, t) {
        var n = null;
        n = !(t = t || {}).params && /\?/.test(e) ? e.match(/[^?]*\?(.*)/)[1] : e;
        for (var o = t.sep || "&", i = n.split(o), r = {}, a = 0, s = i.length; a < s; a++) {
            var l = i[a].split("="), d = l[0], u = l[1] || "";
            if (t.noDecode) r[d] = u; else {
                try {
                    d = decodeURIComponent(d);
                } catch (e) {
                    d = unescape(d);
                }
                try {
                    r[d] = decodeURIComponent(u);
                } catch (e) {
                    r[d] = unescape(u);
                }
            }
        }
        return r;
    };
}, function(e, t, n) {
    "use strict";
    t.a = (() => window.top !== window.self);
}, function(e, t, n) {
    "use strict";
    var o = n(0);
    t.a = function(e) {
        return new Promise(function(t) {
            o.a.sendMessage(e, t);
        });
    };
}, function(e, t, n) {
    "use strict";
    var o = n(9), i = Object(o.a)("webRequest"), r = function() {
        var e = /^sf-\d+_/, t = {
            urls: [ "<all_urls>" ],
            types: [ "xmlhttprequest" ]
        }, n = !1, o = {}, r = {}, a = function(e) {
            for (var t in e) return !1;
            return !0;
        }, s = function(e) {
            delete r[e.requestId], a(o) && a(r) && u();
        }, l = function(t) {
            var n = r[t.requestId], i = t.requestHeaders || [], s = [], l = [], d = [];
            if (n) l = n.changes, s = n.filtered; else if (!a(o)) for (var u, c, p, f = 0; p = i[f]; f++) u = p.name,
            e.test(u) && (c = o[u]) && (p.name = c.name, p.value = c.value, l.push(p), s.push(c.name.toLowerCase()),
            s.push(u.toLowerCase()), /cookie/i.test(p.name) && d.push("set-cookie"), clearTimeout(c.timer),
            delete o[u]);
            if (l.length) return n || (r[t.requestId] = {
                changes: l,
                filtered: s,
                filterResponseHeaders: d
            }), {
                requestHeaders: i.filter(function(e) {
                    return -1 === s.indexOf(e.name.toLowerCase());
                }).concat(l)
            };
        }, d = function(e) {
            var t = r[e.requestId], n = e.responseHeaders;
            if (t && n) {
                var o = t.filterResponseHeaders;
                return {
                    responseHeaders: n.filter(function(e) {
                        return -1 === o.indexOf(e.name.toLowerCase());
                    })
                };
            }
        }, u = function() {
            n && (n = !1, chrome.webRequest.onBeforeSendHeaders.removeListener(l, t, [ "blocking", "requestHeaders" ]),
            chrome.webRequest.onHeadersReceived.removeListener(d, t, [ "blocking", "responseHeaders" ]),
            chrome.webRequest.onResponseStarted.removeListener(s, t), chrome.webRequest.onErrorOccurred.removeListener(s, t),
            i.debug("webRequest", "rm listener"));
        }, c = 10, p = !1, f = null, h = function(e) {
            return (null === f || e) && (f = !!(chrome.webRequest && chrome.webRequest.onBeforeSendHeaders && chrome.webRequest.onResponseStarted && chrome.webRequest.onErrorOccurred)),
            f;
        }, m = /^user-agent$|^origin$|^cookie$/i;
        return {
            wrapHeaderKey: function(e, r) {
                if (h()) {
                    for (var a, u = 100; u-- > 0 && (a = "sf-" + parseInt(1e5 * Math.random()) + "_" + e,
                    o[a]); ) ;
                    return o[a] = {
                        name: e,
                        value: r,
                        timer: setTimeout(function() {
                            delete o[a];
                        }, 3e3)
                    }, n || (n = !0, chrome.webRequest.onBeforeSendHeaders.addListener(l, t, [ "blocking", "requestHeaders" ]),
                    chrome.webRequest.onHeadersReceived.addListener(d, t, [ "blocking", "responseHeaders" ]),
                    chrome.webRequest.onResponseStarted.addListener(s, t), chrome.webRequest.onErrorOccurred.addListener(s, t),
                    i.debug("webRequest", "add listener")), a;
                }
                return e;
            },
            isSpecialHeader: function(e) {
                return m.test(e);
            },
            requestPermission: function(e) {
                h() || p ? e(f) : chrome.permissions && chrome.permissions.request ? chrome.permissions.request({
                    permissions: [ "webRequest", "webRequestBlocking" ]
                }, function(t) {
                    (t || c-- <= 0) && (p = !0), t && h(!0), e(f);
                }) : (p = !0, e(f));
            }
        };
    }(), a = n(20), s = function(e) {
        var t = {};
        return (e = e.split(/\r?\n/)).forEach(function(e) {
            var n = e.indexOf(":");
            if (-1 !== n) {
                var o = e.substr(0, n).trim().toLowerCase(), i = e.substr(n + 1).trim();
                t[o] = i;
            }
        }), t;
    };
    t.a = function(e, t) {
        var n = {}, o = function(e, n) {
            o = null, p.timeoutTimer && clearTimeout(p.timeoutTimer);
            var r = null;
            e && (r = String(e.message || e) || "ERROR"), t && t(r, i(n), n);
        }, i = function(e) {
            var t = {};
            t.statusCode = h.status, t.statusText = h.statusText;
            var n = null, o = h.getAllResponseHeaders();
            return "string" == typeof o && (n = s(o)), t.headers = n || {}, t.body = e, t;
        };
        "object" != typeof e && (e = {
            url: e
        });
        var l = e.url, d = e.method || e.type || "GET";
        d = d.toUpperCase();
        var u = e.data;
        if ("string" != typeof u && (u = a.stringify(u)), u && "GET" === d && (l += (/\?/.test(l) ? "&" : "?") + u,
        u = void 0), !1 === e.cache && -1 !== [ "GET", "HEAD" ].indexOf(d) && (l += (/\?/.test(l) ? "&" : "?") + "_=" + Date.now()),
        !/^https?:\/\//.test(l)) {
            var c = document.createElement("a");
            c.href = l, l = c.href, c = null;
        }
        e.headers = e.headers || {}, u && (e.headers["Content-Type"] = e.contentType || e.headers["Content-Type"] || "application/x-www-form-urlencoded; charset=UTF-8");
        var p = {};
        p.url = l, p.method = d, u && (p.data = u), e.json && (p.json = !0), e.xml && (p.xml = !0),
        e.timeout && (p.timeout = e.timeout), e.mimeType && (p.mimeType = e.mimeType), e.withCredentials && (p.withCredentials = !0),
        Object.keys(e.headers).length && (p.headers = e.headers), p.timeout > 0 && (p.timeoutTimer = setTimeout(function() {
            o && o(new Error("ETIMEDOUT")), h.abort();
        }, p.timeout));
        var f = {
            0: 200,
            1223: 204
        }, h = (e.localXHR, new XMLHttpRequest());
        h.open(p.method, p.url, !0), p.mimeType && h.overrideMimeType(p.mimeType), p.withCredentials && (h.withCredentials = !0);
        var m = [];
        for (var g in p.headers) r && r.isSpecialHeader(g) && m.push({
            key: g,
            value: p.headers[g]
        }), h.setRequestHeader(g, p.headers[g]);
        h.onload = function() {
            var e = f[h.status] || h.status;
            try {
                if (e >= 200 && e < 300 || 304 === e) {
                    var t = h.responseText;
                    if (p.json) t = JSON.parse(t); else if (p.xml) t = new DOMParser().parseFromString(t, "text/xml"); else if ("string" != typeof t) throw console.error("Response is not string!", t),
                    new Error("Response is not string!");
                    return o && o(null, t);
                }
                throw new Error(h.status + " " + h.statusText);
            } catch (e) {
                return o && o(e);
            }
        };
        var v = h.onerror = function() {
            o && o(new Error(h.status + " " + h.statusText));
        }, y = null;
        void 0 !== h.onabort ? h.onabort = v : y = function() {
            4 === h.readyState && o && setTimeout(function() {
                return v();
            });
        }, y && (h.onreadystatechange = y);
        var b = function() {
            try {
                h.send(p.data || null);
            } catch (e) {
                setTimeout(function() {
                    o && o(e);
                });
            }
        };
        r && m.length ? r.requestPermission(function(e) {
            e && function() {
                for (var e, t = 0; e = m[t]; t++) h.setRequestHeader(r.wrapHeaderKey(e.key, e.value), e.value);
            }(), o && b();
        }) : b();
        return n.abort = function() {
            o = null, h.abort();
        }, n;
    };
}, function(e, t, n) {
    "use strict";
    var o = n(16);
    t.a = (e => new Promise((t, n) => {
        Object(o.a)(e, (e, o) => {
            e ? n(e) : t(o);
        });
    }));
}, function(e, t, n) {
    "use strict";
    t.a = function(e, t) {
        t && !Array.isArray(t) && (t = [ t ]);
        for (var n, o = [], i = {
            "{": 0,
            "[": 0
        }, r = {
            "}": "{",
            "]": "["
        }, a = /[{}\]\[":0-9.,-]/, s = /[\r\n\s\t]/, l = "", d = 0; n = e[d]; d++) if ('"' !== n) a.test(n) ? (l += n,
        "{" === n || "[" === n ? (i["{"] || i["["] || (l = n), i[n]++) : "}" !== n && "]" !== n || (i[r[n]]--,
        i["{"] || i["["] || o.push(l))) : "t" === n && "true" === e.substr(d, 4) ? (l += "true",
        d += 3) : "f" === n && "false" === e.substr(d, 5) ? (l += "false", d += 4) : "n" === n && "null" === e.substr(d, 4) ? (l += "null",
        d += 3) : s.test(n) || (i["{"] = 0, i["["] = 0, l = ""); else {
            for (var u = d; -1 !== u && (u === d || "\\" === e[u - 1]); ) u = e.indexOf('"', u + 1);
            -1 === u && (u = e.length - 1), l += e.substr(d, u - d + 1), d = u;
        }
        for (var c, p = [], f = function(e, n) {
            if ("{}" === e || "[]" === e) return "continue";
            try {
                t ? t.every(function(t) {
                    return t.test(e);
                }) && p.push(JSON.parse(e)) : p.push(JSON.parse(e));
            } catch (e) {}
        }, h = 0; c = o[h]; h++) f(c);
        return p;
    };
}, function(e, t, n) {
    "use strict";
    var o = n(1);
    t.a = function e(t, n) {
        if (n = n || {}, "string" == typeof t) {
            if ("[" !== t[0]) return document.createTextNode(t);
            try {
                t = t.replace(/"/g, "\\u0022").replace(/\\'/g, "\\u0027").replace(/'/g, '"').replace(/([{,])\s*([a-zA-Z0-9]+):/g, '$1"$2":'),
                t = JSON.parse(t);
            } catch (e) {
                return document.createTextNode(t);
            }
        }
        if (!Array.isArray(t)) return document.createTextNode(t);
        for (var i = n.fragment || document.createDocumentFragment(), r = 0, a = t.length; r < a; r++) {
            var s = t[r];
            if ("object" == typeof s) for (var l in s) {
                var d, u = s[l], c = u.append;
                delete u.append, i.appendChild(d = o.a.create(l, u)), void 0 !== c && e(c, {
                    fragment: d
                });
            } else i.appendChild(document.createTextNode(s));
        }
        return i;
    };
}, function(e, t, n) {
    "use strict";
    t.decode = t.parse = n(28), t.encode = t.stringify = n(29);
}, function(e, t, n) {
    "use strict";
    var o = function(e) {
        e = e.replace(/\r\n/g, "\n");
        for (var t = "", n = 0; n < e.length; n++) {
            var o = e.charCodeAt(n);
            o < 128 ? t += String.fromCharCode(o) : o > 127 && o < 2048 ? (t += String.fromCharCode(o >> 6 | 192),
            t += String.fromCharCode(63 & o | 128)) : (t += String.fromCharCode(o >> 12 | 224),
            t += String.fromCharCode(o >> 6 & 63 | 128), t += String.fromCharCode(63 & o | 128));
        }
        return t;
    };
    t.a = function(e, t, n) {
        var i = "";
        if (n || "undefined" == typeof URL || "undefined" == typeof Blob) {
            var r = o(e);
            i = "data:" + t + ";charset=utf8;base64," + encodeURIComponent(btoa(r));
        } else {
            var a = new Blob([ e ], {
                encoding: "UTF-8",
                type: t
            });
            i = URL.createObjectURL(a);
        }
        return i;
    };
}, function(e, t, n) {
    "use strict";
    var o = /\\(\\u[0-9a-f]{4})/g;
    t.a = function(e) {
        try {
            return JSON.parse(JSON.stringify(e).replace(o, "$1"));
        } catch (t) {
            return e;
        }
    };
}, function(e, t, n) {
    "use strict";
    t.a = function(e, t) {
        t && !Array.isArray(t) && (t = [ t ]);
        var n = [];
        return e.replace(/<script(?:\s*|\s[^>]+[^\/])>/g, function(o, i) {
            i += o.length;
            var r = e.indexOf("<\/script>", i);
            if (-1 !== r) {
                var a = e.substr(i, r - i);
                t ? t.every(function(e) {
                    return e.test(a);
                }) && n.push(a) : n.push(a);
            }
        }), n;
    };
}, function(e, t, n) {
    "use strict";
    var o = /^[^{]+\{\s*\[native \w/, i = function(e, t) {
        return (i = o.test(document.compareDocumentPosition) || o.test(document.contains) ? function(e, t) {
            var n = 9 === e.nodeType ? e.documentElement : e, o = t && t.parentNode;
            return e === o || !(!o || 1 !== o.nodeType || !(n.contains ? n.contains(o) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(o)));
        } : function(e, t) {
            if (t) for (;t = t.parentNode; ) if (t === e) return !0;
            return !1;
        }).apply(this, arguments);
    };
    t.a = ((e, t) => i(e, t));
}, function(e, t, n) {
    var o = n(26).default;
    e.exports = o;
}, function(e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
        return typeof e;
    } : function(e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol ? "symbol" : typeof e;
    };
    function i(e) {
        return e && "object" === (void 0 === e ? "undefined" : o(e)) && "string" == typeof e.name && "string" == typeof e.message;
    }
    t.default = function(e) {
        return i(e) ? Object.assign(new Error(), {
            stack: void 0
        }, e) : e;
    }, t.isSerializedError = i;
}, , function(e, t, n) {
    "use strict";
    function o(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t);
    }
    e.exports = function(e, t, n, r) {
        t = t || "&", n = n || "=";
        var a = {};
        if ("string" != typeof e || 0 === e.length) return a;
        var s = /\+/g;
        e = e.split(t);
        var l = 1e3;
        r && "number" == typeof r.maxKeys && (l = r.maxKeys);
        var d = e.length;
        l > 0 && d > l && (d = l);
        for (var u = 0; u < d; ++u) {
            var c, p, f, h, m = e[u].replace(s, "%20"), g = m.indexOf(n);
            g >= 0 ? (c = m.substr(0, g), p = m.substr(g + 1)) : (c = m, p = ""), f = decodeURIComponent(c),
            h = decodeURIComponent(p), o(a, f) ? i(a[f]) ? a[f].push(h) : a[f] = [ a[f], h ] : a[f] = h;
        }
        return a;
    };
    var i = Array.isArray || function(e) {
        return "[object Array]" === Object.prototype.toString.call(e);
    };
}, function(e, t, n) {
    "use strict";
    var o = function(e) {
        switch (typeof e) {
          case "string":
            return e;

          case "boolean":
            return e ? "true" : "false";

          case "number":
            return isFinite(e) ? e : "";

          default:
            return "";
        }
    };
    e.exports = function(e, t, n, s) {
        return t = t || "&", n = n || "=", null === e && (e = void 0), "object" == typeof e ? r(a(e), function(a) {
            var s = encodeURIComponent(o(a)) + n;
            return i(e[a]) ? r(e[a], function(e) {
                return s + encodeURIComponent(o(e));
            }).join(t) : s + encodeURIComponent(o(e[a]));
        }).join(t) : s ? encodeURIComponent(o(s)) + n + encodeURIComponent(o(e)) : "";
    };
    var i = Array.isArray || function(e) {
        return "[object Array]" === Object.prototype.toString.call(e);
    };
    function r(e, t) {
        if (e.map) return e.map(t);
        for (var n = [], o = 0; o < e.length; o++) n.push(t(e[o], o));
        return n;
    }
    var a = Object.keys || function(e) {
        var t = [];
        for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && t.push(n);
        return t;
    };
} ] ]);