!function(e) {
    var r = {};
    function t(s) {
        if (r[s]) return r[s].exports;
        var n = r[s] = {
            i: s,
            l: !1,
            exports: {}
        };
        return e[s].call(n.exports, n, n.exports, t), n.l = !0, n.exports;
    }
    t.m = e, t.c = r, t.d = function(e, r, s) {
        t.o(e, r) || Object.defineProperty(e, r, {
            enumerable: !0,
            get: s
        });
    }, t.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(e, "__esModule", {
            value: !0
        });
    }, t.t = function(e, r) {
        if (1 & r && (e = t(e)), 8 & r) return e;
        if (4 & r && "object" == typeof e && e && e.__esModule) return e;
        var s = Object.create(null);
        if (t.r(s), Object.defineProperty(s, "default", {
            enumerable: !0,
            value: e
        }), 2 & r && "string" != typeof e) for (var n in e) t.d(s, n, function(r) {
            return e[r];
        }.bind(null, n));
        return s;
    }, t.n = function(e) {
        var r = e && e.__esModule ? function() {
            return e.default;
        } : function() {
            return e;
        };
        return t.d(r, "a", r), r;
    }, t.o = function(e, r) {
        return Object.prototype.hasOwnProperty.call(e, r);
    }, t.p = "", t(t.s = 39);
}({
    0: function(e, r, t) {
        "use strict";
        var s = class {
            constructor() {
                this.listeners = [];
            }
            addListener(e) {
                -1 === this.listeners.indexOf(e) && this.listeners.push(e);
            }
            dispatch() {
                for (var e = arguments.length, r = new Array(e), t = 0; t < e; t++) r[t] = arguments[t];
                this.listeners.forEach(e => {
                    e(...r);
                });
            }
            hasListener(e) {
                return -1 !== this.listeners.indexOf(e);
            }
            hasListeners() {
                return this.listeners.length > 0;
            }
            removeListener(e) {
                var r = this.listeners.indexOf(e);
                -1 !== r && this.listeners.splice(r, 1);
            }
        }, n = t(9), o = Object(n.a)("mono");
        var i = class {
            constructor() {
                this.onDestroy = new s(), this._lastErrorFired = !1, this._lastError = null;
            }
            get lastError() {
                return this._lastErrorFired = !0, this._lastError;
            }
            set lastError(e) {
                this._lastErrorFired = !e, this._lastError = e;
            }
            clearLastError() {
                this._lastError && !this._lastErrorFired && o.error("Unhandled mono.lastError error:", this.lastError),
                this._lastError = null;
            }
            unimplemented() {
                throw new Error("Unimplemented");
            }
            destroy() {
                this.onDestroy.dispatch();
            }
        }, a = t(25), l = e => (class extends e {
            callFn(e, r) {
                return this.waitPromise({
                    action: "callFn",
                    fn: e,
                    args: r
                });
            }
            waitPromise(e) {
                return new Promise((r, t) => {
                    this.sendMessage(e, e => {
                        if (e) {
                            if (e.err) {
                                var s = a(e.err);
                                return t(s);
                            }
                            return r(e.result);
                        }
                        var n = this.lastError || new Error("Unexpected response");
                        return t(n);
                    });
                });
            }
        }), u = e => (class extends e {}), c = e => (class extends(u(e)){});
        var d = class extends(c(l(i))){
            initMessages() {
                this.sendMessage = this.transport.sendMessage.bind(this.transport), this.onMessage = {
                    addListener: this.transport.addListener.bind(this.transport),
                    hasListener: this.transport.hasListener.bind(this.transport),
                    hasListeners: this.transport.hasListeners.bind(this.transport),
                    removeListener: this.transport.removeListener.bind(this.transport)
                };
            }
        };
        var h = class {
            constructor(e) {
                this.mono = e, this.onChanged = {
                    addListener: e => {
                        browser.storage.onChanged.addListener(e);
                    },
                    hasListener: e => browser.storage.onChanged.hasListener(e),
                    hasListeners: () => browser.storage.onChanged.hasListeners(),
                    removeListener: e => {
                        browser.storage.onChanged.removeListener(e);
                    }
                };
            }
            callback(e, r, t) {
                this.mono.lastError = browser.runtime.lastError, (t || e) && e(r), this.mono.clearLastError();
            }
            get(e, r) {
                browser.storage.local.get(e, e => this.callback(r, e, !0));
            }
            set(e, r) {
                browser.storage.local.set(e, () => this.callback(r));
            }
            remove(e, r) {
                browser.storage.local.remove(e, () => this.callback(r));
            }
            clear(e) {
                browser.storage.local.clear(() => this.callback(e));
            }
        }, f = e => (class extends e {
            constructor() {
                super(), this.isFirefox = !0;
            }
            get isFirefoxMobile() {
                return /(?:Mobile|Tablet);/.test(navigator.userAgent);
            }
        }), g = e => (class extends(f(e)){});
        var b = new class extends(g(d)){
            constructor() {
                super(), this.initMessages(), this.initStorage(), this.initI18n();
            }
            initI18n() {
                this.i18n = {
                    getMessage: browser.i18n.getMessage.bind(browser.i18n)
                };
            }
            initMessages() {
                this.transport = {
                    sendMessage: (e, r) => {
                        r ? browser.runtime.sendMessage(e, e => {
                            this.lastError = browser.runtime.lastError, r(e), this.clearLastError();
                        }) : browser.runtime.sendMessage(e);
                    },
                    addListener: e => {
                        browser.runtime.onMessage.addListener(e);
                    },
                    hasListener: e => browser.runtime.onMessage.hasListener(e),
                    hasListeners: () => browser.runtime.onMessage.hasListeners(),
                    removeListener: e => {
                        browser.runtime.onMessage.removeListener(e);
                    }
                }, super.initMessages();
            }
            initStorage() {
                this.storage = new h(this);
            }
        }();
        r.a = b;
    },
    25: function(e, r, t) {
        var s = t(26).default;
        e.exports = s;
    },
    26: function(e, r, t) {
        "use strict";
        Object.defineProperty(r, "__esModule", {
            value: !0
        });
        var s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e;
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol ? "symbol" : typeof e;
        };
        function n(e) {
            return e && "object" === (void 0 === e ? "undefined" : s(e)) && "string" == typeof e.name && "string" == typeof e.message;
        }
        r.default = function(e) {
            return n(e) ? Object.assign(new Error(), {
                stack: void 0
            }, e) : e;
        }, r.isSerializedError = n;
    },
    39: function(e, r, t) {
        "use strict";
        t.r(r);
        var s = t(0), n = t(6);
        Object(n.a)("tab", () => {
            s.a.sendMessage({
                action: "openPage"
            });
        });
    },
    6: function(e, r, t) {
        "use strict";
        t.d(r, "b", function() {
            return i;
        });
        var s = t(0), n = [];
        s.a.onMessage.addListener((e, r, t) => {
            e && "getLoadedModules" === e.action && (e.url && e.url !== location.href || t(n));
        });
        var o = (e, r, t) => Promise.resolve().then(() => !t || t()).then(t => {
            t && (-1 === n.indexOf(e) && n.push(e), r());
        }), i = (e, r, t) => o(e, () => s.a.callFn("getPreferences").then(t => {
            r(e, {
                preferences: t
            });
        }), t);
        r.a = o;
    },
    9: function(e, r, t) {
        "use strict";
        r.a = (e => {
            var r = null;
            return (r = (() => {})).log = r.info = r.warn = r.error = r.debug = r, r;
        });
    }
});